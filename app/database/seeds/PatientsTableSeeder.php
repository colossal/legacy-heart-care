<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PatientsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 80) as $index)
		{
			$date = $faker->dateTimeBetween($startDate = '-2 months', $endDate = '+20 days');

			$date_end = strtotime("+30 day", strtotime($date->format('Y-m-d')));
			$date_end = date('Y-m-d', $date_end);
			
			$date_start = strtotime("+25 day", strtotime($date->format('Y-m-d')));
			$date_start = date('Y-m-d', $date_start);

			Patient::create([
				'first_name' => $faker->firstName($gender = null|'male'|'female'),
				'last_name' => $faker->lastName,
				'date_of_birth' => $faker->dateTimeBetween($startDate = '-90 years', $endDate = '-40 years'),
				'touchpoint_progress' => '',
				'location_id' => $faker->numberBetween(1,4),
				'touchpoint_start_date' => $date_start,
				'touchpoint_end_date' => $date_end,
				'treatment_completion' => $date,
				'cardiologist_first_name' => $faker->firstName($gender = null|'male'|'female'),
				'cardiologist_last_name' => $faker->lastName,
				'insurer_id' => $faker->numberBetween(1,3),
				'address' => $faker->streetAddress,
				'city' => $faker->city,
				'state' => $faker->stateAbbr,
				'zip' => $faker->postcode,
				'phone' => '214.324.4450',
				'alternate_phone' => '',
				'email' => $faker->email,
				'notes' => $faker->text(20),
				'photo' => '',
				'patient_state' => 1,
				'created_at' => $faker->dateTime(),
				'updated_at' => $faker->dateTime()
			]);
		}
	}

}