<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class LocationsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$location = array(0 => 'Dallas', 1 => 'Austin', 2 => 'Ft.Worth', 3 => 'Kansas City');
		$photo = array(0 => 'loc-dallas.png', 1 => 'loc-austin.png', 2 => 'loc-ftworth.png', 3 => 'loc-kansas-city.png');
		$i = 0;
		foreach(range(1, 4) as $index)
		{
			Location::create([
				'name' => $location[$i],
				'photo' => $photo[$i],
				'active' => 1,
				'description' => $faker->text($maxNbChars = 200),
				'created_at' => $faker->dateTime(),
				'updated_at' => $faker->dateTime()
			]);

			$i++;
		}
	}

}