<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('PatientsTableSeeder');
		$this->call('LocationsTableSeeder');
		$this->call('InsurersTableSeeder');
	}

}
