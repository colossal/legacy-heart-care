<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class InsurersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		$insurer = array(0 => 'Aetna', 1 => 'UnitedHealthcare', 2 => 'Blue Cross Blue Shield', 3 => 'Kansas City');
		$i = 0;
		foreach(range(1, 3) as $index)
		{
			Insurer::create([
				'name' => $insurer[$i],
				'active' => 1,
				'created_at' => $faker->dateTime(),
				'updated_at' => $faker->dateTime()
			]);

			$i++;
		}
	}

}