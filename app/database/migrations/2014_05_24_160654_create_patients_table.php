<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patients', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->string('first_name', 35);
			$table->string('last_name', 35);
			$table->dateTime('date_of_birth');
			$table->tinyInteger('touchpoint_snooze')->default(0);
			$table->tinyInteger('touchpoint_progress')->default(0);
			$table->dateTime('touchpoint_start_date');
			$table->dateTime('touchpoint_end_date');
			$table->tinyInteger('location_id')->unsigned();
			$table->dateTime('treatment_completion');
			$table->string('cardiologist_first_name', 35);
			$table->string('cardiologist_last_name', 35);
			$table->tinyInteger('insurer_id')->unsigned();
			$table->tinyInteger('secondary_insurer_id')->unsigned();
			$table->string('address', 35);
			$table->string('city', 35);
			$table->string('state', 2);
			$table->string('zip', 5);
			$table->string('phone', 12);
			$table->string('alternate_phone', 12);
			$table->string('email', 80);
			$table->text('notes');
			$table->string('photo', 80);
			$table->boolean('patient_state')->default(true);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patients');
	}

}
