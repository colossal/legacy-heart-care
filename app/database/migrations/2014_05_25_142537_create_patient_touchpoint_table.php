<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientTouchpointTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patient_touchpoint', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('patient_id')->unsigned()->index();
            $table->integer('touchpoint_id')->unsigned()->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patient_touchpoint');
	}

}
