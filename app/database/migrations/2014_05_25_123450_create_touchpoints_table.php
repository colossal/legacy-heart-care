<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTouchpointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('touchpoints', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->tinyInteger('touchpoint_progress')->unsigned();
			$table->boolean('heart_failure')->nullable();
			$table->boolean('ranexa')->nullable();
			$table->tinyInteger('angina')->nullable();
			$table->tinyInteger('sympton_severity')->nullable();
			$table->tinyInteger('nitro')->nullable();
			$table->tinyInteger('walking_distance')->nullable();
			$table->boolean('emergency_room_visit')->nullable();
			$table->boolean('cardio_hospitalization')->nullable();
			$table->tinyInteger('cardio_hospitalization_total')->nullable();
			$table->boolean('cardio_related_procedure')->nullable();
			$table->text('notes')->nullable();
			$table->boolean('touchpoint_complete')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('touchpoints');
	}

}
