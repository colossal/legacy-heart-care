<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{

			$table->increments('id')->unsigned();
			$table->string('first_name', 35);
			$table->string('last_name', 35);
			$table->string('email', 80)->unique();
			$table->string('password', 60);
			$table->string('remember_token', 100);
			$table->string('confirmation', 16)->nullable();
			$table->boolean('active')->default(false);
			$table->tinyInteger('access_level')->unsigned();
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
