@extends('admin.layouts.master')

@section('title')
Staff Edit
@stop

@section('head')
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"  rel='stylesheet' type='text/css'>
	<link href="/css/summernote.css" rel='stylesheet' type='text/css' />
@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">Staff Edit <a href="/admin/manage/staff" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a></div>
					<div class="panel-body">

					{{ Form::model($staff, array('route' => array('admin.manage.staff.destroy', $staff->id), 'method' => 'DELETE')) }}										

							@if($errors->any())
								<div class="alert alert-danger">
									<ul>
										{{ implode('', $errors->all('<li class="error">:message</li>')) }}
									</ul>
								</div>
							@endif

							<div class="form-group">
								{{ Form::label('first_name', 'First Name *') }}
								{{ Form::text('first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control')) }}
							</div>
							<div class="form-group">
								{{ Form::label('last_name', 'Last Name *') }}
								{{ Form::text('last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control')) }}
							</div>
							<div class="form-group">
								{{ Form::label('title', 'Title') }}
								{{ Form::text('title', null, array('placeholder' =>'Title', 'class' => 'form-control')) }}
							</div>
							<div class="form-group">
								{{ Form::label('email', 'Email *') }}
								{{ Form::text('email', null, array('placeholder' =>'Email', 'class' => 'form-control')) }}
							</div>
							<div class="form-group">
								{{ Form::label('phone', 'Phone') }}
								{{ Form::text('phone', null, array('placeholder' =>'Phone', 'class' => 'form-control')) }}
							</div>
							<div class="form-group">
								{{ Form::label('fax', 'Fax') }}
								{{ Form::text('fax', null, array('placeholder' =>'Fax', 'class' => 'form-control')) }}
							</div>
							<div class="form-group">
								{{ Form::label('bio', 'Bio') }}
								{{ Form::textarea('bio', null, array('placeholder' =>'Bio', 'class' => 'form-control summernote')) }}
							</div>

							<div class="form-group">
								
								<select name="active" class="form-control">
								
									@if( $staff->active == 0 )
										<option value="0" selected="selected">Inactive</option>
										<option value="1">Active</option>
									@else
										<option value="0">Inactive</option>
										<option value="1"selected="selected">Active</option>
									@endif
								
								</select>
							</div>

							<div class="form-group pull-right">			
								{{ link_to('admin/manage/staff', 'Cancel',  array('class' => 'btn btn-danger')) }}
								{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
							</div>

						{{ Form::close() }}
					</div>
					</div>
				</div> <!-- /.panel -->
			</div>
		</div>
		
	 </section> <!-- /.container -->
@stop

@section('script')
<script src="/js/vendor/summernote.min.js"></script>
@stop