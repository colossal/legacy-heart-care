@extends('admin.layouts.master')

@section('title')
Staff Create
@stop

@section('head')
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"  rel='stylesheet' type='text/css'>
	<link href="/css/summernote.css" rel='stylesheet' type='text/css' />
@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">
				<div class="panel">
					<div class="panel-heading">Create a new staff member <a href="/admin/manage/staff" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a></div>
					<div class="panel-body">
						{{ Form::open(array('url' => 'admin/manage/staff', 'class' => 'form-admin', 'files' => true)) }}

							@if($errors->any())
								<div class="alert alert-danger">
									<ul>
										{{ implode('', $errors->all('<li class="error">:message</li>')) }}
									</ul>
								</div>
							@endif

							<div class="row">
								<div class="col-lg-7 col-sm-12">
									<div class="form-group">
										{{ Form::label('first_name', 'First Name *') }}
										{{ Form::text('first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control')) }}
									</div>
									<div class="form-group">
										{{ Form::label('last_name', 'Last Name *') }}
										{{ Form::text('last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control')) }}
									</div>
									<div class="form-group">
										{{ Form::label('title', 'Title') }}
										{{ Form::text('title', null, array('placeholder' =>'Title', 'class' => 'form-control')) }}
									</div>
									<div class="form-group">
										{{ Form::label('email', 'Email *') }}
										{{ Form::text('email', null, array('placeholder' =>'Email', 'class' => 'form-control')) }}
									</div>
									<div class="form-group">
										{{ Form::label('phone', 'Phone') }}
										{{ Form::text('phone', null, array('placeholder' =>'Phone', 'class' => 'form-control')) }}
									</div>
									<div class="form-group">
										{{ Form::label('fax', 'Fax') }}
										{{ Form::text('fax', null, array('placeholder' =>'Fax', 'class' => 'form-control')) }}
									</div>
								</div>
								<div class="col-lg-5 col-sm-12">
									
									{{ Form::label('photo', 'Bio Photo') }}
									<div class="form-group">

										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail" style="max-width:355px;">
												@if(!empty($staff->photo))
													<img src="/images/staff/{{ $staff->photo }}" alt="">
												@else
													<img data-src="holder.js/300x300/auto/#41cac0:#fff" alt="" class="img-responsive">
												@endif										
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 355px; max-height: 355px;"></div>
											<div>
												<span class="btn btn-default btn-file">
													<span class="fileinput-new">Select image</span>
													<span class="fileinput-exists">Change</span>
													<input type="file" name="photo">
												</span>
												<a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
											</div>
										</div>
									</div>

								</div>
							</div>


							<div class="form-group">
								{{ Form::label('bio', 'Bio') }}
								{{ Form::textarea('bio', '', array('placeholder' =>'Bio', 'class' => 'form-control summernote')) }}
							</div>

							<div class="form-group">
								<select name="active" class="form-control">
									<option value="0">Save as Draft</option>
									<option value="1">Publish</option>
								</select>
							</div>

							<div class="form-group pull-right">			
								{{ link_to('admin/manage/staff', 'Cancel',  array('class' => 'btn btn-danger')) }}
								{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
							</div>

						{{ Form::close() }}
					</div>
					</div>
				</div> <!-- /.panel -->
			</div>
		</div>
		
	 </section> <!-- /.container -->
@stop

@section('script')
	<script src="/js/vendor/summernote.min.js"></script>
	<script src="/js/vendor/holder.js"></script>
@stop