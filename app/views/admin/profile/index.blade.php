@extends('admin.layouts.master')

@section('title')
Staff
@stop

@section('content')
	<section class="wrapper">

		<div class="row">
			<div class="col-sm-12">
				
				<div class="panel">
					<div class="panel-heading">Staff<a href="/admin/manage/staff/create" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus-sign"></i> Add New</a>

					</div>
					<nav class="tab-dark">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#staff_list" data-toggle="tab">List</a></li>
							<li><a href="#staff_sorting" data-toggle="tab">Sorting</a></li>
						</ul>
					</nav>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane fade in active" id="staff_list">
							<div class="panel-body">

								@if( Session::has('success') )
									<div class="alert alert-success alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
										{{ Session::get('success') }}
									</div>
								@endif
						
								<table class="table table-striped table-hover">
									<thead>
									<tr>
										<th width="45%">Name</th>
										<th>Status</th>
										<th>Actions</th>
									</tr>
									</thead>
									<?php
										$status = Config::get('constants.STATUS');
									?>
									@if(!empty($staff[0]))
										@foreach($staff as $item)
											<tr>
												<td><a href="/admin/manage/staff/{{ $item->id }}/edit">{{ $item->first_name }} {{ $item->last_name }}</a></td>
												<td>{{ $status[$item->active] }}</td>
												<td>
													<a href="#" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
													<a href="/admin/manage/staff/{{ $item->id }}/edit" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
													<a href="/admin/manage/staff/{{ $item->id }}" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></a>
												</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="3">
												<div class="alert alert-info">No staff members were found.</div>
											</td>
										</tr>										
									@endif
								</table>
							</div> <!-- /.panel-body -->
						</div> <!-- /#staff_list -->
						<div class="tab-pane fade" id="staff_sorting">
							<div class="panel-body">
								<div class="list-group">
								@if(!empty($staff[0]))
									@foreach($staff as $item)
										<div id="{{ $item->id }}" class="list-group-item media">
											@if( empty( $item->photo ) )
												<img data-src="holder.js/40x40/auto/#41cac0:#fff" alt="" class="img-responsive pull-left">
											@else
												<div class="pull-left"><img src="/images/staff/{{ $item->photo }}" alt="{{ $item->first_name }} {{ $item->first_name }}" width="40" /></div>
											@endif
											<div class="media-body">{{ $item->first_name }} {{ $item->first_name }} <i class="glyphicon glyphicon-sort pull-right"></i></div>
										</div>										
									@endforeach
								@else
									<div class="alert alert-info">No staff members were found.</div>
								@endif
								</div>
							</div> <!-- /#staff_sorting -->
						</div>
					</div>					
				</div> <!-- /.panel -->
			</div> <!-- /.col-sm-12 -->
		</div> <!-- /.row -->
		
	 </section> <!-- /.container -->
@stop

@section('script')
<script>
	$('.delete').click( function (event) {
		var item = $(this);
		//bootbox.confirm("Are you sure delete?", function(result) {
		//	if(result){
		item.closest('tr').animate(
			{ opacity: 0 }, {
				duration: 400,
				easing: 'easeOutExpo',
				complete:  function() {
					item.closest('tr').remove();
				} 
			});
		//	}
		//});
				
		event.preventDefault();
	});
</script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="/js/vendor/jquery.ui.touch-punch.min.js"></script>
<script>
	$(function () {
		$(".list-group").sortable({
		    tolerance: 'pointer',
		    revert: 'invalid',
		    placeholder: 'placeholder-item',
		    forceHelperSize: true,

		    update: function(event, ui) {
		        order = $('.list-group').sortable("toArray");
		        $.ajax({
		        	type: "POST",
		            url: URL.base + '/admin/manage/staff/sort',
		            data: {order:order},
		            success: function(data)
					{
						console.log(data);
					}                     
		        });
		    }
		});
	});
</script>
<script src="/js/vendor/holder.js"></script>
@stop