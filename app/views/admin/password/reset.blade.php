@extends('admin.layouts.plain')

@section('title')
Reset Password
@stop

@section('head')

@stop

@section('content')
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<br><br>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						@if(count($errors))
						<div class="col-md-12 form-group">
							<div class="alert alert-danger">
								<ul>
									@foreach($errors->all() as $error)
									<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
						@endif
						<div class="col-md-12">
							<small>For security purposes, we ask that you fill out your first and last name again to confirm your identity</small>
						</div>
						{{ Form::open(['class' => 'password-reset']) }}
						<div class="col-md-6 form-group">
							<label for="">Your First Name</label>
							{{ Form::text('first_name', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-6 form-group">
							<label for="">Your Last Name</label>
							{{ Form::text('last_name', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-12 form-group">
							<label for="">Your Email</label>
							{{ Form::email('email', null, ['class' => 'form-control']) }}
						</div>
						<div class="form-group col-md-12 password-rules hidden">
							<div class="well">
								<strong>Your password must</strong><br>
								be a minimum of 10 characters <br>
								contain at least 1 uppercase character <em>(A-Z)</em><br>
								contain at least 1 numeral <em>(0-9)</em><br>
								container a special character <em>(ex: @$%)</em>

							</div>
						</div>
						<div class="col-md-12 form-group">
							<label for="">Your New Password</label>
							{{ Form::password('password', ['class' => 'form-control', 'id' => 'password']) }}
						</div>
						<div class="col-md-12 form-group">
							<label for="">Confirm Password</label>
							{{ Form::password('password_confirm', ['class' => 'form-control']) }}
						</div>
						<div class="col-md-12 form-group text-center">
							<button type="submit" class="btn btn-primary">Update Password</button>
						</div>
						{{ Form::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('script')

	<script src="{{ asset('library/js/password.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('#password').password({
				minimumLength: 10,
				showText: true, // shows the text tips
				animate: true, // whether or not to animate the progress bar on input blur/focus
				animateSpeed: 'fast', // the above animation speed
			});
			$(window).on('keyup', function(){
				var passwordLength = $('#password').val().length;
				if($('#password').val().length > 0) {
					$('.password-rules').removeClass('hidden');
					$('#password').on('password.score', function(e, score){
						if(score >= 68) {
							$('button[type="submit"]').removeClass('disabled');
						} else {
							$('button[type="submit"]').addClass('disabled');
						}
					});
				} else {
					$('.password-rules').addClass('hidden');
					$('button[type="submit"]').removeClass('disabled');
				}
			});
		});
	</script>
@stop