@extends('admin.layouts.master')

@section('title')
    {{--Password--}}
@stop

@section('head')
    <link rel="stylesheet" href="{{ URL::asset('library/css/password.css') }}">
@stop

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ URL::current() }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @if($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                    </ul>
                </div>
            @endif
            <br><br>
            <div class="panel panel-default">
                <div class="panel-body">
                    <h4>Reset Your Password</h4>
                    <hr>
                    <p>It has been over a year since your last password change. Please change your password to continue.</p>
                    <div class="well">
                        <strong>Your password must</strong><br>
                        be a minimum of 10 characters <br>
                        contain at least 1 uppercase character <em>(A-Z)</em><br>
                        contain at least 1 numeral <em>(0-9)</em><br>
                        container a special character <em>(ex: @$%)</em>

                    </div>
                    <div class="form-group">
                        <label for="">Your New Password</label>
                        <input type="password" id="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Update Password">
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>

@stop

@section('script')
    <script src="{{ URL::asset('library/js/password.js') }}"></script>
    <script>
//        ^.*(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X]).*$
        $(document).ready(function(){
            $('#password').password({
                minimumLength: 10,
                showText: true, // shows the text tips
                animate: true, // whether or not to animate the progress bar on input blur/focus
                animateSpeed: 'fast', // the above animation speed
            });
            $(window).on('keyup', function(){
                var passwordLength = $('#password').val().length;
                if($('#password').val().length > 0) {
                    $('#password').on('password.score', function(e, score){
                        if(score >= 68) {
                            $('input[type="submit"]').removeClass('disabled');
                        } else {
                            $('input[type="submit"]').addClass('disabled');
                        }
                    });
                } else {
                    $('input[type="submit"]').removeClass('disabled');
                }
            });
        });
    </script>
@stop