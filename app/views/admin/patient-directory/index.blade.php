@extends('admin.layouts.master')

@section('title')
Patients
@stop

@section('head')

@stop

@section('content')
	<div class="row">
	  	<div class="col-lg-5">
			<br />
			<a href="{{ URL::to('patient-directory/create') }}" style="margin-top:-5px;" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i> Add Patient Record</a>
			
			@if(!empty($patients[0]))
				<a href="{{ URL::to('patient-directory/download') }}" style="margin-top:-5px;" class="btn btn-default"><i class="glyphicon glyphicon-circle-arrow-down"></i> Download Patient Data</a>
			@endif
					
		</div>   	  	
		
		<div class="col-xs-12 col-sm-12 col-lg-3">
			<br />
			<label class="pull-right" for="">Filter View:</label> 
		</div>
		<div class="col-lg-4">
					
			<div class="row">
				<div class="col-xs-8">
					{{ Form::open(array('route' => 'patient-directory.set', 'class' => 'filter')) }}
					<div class="form-group">
						<select name="location_id" id="location_id" class="form-control col-sm-6">
							<option value="">View All Locations</option>
							@foreach(Location::all() as $location)
							<option value="{{ $location->id }}" {{ Session::get('location.id') && Session::get('location.id') == $location->id ? 'selected' : null  }}>{{ $location->name }}</option>
							@endforeach
						</select>
					</div>
					{{ Form::close() }}
				</div>
				<div class="col-xs-4">
					{{ Form::open(array('route' => 'patient-directory.state', 'class' => 'filter')) }}
					<div class="form-group">
						{{ Form::select('patient_state', Config::get('constants.PATIENT_STATE'), $patient_state, array('class' => 'form-control col-sm-6', 'id' => 'patient_state') ) }}
					</div>

					{{ Form::close() }}
				</div>

			</div>
	
		</div>

	</div> <!-- /.row -->
	<div class="row"><br /></div>

	<section class="row">
		<div class="col-sm-12">
			
			@if( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('success') }}
				</div>
			@endif

			@if(!empty($patients[0]))

			<table class="table table-striped table-hover">
				<thead>
				<tr>
					<th colspan="2">Title</th>
					<th>Phone</th>
					<th>Status</th>
					<th>Cardiologist</th>
					<th>Round</th>
					<th>TouchPoint</th>
					<th>Actions</th>
				</tr>
				</thead>
				
				<?php $touchpoints = Config::get('constants.TOUCHPOINT'); ?>

				@foreach($patients as $patient)
					<tr>
						<td width="40">
							@if(!empty($patient->photo))
								<img src="/images/patients/{{$patient->photo}}" width="40" alt="">
							@else
								<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
							@endif
						</td>
						<td class="td_valign">
						<a href="/patient-directory/{{ $patient->id }}/edit">
							@if($patient->patient_id != NULL)
							<strong>#{{ str_pad($patient->patient_id, 5, "0", STR_PAD_LEFT) }}</strong>
							@else
							<strong class="text-muted">#00000</strong>
							@endif
						&nbsp;&nbsp;&nbsp;{{ $patient->first_name }} {{ $patient->last_name }}</a></td>
						<td class="td_valign">{{ $patient->phone }}</td>
						<td class="td_valign">
							@if($patient->patient_state == 1)
								<span style="color:#00804d">Active</span>
							@elseif ($patient->patient_state == 2)
								<span style="color:#d1be20">Archived</span>
							@elseif ($patient->patient_state == 3)
								<span style="color:#878787">Expired</span>
							@endif
						
						</td>
						<td class="td_valign">
							@if(!empty($patient->cardiologist_last_name))
								Dr. {{ $patient->cardiologist_first_name }} {{ $patient->cardiologist_last_name }}
							@endif
						</td>
						<td class="td_valign">
							{{ $patient->rounds_label }}
						</td>
						<td class="td_valign">
							@if(isset($touchpoints[$patient->touchpoint_progress]))
								@if(($patient->touchpoint_progress == 1) && (date('Y-m-d H:i:s', strtotime($patient->touchpoints()->orderby('created_at', 'desc')->first()->created_at)) < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00'))))
									60 days
								@else
									{{ $touchpoints[$patient->touchpoint_progress] }}
								@endif
							@endif
						</td>
						<td class="text-center">
							<a href="{{ URL::to('patient-directory/'.$patient->id.'/edit') }}" class="btn btn-primary view">Patient Info</a>
						</td>
					</tr>
				@endforeach
			</table>
			
			@if(!empty($patients[0]))
				<div class="text-right">
				{{ $patients->appends(Request::except('page', '_token'))->links() }}
				</div>
			@endif
			
			@else
					
				<div class="alert alert-info">No patients were found.</div>	
					
			@endif
			
		</div> <!-- /.col-sm-12 -->	
	 </section> <!-- /.container -->
@stop

@section('script')
	<script>
		$(function() {
			$('#location_id').change(function() {
				this.form.submit();
			});
			$('#patient_state').change(function() {
				this.form.submit();
			});
		});
	</script>
@stop