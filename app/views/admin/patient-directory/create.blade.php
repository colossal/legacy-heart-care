@extends('admin.layouts.master')

@section('title')
Patient Create
@stop

@section('head')
	
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/patient-directory" class="btn btn-default btn-sm pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
			<br /><br />
		</div>
	</div>

	<!-- DEV NOTE : PATIENT INFORMATION FORM -->

	{{ Form::open(array('route' => 'patient-directory.store', 'class' => 'patient', 'files' => true)) }}
   		<div class="row">
	   		<div class="col-sm-10 col-sm-offset-1">

	   			@if(Session::get('class'))
					<div class="alert {{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						@if($errors->any())
							<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
							</ul>
						@else
							{{ Session::get('success') }}
						@endif
					</div>
				@endif
	   			
		   		<div class="row">
		   			<div class="col-md-12">
		   				<span class="text-danger">*</span> are required fields
		   				<hr>
		   			</div>
		   			<div class="col-md-12 form-group">
		   				{{-- <a data-toggle="collapse" data-target="#findPatient" class="btn btn-default btn-xs">Recurring Patient?</a> --}}
		   				<div class="form-group" id="findPatient">
		   					<div class="well">
		   						<strong>Check for Existing Patients</strong><br>
		   						We recommend entering the patient's Patient ID in the following search bar before entering any data. The system will autopopulate any information it can find if a matching record exists.
		   					</div>
		   					{{ Form::hidden('search_token', csrf_token()) }}
		   					{{ Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'SEARCH BY PATIENT ID', 'style' => 'margin-top:10px']) }}
		   					<button class="btn btn-primary search-by-id" style="margin-top:10px;">Search</button>
		   					<div class="success hidden">
		   						<br>
		   						<div class="alert alert-success">
		   							<strong>Success</strong><br>
		   							We have found "<span class="patient-name"></span>" in the patient directory. We have populated the fields below with the available information. You may update any fields, which will update only the <strong>New Patient Record</strong> you are about to create.
		   						</div>
		   					</div>
		   					<div class="info hidden">
		   						<br>
		   						<div class="alert alert-info">
		   							<strong>No Results</strong> <br>	
		   							We did not find any patient records with that Patient ID
		   						</div>
		   					</div>
		   				</div>
		   			</div>
		   			<div class="col-sm-4">
		   				<div class="form-group">
		   					{{ Form::label('patient_id', 'Patient ID') }} <span class="text-danger">*</span>
		   					<div class="input-group">
		   						<span class="input-group-addon">#</span>
		   						{{ Form::text('patient_id', '', array('class' => 'form-control patient-id')) }}
		   					</div>
		   				</div>
		   			</div>
		   			<div class="col-sm-4">
		   				<div class="form-group">
		   					{{ Form::label('non_english', 'Non-english Speaker') }}
		   					{{ Form::text('non_english', null, ['class' => 'form-control']) }}
		   				</div>
		   			</div>
					<div class="col-sm-4">
						{{-- <div class="form-group">
							{{ Form::label('repeat', 'Is this a recurring patient?') }}
							<select name="repeat" id="repeat" class="form-control">
								<option value="0">N/A</option>
								@for($i = 1; $i <= 10; $i++)
									<option value="{{ $i }}">{{ $i }} {{ $i == 1 ? 'time' : 'times' }}</option>
								@endfor
							</select>
						</div> --}}
					</div>
		   			<div class="clearfix"></div>
			   		<div class="col-sm-6">
			   			<div class="form-group">
							{{ Form::label('first_name', 'First Name') }} <span class="text-danger">*</span>
							{{ Form::text('first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control', 'style' => 'height:35px')) }}
						</div>
						<div class="form-group">
							{{ Form::label('last_name', 'Last Name') }} <span class="text-danger">*</span>
							{{ Form::text('last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control', 'style' => 'height:35px')) }}
						</div>
						{{ Form::label('cardiologist', 'Cardiologist') }}
				   		<div class="row">				   			
					   		<div class="col-sm-6">
					   			<div class="form-group">
									{{ Form::text('cardiologist_first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control', 'style' => 'height:35px')) }}
								</div>
					   		</div>
					   		<div class="col-sm-6">
					   			<div class="form-group">
									{{ Form::text('cardiologist_last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control', 'style' => 'height:35px')) }}
								</div>
					   		</div>
				   		</div> <!-- /.row .cardio-name -->
			   		</div>
			   		
			   		<div class="col-sm-6">
					   		
						<div class="form-group">								
							{{ Form::label('location_id', 'Treatment Location') }} <span class="text-danger">*</span>
							{{ Form::select('location_id', $locations, $location_id, array('class' => 'form-control')) }}
						</div>
				   		<?php

				   			if(Input::old('treatment_completion')) {
				   				$completion = date('m/d/Y', strtotime(Input::old('treatment_completion')));
				   			} else {
				   				$completion = null;
				   			}
				   		?>
				   		<div class="form-group">
					   		<label for="">Treatment Completion</label> <span class="text-danger">*</span><br />
					   		<input style="height:35px;" id="treatment_completion" name="treatment_completion" value="{{$completion}}" type="text" placeholder="{{ date('m/d/Y') }}" class="treatment_completion" />
				   		</div>
				   		
				   		<div class="form-group">
				   			<div class="media">
								<div class="pull-left">
									
									<img width="70" src="http://placehold.it/70&text=Profile" alt="">
									
								</div>
								<div class="media-body">
									<br />
									<label for="photo">Photo</label>
									{{ Form::file('photo') }}
								</div>
							</div>
				   		</div>
				   	
				   		
			   		</div>				   		
			   		
			   		<div class="form-group col-sm-6">
			   			{{ Form::label('insurer_id', 'Primary Insurer') }}
						{{ Form::select('insurer_id', $insurers, null, array('class' => 'form-control')) }}
			   		</div>
			   		
			   		<div class="form-group col-sm-6">
			   			<label class="optional" for="">Secondary Insurer</label>
						{{ Form::select('secondary_insurer_id', $secondary_insurers, null, array('class' => 'form-control')) }}
			   		</div>

			   		<div class="form-group col-sm-12">
						{{ Form::label('address', 'Patient Street Address') }}
						{{ Form::text('address', null, array('placeholder' =>'Address', 'class' => 'form-control')) }}
					</div>

					<div class="form-group col-sm-6">
						{{ Form::label('city', 'City') }}
						{{ Form::text('city', null, array('placeholder' =>'City', 'class' => 'form-control')) }}
					</div>
					<?php
						$states = Config::get('constants.USA_STATES');
					?>

			   		<div class="form-group col-sm-3">
				   		{{ Form::label('state', 'State') }}
						{{ Form::select('state', $states, null, array('class' => 'form-control')) }}				  
			   		</div>

			   		<div class="form-group col-sm-3">
				   		{{ Form::label('zip', 'ZIP Code') }}
						{{ Form::text('zip', null, array('placeholder' =>'Zip', 'class' => 'form-control')) }}				  
			   		</div>

					<?php
			   			if( Input::old('date_of_birth') ) {
			   				$dob = date('m/d/Y', strtotime(Input::old('date_of_birth')));
			   			} else {
			   				$dob = null;
			   			}
			   		?>

			   		<div class="form-group col-sm-6">
				   		<label for="">Date of Birth</label>
				   		{{ Form::text('date_of_birth', $dob, array('placeholder' =>'00/00/0000', 'id' => 'dob', 'class' => 'dob')) }}
			   		</div>
			   		
					<div class="form-group col-sm-6">
				   		{{ Form::label('email', 'Email Address', array('class' => 'optional')) }}
				   		{{ Form::email('email', null, array('placeholder' => 'name@email.com')) }}
			   		</div>
			   		
			   		<div class="form-group col-sm-6">
				   		{{ Form::label('phone', 'Phone') }} <span class="text-danger">*</span>
				   		{{ Form::input('phone', 'phone', null, array('placeholder' => '123.456.7890')) }}  
						
						<!-- DEV NOTE : TARUN USE JAVASCRIPT VALIDATION | PHONE NUMBER FORMAT SHOULD BE LIMITED TO ###.###.#### -->
			   		</div>
			   		
			   		<div class="form-group col-sm-6">
				   		<label class="optional" for="">Alternate Phone Number</label>
				   		{{ Form::input('phone', 'alternate_phone', null, array('placeholder' => '123.456.7890', 'class' => 'alt-phone')) }}
			   		</div>

			   		<div class="form-group col-sm-12">
						{{ Form::label('notes', 'Notes') }}
						{{ Form::textarea('notes', null, array('placeholder' =>'enter your notes here', 'class' => 'form-control', 'rows' => '10')) }}
						<!-- <textarea name="notes" id="" cols="30" rows="10" placeholder="enter your notes here"></textarea> -->
					</div>	
						   		
			   		<div class="clear"></div>
			   		<br /><br />
			   		<div style="margin-top:25px;" class="col-lg-6 pull-right">
						<div class="btn-group pull-right patient-state" style="max-width:inherit;">
							<label for="patient-state1" class="btn btn-default patient-state active-patient"><input type="radio" name="patient_state" id="patient-state1" value="1" /> Active</label>
							<label for="patient-state4" class="btn btn-default patient-state incorrect-patient"><input type="radio" name="patient_state" id="patient-state4" value="4" /> Incorrect</label>
							<label for="patient-state2" class="btn btn-default patient-state archived-patient"><input type="radio" name="patient_state" id="patient-state2" value="2" /> Archived</label>
							<label for="patient-state3" class="btn btn-default expired-patient"><input type="radio" name="patient_state" id="patient-state3" value="3"/> Expired</label>
						</div>
						<div class="well deceased-date hidden" style="display:block;margin-top:20px;">
							<label for="">Expired Patient Notes</label>
							<textarea name="expiration_note" id="expiration_note" cols="30" rows="10" class="form-control"></textarea>
								
							<div class="clearfix"></div>
						</div>
			   		</div>
					
			   		<div class="form-group col-lg-6 pull-left">			
						{{ link_to('/patient-directory', 'Cancel',  array('class' => 'btn btn-danger')) }}
						{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
					</div>
			   		
			   		
		   		</div> <!-- /.row -->
		   		
	   		</div> <!-- /.col-sm-* -->
	   		
   		</div> <!-- ./row -->
   		
	{{ Form::close() }}
	<!-- ///END DEV NOTE : PATIENT INFORMATION FORM -->

@stop

@section('script')
	<script>
		$(document).keypress(function(e) {
		    if(e.which == 13) {
		        e.preventDefault();
		    }
		});
		$('button.search-by-id').on('click', function(e){
			e.preventDefault();
			var form = $(this).parent();

			var fields = form.find(':input').val();
			var url = '{{ URL::to('get-by-id') }}';
			$.ajax({
				method:'POST',
				url: url,
				data: {
					search:form.find('input[name="search"]').val(),
					_token:form.find('input[name="search_token"]').val()
				}, 
				success:function(response){
					if(response.status == true) {
						form.find('.success').removeClass('hidden');
						form.find('.info').addClass('hidden');
						form.find('span.patient-name').text(response.name);
						$('form.patient').find('input[name="patient_id"]').val(response.id).parent().addClass('has-success');
						$('form.patient').find('input[name="first_name"]').val(response.first_name).parent().addClass('has-success');
						$('form.patient').find('input[name="last_name"]').val(response.last_name).parent().addClass('has-success');
						$('form.patient').find('select[name="location_id"]').val(response.location_id).parent().addClass('has-success');
						$('form.patient').find('input[name="cardiologist_first_name"]').val(response.cardiologist_first_name).parent().addClass('has-success');
						$('form.patient').find('input[name="cardiologist_last_name"]').val(response.cardiologist_last_name).parent().addClass('has-success');
						$('form.patient').find('select[name="insurer_id"]').val(response.insurer_id).parent().addClass('has-success');
						$('form.patient').find('select[name="secondary_insurer_id"]').val(response.secondary_insurer_id).parent().addClass('has-success');
						$('form.patient').find('input[name="address"]').val(response.address).parent().addClass('has-success');
						$('form.patient').find('input[name="city"]').val(response.city).parent().addClass('has-success');
						$('form.patient').find('select[name="state"]').val(response.state).parent().addClass('has-success');
						$('form.patient').find('input[name="zip"]').val(response.zip).parent().addClass('has-success');
						$('form.patient').find('input[name="phone"]').val(response.phone).parent().addClass('has-success');
						$('form.patient').find('input[name="alternate_phone"]').val(response.alternate_phone).parent().addClass('has-success');
						$('form.patient').find('input[name="email"]').val(response.email).parent().addClass('has-success');
						// $('form.patient').find('input[name="treatment_completion"]').val(response.treatment_completion).parent().addClass('has-success');
						$('form.patient').find('input[name="date_of_birth"]').val(response.date_of_birth).parent().addClass('has-success');

						/*
						'status' => true,
    			'id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->id,
    			'name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->name,
    			'first_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->first_name,
    			'last_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->last_name,
    			'location_id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->location_id,
    			'cardiologist_first_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->cardiologist_first_name,
    			'cardiologist_last_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->cardiologist_last_name,
    			'insurer_id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->insurer_id,
    			'secondary_insurer_id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->secondary_insurer_id,
    			'address' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->address,
    			'city' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->city,
    			'state' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->state,
    			'zip' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->zip,
    			'phone' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->phone,
    			'alternate_phone' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->alternate_phone,
    			'email' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->email,
    			'photo' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->photo,
    			'treatment_completion' => date('m/d/Y', strtotime(Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->treatment_completion)),
						*/
					} else {
						form.find('.success').addClass('hidden');
						form.find('.info').removeClass('hidden');
						$('form.patient').find('input[name="patient_id"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="first_name"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="last_name"]').val('').parent().removeClass('has-success');
						$('form.patient').find('select[name="location_id"]').val(0).parent().removeClass('has-success');
						$('form.patient').find('input[name="cardiologist_first_name"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="cardiologist_last_name"]').val('').parent().removeClass('has-success');
						$('form.patient').find('select[name="insurer_id"]').val(0).parent().removeClass('has-success');
						$('form.patient').find('select[name="secondary_insurer_id"]').val(0).parent().removeClass('has-success');
						$('form.patient').find('input[name="address"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="city"]').val('').parent().removeClass('has-success');
						$('form.patient').find('select[name="state"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="zip"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="phone"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="alternate_phone"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="email"]').val('').parent().removeClass('has-success');
						// $('form.patient').find('input[name="treatment_completion"]').val('').parent().removeClass('has-success');
						$('form.patient').find('input[name="date_of_birth"]').val('').parent().removeClass('has-success');
					}
				},
				error:function(response){
					console.log(response);
				}
			});
		});
		$('label.expired-patient').on('click', function(){
			$('.deceased-date').removeClass('hidden');
		});
		$('label.patient-state').on('click', function(){
			$('.deceased-date').addClass('hidden');
		});
	</script>
@stop