@extends('admin.layouts.master')

@section('title')
Patient Edit
@stop

@section('head')
	
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
	   <!-- <a href="/patient-directory" class="btn btn-default btn-sm pull-left"><i class="glyphicon glyphicon-chevron-left"></i> Back</a> -->
			<a href="{{ URL::to('patient-directory') }}" class="btn btn-default btn-sm pull-left"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
			<br /><br />
		</div>
	</div>

	<!-- DEV NOTE : PATIENT INFORMATION FORM -->
	{{ Form::model($patient, array('route' => array('patient-directory.update', $patient->id), 'method' => 'PUT', 'class' => 'patient', 'files' => true)) }}
   		<div class="row">
	   		<div class="col-sm-10 col-sm-offset-1">

	   			@if(Session::get('class'))
					<div class="alert {{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						@if($errors->any())
							<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
							</ul>
						@else
							{{ Session::get('success') }}
						@endif
					</div>
				@endif
	   			
		   		<div class="row">
		   			<div class="col-md-12">
		   				<span class="text-danger">*</span> are required fields
		   				<hr>
		   			</div>
		   			<div class="col-md-12">
		   				<p style="margin-bottom:0;">You are editing {{ $patient->name.'\'s' }}...</p>
		   				<?php $count = Patient::where('patient_id', $patient->patient_id)->orderby('created_at', 'desc')->count(); ?>
		   				<div style="background:#FFF;box-sizing: border-box;padding:20px;margin-top:-15px;" class="rounds-container">
		   					<div style="display:flex;width:100%;">
		   						@foreach(Patient::where('patient_id', $patient->patient_id)->orderby('created_at', 'desc')->get() as $p)
		   						<div style="padding:0 5px;">
		   							<a href="{{ URL::to('patient-directory/'.$p->id.'/edit') }}" class="btn btn-{{ $p->id == $patient->id ? 'primary' : 'default' }} btn-sm">Round {{ $count-- }}</a> 
									@if(Auth::user()->access_level == 9 && Patient::where('patient_id', $patient->patient_id)->count() > 1)
		   							<br>
		   							<a data-toggle="collapse" data-target="#deleteRound{{ $p->id }}" style="font-size:.9rem;display:inline-block;text-align: center;padding:5px 5px;background:rgba(171, 79, 85, .2);border-radius: .5rem;margin-top:10px;cursor:pointer;text-decoration: none;" class="text-danger">Delete Round?</a>	
		   							<div class="well collapse" id="deleteRound{{ $p->id }}" style="margin-top:10px;border:none;">
		   								<strong class="text-danger">Are you sure you want to delete this round?</strong><br>
		   								Please note, <span class="text-danger">This cannot be undone!</span><br>
		   								<a href="{{ URL::to('patient-directory/'.$p->id.'/delete') }}" class="btn btn-danger btn-xs">Confirm</a>
		   							</div>
		   							@endif
		   						</div>
		   						@endforeach
		   					</div>
		   				<hr style="opacity:.45;">
		   				<small class="text-muted">Patient record created: {{ date('m/d/Y', strtotime($patient->created_at)) }}</small>
		   				</div>
		   				<hr>
		   			</div>
		   			<div class="col-sm-6">
		   				<div class="form-group">
		   					{{ Form::label('patient_id', 'Patient ID') }} <span class="text-danger">*</span>
		   					<div class="input-group">
		   						<span class="input-group-addon">#</span>
		   						{{ Form::text('patient_id', $patient->patient_id, array('class' => 'form-control')) }}
		   					</div>
		   				</div>
		   			</div>
		   			<div class="col-sm-6">
		   				<div class="form-group">
		   					{{ Form::label('non_english', 'Non-english Speaker') }}
		   					{{ Form::text('non_english', $patient->non_english, ['class' => 'form-control']) }}
		   				</div>
		   			</div>
					{{-- <div class="col-sm-4">
						<div class="form-group">
							{{ Form::label('repeat', 'Is this a recurring patient?') }}
							<select name="repeat" id="repeat" class="form-control">
								<option value="0" {{ $patient->repeat == 0 ? 'selected' : null }}>N/A</option>
								@for($i = 1; $i <= 10; $i++)
									<option value="{{ $i }}" {{ $patient->repeat == $i ? 'selected' : null }}>{{ $i }} {{ $i == 1 ? 'time' : 'times' }}</option>
								@endfor
							</select>
						</div>
					</div> --}}
		   			<div class="clearfix"></div>
			   		<div class="col-sm-6">
			   			<div class="form-group">
							{{ Form::label('first_name', 'First Name') }} <span class="text-danger">*</span>
							{{ Form::text('first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control')) }}
						</div>
						<div class="form-group">
							{{ Form::label('last_name', 'Last Name') }} <span class="text-danger">*</span>
							{{ Form::text('last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control')) }}
						</div>
						{{ Form::label('cardiologist', 'Cardiologist') }}
				   		<div class="row">				   			
					   		<div class="col-sm-6">
					   			<div class="form-group">
									{{ Form::text('cardiologist_first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control')) }}
								</div>
					   		</div>
					   		<div class="col-sm-6">
					   			<div class="form-group">
									{{ Form::text('cardiologist_last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control')) }}
								</div>
					   		</div>
				   		</div> <!-- /.row .cardio-name -->
			   		</div>
			   		
			   		<div class="col-sm-6">
					   		
						<div class="form-group">								
							{{ Form::label('location_id', 'Treatment Location') }} <span class="text-danger">*</span>
							{{ Form::select('location_id', $locations, null, array('class' => 'form-control')) }}
						</div>
				   		
				   		<?php
				   			$completion = date('m/d/Y', strtotime($patient->treatment_completion));
				   		?>

				   		<div class="form-group">
					   		<label for="">Treatment Completion</label> <span class="text-danger">*</span><br />
					   		<input style="height:35px;" name="treatment_completion" value="{{$completion}}" type="text" placeholder="{{ date('m/d/Y') }}" disabled="" class="treatment_completion" />
				   		</div>
				   		
				   		<div class="form-group">
				   			<div class="media">
								<div class="pull-left">
									@if(!empty($patient->photo))
									<img width="70" src="/images/patients/{{$patient->photo}}" alt="">
									@else
									<img width="70" src="/images/patients/bg-generic-ppl.jpg" alt="">
									@endif
								</div>
								<div class="media-body">
									<label for="photo">Photo <small>Please upload a square image no smaller than 150x150 pixels in size</small></label>
									<input type="file" name="photo">
								</div>
							</div>
				   		</div>
				   		
				   		
			   		</div>				   		
			   		
			   		<div class="form-group col-sm-6">
			   			{{ Form::label('insurer_id', 'Primary Insurer') }}
						{{ Form::select('insurer_id', $insurers, null, array('class' => 'form-control')) }}
			   		</div>
			   		
			   		<div class="form-group col-sm-6">
			   			<label class="optional" for="">Secondary Insurer</label>
						{{ Form::select('secondary_insurer_id', $secondary_insurers, null, array('class' => 'form-control')) }}
			   		</div>

			   		<div class="form-group col-sm-12">
						{{ Form::label('address', 'Patient Street Address') }}
						{{ Form::text('address', null, array('placeholder' =>'Address', 'class' => 'form-control')) }}
					</div>

					<div class="form-group col-sm-6">
						{{ Form::label('city', 'City') }}
						{{ Form::text('city', null, array('placeholder' =>'City', 'class' => 'form-control')) }}
					</div>
					<?php
						$states = Config::get('constants.USA_STATES');
					?>

			   		<div class="form-group col-sm-3">
				   		{{ Form::label('state', 'State') }}
						{{ Form::select('state', $states, null, array('class' => 'form-control')) }}				  
			   		</div>

			   		<div class="form-group col-sm-3">
				   		{{ Form::label('zip', 'ZIP Code') }}
						{{ Form::text('zip', null, array('placeholder' =>'Zip', 'class' => 'form-control')) }}				  
			   		</div>

					<?php
						$dob = date('m/d/Y', strtotime($patient->date_of_birth));
					?>
			   		
			   		<div class="form-group col-sm-6">
				   		<label for="">Date of Birth</label>
				   		{{ Form::text('date_of_birth', $dob, array('placeholder' =>'00/00/0000', 'id' => 'dob', 'class' => 'dob')) }}
				   		
			   		</div>
			   		
					<div class="form-group col-sm-6">
				   		{{ Form::label('email', 'Email Address', array('class' => 'optional')) }}
				   		{{ Form::email('email', null, array('placeholder' => 'name@email.com')) }}
			   		</div>	
			   		
			   		<div class="form-group col-sm-6">
				   		{{ Form::label('phone', 'Phone') }} <span class="text-danger">*</span>
						{{ Form::input('phone', 'phone', null, array('placeholder' => '123.456.7890')) }} 
						<!-- DEV NOTE : TARUN USE JAVASCRIPT VALIDATION | PHONE NUMBER FORMAT SHOULD BE LIMITED TO ###.###.#### -->
			   		</div>
			   		
			   		<div class="form-group col-sm-6">
				   		<label class="optional" for="">Alternate Phone Number</label>
				   		{{ Form::input('phone', 'alternate_phone', null, array('placeholder' => '123.456.7890', 'class' => 'alt-phone')) }}
			   		</div>
			   		
			   		<div class="form-group col-sm-12">
						{{ Form::label('notes', 'Notes') }}						
						{{ Form::textarea('notes', null, array('placeholder' =>'enter your notes here', 'class' => 'form-control', 'rows' => '25')) }}
					</div>		   		
			   		<div class="clear"></div>
			   		<br /><br />
			   		<br /><br />
					<div class="form-group col-sm-4">
						{{ link_to('/patient-directory', 'Cancel',  array('class' => 'btn btn-danger')) }}
						{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
					</div>
			   		<div class="form-group col-sm-4" style="margin-top:25px;">
						<div class="btn-group patient-state pull-right" style="max-width:inherit;">
							<label for="patient-state1" class="btn btn-default patient-state active-patient <?php if($patient->patient_state == 1) echo 'active' ?>"><input type="radio" name="patient_state" id="patient-state1" value="1" <?php if($patient->patient_state == 1) echo 'checked' ?> /> Active</label>
							<label for="patient-state4" class="btn btn-default patient-state incorrect-patient <?php if($patient->patient_state == 4) echo 'active' ?>"><input type="radio" name="patient_state" id="patient-state4" value="4" <?php if($patient->patient_state == 4) echo 'checked' ?> /> Incorrect</label>
							<label for="patient-state2" class="btn btn-default patient-state archived-patient <?php if($patient->patient_state == 2) echo 'active' ?>"><input type="radio" name="patient_state" id="patient-state2" value="2" <?php if($patient->patient_state == 2) echo 'active' ?> /> Archived</label>
							
						</div>
						
						@if($patient->patient_expiry_date)

						@endif
			   		</div>
					<div class="form-group col-sm-4 text-right" style="margin-top:25px;">
						<div class="btn-group patient-state pull-right">
							<label for="" class="btn">Has Patient Expired?</label>
							<label for="patient-state3" class="btn btn-default expired-patient <?php if($patient->patient_state == 3) echo 'active' ?>"><input type="radio" name="patient_state" id="patient-state3" value="3" <?php if($patient->patient_state == 3) echo 'active' ?> /> Yes</label>
						</div>
						<div class="well deceased-date {{ $patient->patient_state != 3 ? 'hidden' : null }}" style="display:block;margin-top:20px;">
							<label for="">Expired Patient Notes</label>
							<textarea name="expiration_note" id="expiration_note" cols="30" rows="10" class="form-control">{{ $patient->expiration_note }}</textarea>
								
							<div class="clearfix"></div>
						</div>
					</div>
					@if(Auth::user()->access_level == 0 || Auth::user()->access_level == 9)
					<div class="form-group col-md-12">
						<a data-toggle="modal" data-target="#deletePatient" style="cursor:pointer;" class="text-{{ $patient->touchpoints()->count() == 0 ? 'danger' : 'muted' }}">Delete Patient</a>
					</div>
					@endif

			   		
			   		
		   		</div> <!-- /.row -->
		   		
	   		</div> <!-- /.col-sm-* -->
	   		
   		</div> <!-- ./row -->
   		
	{{ Form::close() }}
	<div class="row">
		
	</div>
	<div class="modal fade" id="deletePatient" tabindex="-1" role="dialog" aria-labelledby="deletePatient">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header modal-danger">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Delete {{ $patient->first_name.' '.$patient->last_name }}?</h4>
				</div>
				{{ Form::open(['route' => ['patient-directory.destroy', $patient->id], 'method' => 'DELETE']) }}
				<div class="modal-body">
					@if($patient->touchpoints()->count() == 0)
						<strong class="text-danger">PLEASE NOTE</strong><br>
						<strong>This action cannot be undone.</strong>
					@else
						{{ $patient->first_name.' '.$patient->last_name }} cannot be deleted because they have touchpoints assigned to them
					@endif
				</div>
				<div class="modal-footer">
					<a class="btn btn-default" data-dismiss="modal">Close</a>
					@if($patient->touchpoints()->count() == 0)
					<button type="submit" class="btn btn-danger">Delete Patient</button>
					@endif
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<!-- ///END DEV NOTE : PATIENT INFORMATION FORM -->

@stop

@section('script')
	<script>

		$('label.expired-patient').on('click', function(){
			$('.deceased-date').removeClass('hidden');
		});
		$('label.patient-state').on('click', function(){
			$('.deceased-date').addClass('hidden');
		});
	</script>
@stop