@extends('admin.layouts.master')

@section('title')
Patient Delete
@stop

@section('head')

@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/locations" class="btn btn-default btn-sm pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			<div class="alert alert-warning">
				<p>Are you sure you want to delete your <strong>{{$location->name}}</strong> location?</p>
			</div>
			{{ Form::model($location, array('route' => array('admin.locations.destroy', $location->id), 'method' => 'DELETE')) }}										
				<div class="form-group">
					{{ Form::label('name', 'Title *') }}
					{{ Form::text('name', null, array('placeholder' =>'Title', 'class' => 'form-control', 'disabled' => '')) }}
				</div>
				
				<div class="form-group pull-right">			
					{{ link_to('admin/locations', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				</div>

			{{ Form::close() }}
			
		</div>
	 </section> <!-- /.container -->
@stop

@section('script')

@stop