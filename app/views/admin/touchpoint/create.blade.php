@extends('admin.layouts.master')

@section('title')
TouchPoint Create
@stop

@section('head')
	
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<!-- <br />
			<a href="/patient-directory" class="btn btn-default btn-sm pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a> -->
			<br /><br />
		</div>
	</div>

	<!-- DEV NOTE : PATIENT INFORMATION FORM -->

	<div class="patient touchpoint-record">
   		<div class="row patient-content">
	   		<div class="col-sm-10 col-sm-offset-1">
	   			@if(Session::get('class'))
					<div class="alert {{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						@if($errors->any())
							<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
							</ul>
						@else
							{{ Session::get('success') }}
						@endif
					</div>
				@endif
		   		
		   		<div class="clear"></div>
		   		<div class="row">
			   		<div class="col-sm-6 patient-edit">
			   			<h3 class="date"><small style="color:#a1a1a1; font-size:13px;">Treatment completion date</small><br />{{ $patient->treatment_completion->format('F j, Y') }}</h3>

				   		<div class="row">
				   			@if(!empty($patient->photo))
							<img width="79" src="/images/patients/{{$patient->photo}}" alt="">
							@else
							<img class="col-sm-4" src="http://placehold.it/79&text=Profile" alt="" />
							@endif
					   		<div class="col-sm-8">
						   		<h3 class="patient-name">{{ $patient->fullName()}}</h3>
						   		<h4 class="date-of-birth">DOB: {{ $patient->date_of_birth->format('m-d-Y') }}</h4>
						   		<h4 class="cardiologist">Dr. {{ $patient->fullDoctorName() }}</h4>
						   		<h4 class="phone">{{ $patient->phone }}</h4>
						   		<h4 class="insurance">{{ $patient->insurer->name }}</h4>	
						   		{{ $patient->non_english ? '<h4 class="non-english"><i class="glyphicon glyphicon-comment"></i> '.$patient->non_english.'</h4>' : null }}
								{{ $patient->repeat > 0 ? '<h4 class="repeat">Patient has repeat '.$patient->repeat.($patient->repeat == 1 ? ' time' : ' times').'</h4>' : null }}
						   		<a target="_blank" href="{{ URL::to('/patient-directory/'.$patient->id.'/edit') }}">edit patient info</a>
					   		</div>
				   		</div>
			   		</div>
			   		<div class="col-sm-6 touchpoints">
			   			<h3 class="date"><small style="color:#a1a1a1; font-size:13px;">TouchPoint will be recorded on</small><br />{{ $now = Carbon\Carbon::now()->toFormattedDateString(); }}</h3>
				   		<h4>TouchPoints</h4>
				   		{{ $patient->progressCreate() }}
				   		@if(Patient::where('patient_id', $patient->patient_id)->where('created_at', '<', $patient->created_at)->first())
				   			<?php 
				   				$pastPatients = Patient::where('patient_id', $patient->patient_id)->where('created_at', '<', $patient->created_at)->orderby('created_at', 'desc')->get();
				   			?>
				   			<h5>Past Round Touchpoints</h5>
				   			@foreach($pastPatients as $pastPatient)
								<div>
									<em class="text-muted"><small>{{ date('m/d/Y', strtotime($pastPatient->created_at)) }}</small></em> 
									<div>{{ $pastPatient->progress() }}</div>
								</div>

				   			@endforeach
				   		@endif
				   		<div class="row">
				   			<div class="col-sm-12">
				   				{{ Form::open( array('route' => 'patient-directory.touchpoint.unable', 'id' => 'form-unable')) }}
						   			{{ Form::hidden('patient_id', $patient->id ) }}
									<button class="btn btn-warning btn-xs">Unable to Contact?</button>
								<br>
								{{ $patient->touchpoint_snooze > 0 ? $patient->first_name.' '.$patient->last_name.'  was contacted '.$patient->touchpoint_snooze.' '.($patient->touchpoint_snooze == 1 ? 'time' : 'times').' before' : null }}
						   		{{ Form::close() }}
				   			</div>
				   		</div> <!-- /.row -->

				   		
				   			
				   			
			   		</div>
		   		</div>
		   		<div class="clear"></div>

		   		{{ Form::open( array('route' => 'patient-directory.touchpoint.store', 'class' => '')) }}
		   			{{ Form::hidden('patient_id', $patient->id ) }}
		   			{{ Form::hidden('touchpoint_progress', '' ) }}

		   		<div class="row report">



			   		<div class="report-row">
				   		<div class="col-sm-5 form-group">
					   		<label for="#">On average, how many times per week do you have chest pain, chest tightness, or angina? This may also include episodes of shortness of breath or fatigue.</label>
				   		</div>
				   		<div class="col-sm-7 form-group">

				   			{{ Form::select('angina', Config::get('constants.ANGINA'), null, array('class' => 'form-control') ) }}

				   		</div>
				   		<div class="clear"></div>
			   		</div>

			   		<div class="report-row">
			   			<div class="col-sm-5 form-group">
				   			<label for="#">How severe would you rate your symptoms?</label>
			   			</div>
			   			<div class="col-sm-7 form-group">
			   				<div class="indicator hidden-sm hidden-xs">
				   				<small class="pull-left" style="padding-left:50px;">Not Severe</small>
				   				<small class="pull-right" style="margin-right:-40px;">Extremely Severe</small>
			   				</div>
				   			<div class="clear"></div>

				   			{{ Form::select('sympton_severity', Config::get('constants.SYMPTON_SEVERITY'), null, array('id' => 'sympton-severity', 'class' => 'form-control visible-sm visible-xs') ) }}

				   			<div class="btn-group hidden-sm hidden-xs symptom-buttons" data-toggle="buttons">
								<label class="btn btn-default"><input type="radio" name="sympton_severity" value="NA"/>N/A</label>
				   				@for($i = 1; $i <= 10; $i++)
				   				<label class="btn btn-default"><input type="radio" name="sympton_severity" value="{{ $i }}"/>{{ $i }}</label>
				   				@endfor
					   		</div>
							{{--<label class="btn btn-default symptom-na-button" style="font-weight: bold;"><input type="radio" name="sympton_severity" value="NA" style="display:none;"/>N/A</label>--}}

			   			</div>
				   		<div class="clear"></div>
			   		</div>

			   		<div class="report-row">
				   		<div class="col-sm-5 form-group">
					   		<label for="#">On average, how many times per week do you have to take nitroglycerin for your chest pain, chest tightness or angina?</label>
				   		</div>
				   		<div class="col-sm-7 form-group">
					   		<select name="nitro" id="nitro" class="form-control">
								<option value="NA">N/A</option>
					   			<?php for ($i=0; $i <= 30; $i++) : ?>

					   			<option value="<?= $i ?>"><?= $i ?></option>

					   			<?php endfor; ?>
					   		</select>
				   		</div>

				   		<div class="clear"></div>
			   		</div>


					<div class="report-row">
						<div class="col-sm-5 form-group">
							<label for="#">{{ $patient->in_the_last_date_range }}, how many times did you go to a hospital emergency room?</label>
						</div>
						<div class="col-sm-7 form-group">
							<div class="row">
								<div class="col-sm-3 form-group">
									<select name="emergency_number_visits" id="emergency_number_visits" class="form-control">
										<option value="NA">N/A</option>
										@for($i = 0; $i <= 14;$i++)
											<option value="{{ $i }}">{{ $i }} {{ $i == 0 || $i > 1 ? 'visits' : 'visit' }}</option>
										@endfor
									</select>
								</div>
								<div class="col-sm-9 form-group">
									<textarea name="emergency_visit_reason" id="emergency_visit_reason" cols="30" rows="5" placeholder="The reason for the visit(s)"></textarea>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>

					<div class="report-row">
						<div class="col-sm-5 form-group">
							<label for="#">How many different times did you stay in a hospital overnight or longer {{ strtolower($patient->in_the_last_date_range) }}?</label>
						</div>
						<div class="col-sm-7 form-group">
							<div class="row">
								<div class="col-sm-3 form-group">
									<select name="overnight_hospital_visits" id="overnight_hospital_visits" class="form-control">
										<option value="NA">N/A</option>
										@for($i = 0; $i <= 14;$i++)
											<option value="{{ $i }}">{{ $i }} {{ $i == 0 || $i > 1 ? 'visits' : 'visit' }}</option>
										@endfor
									</select>
								</div>
								<div class="col-sm-9 form-group">
									<textarea name="overnight_hospital_reasons" id="overnight_hospital_reasons" cols="30" rows="5" placeholder="The reason for the visit(s)"></textarea>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>

			   		{{--<div class="report-row">--}}
				   		{{--<div class="col-sm-5">--}}
					   		{{--<label for="#">Have you had an emergency room visit related to cardio?</label>--}}
				   		{{--</div>--}}
				   		{{--<div class="col-sm-7">--}}
					   		{{--<div class="btn-group" data-toggle="buttons">--}}
						   		{{--<label class="btn btn-default <?php if(Input::old('emergency_room_visit') == 'NA') { echo 'active';} ?>">{{ Form::radio('emergency_room_visit', 'NA') }}N/A</label>--}}
						   		{{--<label class="btn btn-default <?php if(Input::old('emergency_room_visit') == '1') { echo 'active';} ?>">{{ Form::radio('emergency_room_visit', '1') }}yes</label>--}}
						   		{{--<label class="btn btn-default <?php if(Input::old('emergency_room_visit') == '0') { echo 'active';} ?>">{{ Form::radio('emergency_room_visit', '0') }}no</label>--}}
					   		{{--</div>--}}
				   		{{--</div>--}}
				   		{{--<div class="clear"></div>--}}
			   		{{--</div>--}}

			   		{{--<div class="report-row">--}}
				   		{{--<div class="col-sm-5">--}}
					   		{{--<label for="#">Cardio Hospitalization?</label>--}}
				   		{{--</div>--}}
				   		{{--<div class="col-sm-7">--}}
					   		{{--<div class="row">--}}
						   		{{--<div class="col-sm-5 pull-left">--}}
							   		{{--<div class="btn-group" data-toggle="buttons">--}}
								   		{{--<label class="btn btn-default <?php if(Input::old('cardio_hospitalization') == 'NA') { echo 'active';} ?>">{{ Form::radio('cardio_hospitalization', 'NA') }}N/A</label>--}}
								   		{{--<label class="btn btn-default <?php if(Input::old('cardio_hospitalization') == '1') { echo 'active';} ?>">{{ Form::radio('cardio_hospitalization', '1') }}yes</label>--}}
								   		{{--<label class="btn btn-default <?php if(Input::old('cardio_hospitalization') == '0') { echo 'active';} ?>">{{ Form::radio('cardio_hospitalization', '0') }}no</label>--}}
							   		{{--</div>--}}
							   		{{--<br /><br />--}}
						   		{{--</div>--}}
						   		{{--<div class="col-sm-7 pull-left">--}}
							   		{{--<select name="cardio_hospitalization_total" id="cardio-hospice-amount" class="form-control">--}}

								   		{{--<?php for ($i=0; $i <= 100; $i++) : ?>--}}

							   			{{--<option value="<?= $i ?>"><?= $i ?></option>--}}

							   			{{--<?php endfor; ?>--}}

								   		{{--<!--NOTES TO MIGUEL: PLEASE ADD MORE NUMBERS...probably till 100 -->--}}
							   		{{--</select>--}}
						   		{{--</div>--}}
					   		{{--</div>--}}
				   		{{--</div>--}}
				   		{{--<div class="clear"></div>--}}
			   		{{--</div>--}}

						<div class="report-row">
							<div class="col-sm-5 form-group">
								<label for="#">{{ $patient->in_the_last_date_range }}, have you had a cardiac related event, such as a bypass or stent, or a heart attack?</label>
							</div>
							<div class="col-sm-7 form-group">
								<div class="row">
									<div class="col-sm-12 form-group">
										<label><input type="checkbox" name="cardiac_coronary" value="1"> {{ ucwords('coronary artery bypass') }}</label><br>
										<label><input type="checkbox" name="cardiac_pci" value="1"> {{ ucwords('PCI (stent)') }}</label><br>
										<label><input type="checkbox" name="cardiac_heart_attack" value="1"> {{ ucwords('myocardial infarction') }} (heart attack)</label>
									</div>
									<div class="col-sm-12 form-group">
										<textarea name="cardiac_notes" id="cardiac_notes" class="form-control" cols="30" rows="10" placeholder="Numbers and Notes"></textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="report-row">
							<div class="form-group col-sm-5">
								{{ Form::label('life_improved', 'Have you noticed any improvements in any other areas of your life since completing treatment?') }}
							</div>
							<div class="col-sm-7 form-group">
								{{ Form::textarea('life_improved', null, ['class' => 'form-control']) }}
							</div>
						</div>

				   		<div class="form-group col-sm-9">
							{{ Form::label('notes', 'Notes') }}
							{{ Form::textarea('notes', null, array('placeholder' =>'enter your notes here', 'class' => 'form-control', 'rows' => '10')) }}
						</div>
						<div class="form-group col-sm-3">
							{{ Form::label('interviewer_initial', 'Interviewer Initial') }}
							{{ Form::text('interviewer_initial', null, ['class' => 'form-control', 'maxlength' => 3, 'onkeyup' => 'this.value=this.value.toUpperCase()']) }}
						</div>

				   		<div class="col-sm-12">
				   			<div class="form-group pull-left">
				   				<div class="checkbox">
									<label>
										<input type="checkbox" name="skiptouchpoint" value="1"> <span class="btn btn-danger btn-xs">Skip this TouchPoint</span>
									</label>
								</div>
				   			</div>
				   			<div class="form-group pull-right">
								{{ link_to('clinic-locations', 'Cancel',  array('class' => 'btn btn-danger')) }}
								{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
							</div>
				   		</div>

		   		</div> <!-- /.row.reports -->
		   		{{ Form::close() }}
	   		</div> <!-- ./col-sm-10.col-sm-offset-1 -->
   		</div> <!-- /.row.patient-content -->
	</div> <!-- /.patient.touchpoint-record -->

	<!-- ///END DEV NOTE : PATIENT INFORMATION FORM -->

@stop

@section('script')
	<script>
	    $(document).ready(function(){
			$('.symptom-na-button').on('click', function(){
				$(this).addClass('active');
			});
			$('.symptom-buttons label').on('click', function(){
				$('.symptom-na-button').removeClass('active');
			});
	    	var filterGroup = $('div.btn-group');
	    	
	    	$(location).click(function(){
		    	$(filterGroup).text($(this).text());
	    	});

	    	$('#form-unable').submit(function() {
				var c = confirm('Are you sure you were not able to contact {{ $patient->fullName()}}?');
				return c; //you can just return c because it will be true or false
			});

			

	    });

		

    </script>
@stop