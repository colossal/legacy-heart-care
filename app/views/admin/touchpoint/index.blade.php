@extends('admin.layouts.master')

@section('title')
Patients
@stop

@section('head')

@stop

@section('content')
	<div class="row">
	  	<!--DEV NOTE : LOCATION FILTER
	  	- Selecting the location with filter patients to the specific location chosen-->

	  	<div class="col-sm-6">
			<br />
			<a href="/patient-directory/create" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i> Add Patient Record</a>
		</div>

   	  	{{ Form::open(array('route' => 'patient-directory.set', 'class' => 'filter pull-right col-sm-6')) }}
			<br />
			<div class="row">
	   			<div class="col-sm-6">
		   			<label class="pull-left" for="">Filter by Treatment Location:</label> 
	   			</div>
	   			<div class="col-sm-6">
	   				<div class="form-group">
	   					<?php
	   						$location_id = Session::get('location.id');
	   					?>
	   						
						{{ Form::select('location_id', $locations, $location_id, array('placeholder' =>'Categories', 'class' => 'form-control', 'id' => 'location_id')) }}
						
					</div>
	   			</div>
			</div>
			<br />
		{{ Form::close() }}
			<!-- ///END DEV NOTE : LOCATION FILTER -->
	</div>

	<section class="row">
		<div class="col-sm-12">
			
			@if( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('success') }}
				</div>
			@endif

			@if(!empty($patients[0]))

			<table class="table table-striped table-hover">
				<thead>
				<tr>
					<th colspan="2">Title</th>
					<th>Phone</th>
					<th>Cardiologist</th>
					<th>TouchPoint</th>
					<th>Actions</th>
				</tr>
				</thead>
				
				@foreach($patients as $patient)
					<tr>
						<td width="40">
							@if(!empty($patient->photo))
								<img src="/images/patients/{{$patient->photo}}" width="40" alt="">
							@else
								<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
							@endif
						</td>
						<td class="td_valign"><a href="/patient-directory/{{ $patient->id }}/edit">{{ $patient->first_name }} {{ $patient->last_name }}</a></td>
						<td class="td_valign">{{ $patient->phone }}</td>
						<td class="td_valign">Dr. {{ $patient->cardiologist_first_name }} {{ $patient->cardiologist_last_name }}</td>
						<td class="td_valign">60 Days</td>
						<td class="text-center">
							<a href="{{ URL::asset('patient-directory/'.$patient->id.'/edit') }}" class="btn btn-primary">View</a>
						</td>
					</tr>
				@endforeach
			</table>
			
			@if(!empty($patients[0]))
				<div class="text-right">
				{{ $patients->appends(Request::except('page', '_token'))->links() }}
				</div>
			@endif
			
			@else
					
				<div class="alert alert-info">No patients were found.</div>	
					
			@endif
			
		</div> <!-- /.col-sm-12 -->	
	 </section> <!-- /.container -->
@stop

@section('script')
	<script>
		$(function() {
			$('#location_id').change(function() {
				this.form.submit();
			});
		});
	</script>
@stop