@extends('admin.layouts.master')

@section('title')
TouchPoint
@stop

@section('head')
	
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<!-- <br />
			<a href="/patient-directory" class="btn btn-default btn-sm pull-right"><i class="glyphicon glyphicon-chevron-left"></i> Back</a> -->
			<br /><br />
		</div>
	</div>

	<!-- DEV NOTE : PATIENT INFORMATION FORM -->

	<div class="patient touchpoint-record">

		<?php
			$active = array('0' => '', '1' => 'active');
		?>

   		{{ Form::model($touchpoint) }}

   			{{ Form::hidden('patient_id', $patient->id ) }}
   			
   			{{ Form::hidden('touchpoint_progress', '' ) }}
   			
	   		<div class="row patient-content">
		   		
		   		<div class="col-sm-10 col-sm-offset-1">
			   		
		   			@if(Session::get('class'))
						<div class="alert {{ Session::get('class') }}">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							@if($errors->any())
								<ul>
								{{ implode('', $errors->all('<li class="error">:message</li>')) }}
								</ul>
							@else
								{{ Session::get('success') }}
							@endif
						</div>
					@endif

			   		<div class="clear"></div>
			   		
			   		<div class="row">
				   		
				   		<div class="col-sm-6 patient-edit">
   					   		<h3 class="date"><small style="color:#a1a1a1; font-size:13px;">Treatment completion date</small><br /> {{ $patient->treatment_completion->format('F j, Y') }}</h3>

					   		<div class="row">
					   			@if(!empty($patient->photo))
								<img width="79" src="{{ URL::asset('images/patients/'.$patient->photo) }}" alt="">
								@else
								<img class="col-sm-4" src="http://placehold.it/79&text=Profile" alt="" />
								@endif						   		

						   		<div class="col-sm-8">
							   		<h3 class="patient-name">{{ $patient->fullName()}}</h3>
							   		<h4 class="date-of-birth">DOB: {{ $patient->date_of_birth->format('m-d-Y') }}</h4>
							   		<h4 class="cardiologist">Dr. {{ $patient->fullDoctorName() }}</h4>
							   		<h4 class="phone">{{ $patient->phone }}</h4>
							   		<h4 class="insurance">{{ $patient->insurer->name }}</h4>
									{{ $patient->non_english ? '<h4 class="non-english"><i class="glyphicon glyphicon-comment"></i> '.$patient->non_english.'</h4>' : null }}
									{{ $patient->repeat > 0 ? '<h4 class="repeat">Patient has repeat '.$patient->repeat.($patient->repeat == 1 ? ' time' : ' times').'</h4>' : null }}
							   		<a target="_blank" href="{{ URL::to('/patient-directory/'.$patient->id.'/edit') }}">edit patient info</a>
						   		</div>
					   		</div>
					   		<br /><br />
				   		</div>
				   		<div class="col-sm-6 touchpoints">
			   			<h3 class="date"><small style="color:#a1a1a1; font-size:13px;">TouchPoint created on</small><br /> {{ $touchpoint->created_at->format('F j, Y') }}</h3>
						<h4>TouchPoints</h4>
				   		{{ $patient->progress() }}
				   		@if(Patient::where('patient_id', $patient->patient_id)->where('created_at', '<', $patient->created_at)->first())
				   			<?php 
				   				$pastPatients = Patient::where('patient_id', $patient->patient_id)->where('created_at', '<', $patient->created_at)->orderby('created_at', 'desc')->get();
				   			?>
				   			<h5>Past Round Touchpoints</h5>
				   			@foreach($pastPatients as $pastPatient)
								<div>
									<em class="text-muted"><small>{{ date('m/d/Y', strtotime($pastPatient->created_at)) }}</small></em> 
									<div>{{ $pastPatient->progress() }}</div>
								</div>

				   			@endforeach
				   		@endif
				   		<div class="row">
				   			
				   			<div class="col-sm-12">
								<a href="{{ $touchpoint->id }}/edit" class="btn btn-primary">Edit TouchPoint</a>
				   				@if($touchpoint->touchpoint_complete == 0)
				   					<p class="help-block">This TouchPoint was either skipped or we were not able to contact patient after 3 attempts.</p>
				   				@endif
				   			</div>
				   		</div> <!-- /.row -->


			   			</div>
			   		</div>
			   		<div class="clear"></div>
			   		<div class="row report">
				   		
				   		
				   		
				   		<div class="report-row">
					   		<div class="col-sm-5 form-group">
						   		<label for="#">On average, how many times per week do you have chest pain, chest tightness, or angina? This may also include episodes of shortness of breath or fatigue. </label>
					   		</div>
					   		<div class="col-sm-7 form-group">
					   			{{ Form::select('angina', Config::get('constants.ANGINA'), null, array('class' => 'form-control', 'disabled' => '') ) }}
					   		</div>
					   		<div class="clear"></div>
				   		</div>
				   		
				   		<div class="report-row">
				   			<div class="col-sm-5 form-group">
					   			<label for="#">How severe would you rate your symptoms?</label>
				   			</div>
				   			<div class="col-sm-7 form-group">
				   				<div class="indicator hidden-sm hidden-xs">
									<small class="pull-left" style="padding-left:50px;">Not Severe</small>
									<small class="pull-right" style="margin-right:-40px;">Extremely Severe</small>
				   				</div>
					   			<div class="clear"></div>
					   			
					   			{{ Form::select('sympton_severity', Config::get('constants.SYMPTON_SEVERITY'), null, array('id' => 'sympton-severity', 'class' => 'form-control visible-sm visible-xs', 'disabled' => '') ) }}

					   			<div class="btn-group hidden-sm hidden-xs" data-toggle="buttons">
							   		<label class="btn btn-default <?php if($touchpoint->sympton_severity == 'NA') { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="NA" />N/A</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 1) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="1" />1</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 2) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="2" />2</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 3) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="3" />3</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 4) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="4" />4</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 5) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="5" />5</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 6) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="6" />6</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 7) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="7" />7</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 8) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="8" />8</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 9) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="9" />9</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->sympton_severity === 10) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="sympton_severity" id="sympton-severity" value="10" />10</label>
						   		</div>
						   		
				   			</div>
					   		<div class="clear"></div>
				   		</div>
				   		
				   		<div class="report-row">
					   		<div class="col-sm-5 form-group">
						   		<label for="#">On average, how many times per week do you have to take nitroglycerin for your chest pain, chest tightness or angina?</label>
					   		</div>
					   		<div class="col-sm-7 form-group">
					   			<select name="nitro" id="nitro" class="form-control" disabled>
									<option value="NA" {{ $touchpoint->nitro == 'NA' ? 'checked' : null }}>N/A</option>
						   			<?php for ($i=0; $i <= 100; $i++) : if( $touchpoint->nitro == $i ) : ?>
						   			
						   				<option value="<?= $i ?>" selected><?= $i ?></option>

						   			<?php else : ?>

						   				<option value="<?= $i ?>"><?= $i ?></option>

						   			<?php endif; endfor; ?>						   		
							   		<!--NOTES TO MIGUEL: PLEASE ADD MORE NUMBERS...probably till 100 -->
						   		</select>

					   			
						   		
					   		</div>
					   		<div class="clear"></div>
				   		</div>
				   		


						@if(!((int)$touchpoint->emergency_room_visit || $touchpoint->emergency_room_visit == 'NA') && !((int)$touchpoint->cardio_hospitalization || $touchpoint->cardio_hospitalization == 'NA'))
							<div class="report-row">
								<div class="col-sm-5 form-group">
									<label for="#">{{ $touchpoint->in_the_last_date_range }}, how many times did you go to a hospital emergency room?</label>
								</div>
								<div class="col-sm-7 form-group">
									<div class="row">
										<div class="col-sm-3 form-group">
											<select name="emergency_number_visits" id="emergency_number_visits" class="form-control" disabled>
												<option value="NA" {{ $touchpoint->emergency_number_visits == 'NA' ? 'checked' : null }}>N/A</option>
												@for($i = 0; $i <= 14;$i++)
													<option value="{{ $i }}" {{ $touchpoint->emergency_number_visits == $i ? 'selected' : null }}>{{ $i }} {{ $i == 0 || $i > 1 ? 'visits' : 'visit' }}</option>
												@endfor
											</select>
										</div>
										<div class="col-sm-9 form-group">
											<textarea name="emergency_visit_reason" id="emergency_visit_reason" cols="30" rows="5" placeholder="The reason for the visit(s)" readonly>{{ $touchpoint->emergency_visit_reason }}</textarea>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>

							<div class="report-row">
								<div class="col-sm-5 form-group">
									<label for="#">How many different times did you stay in a hospital overnight or longer {{ strtolower($touchpoint->in_the_last_date_range) }}?</label>
								</div>
								<div class="col-sm-7 form-group">
									<div class="row">
										<div class="col-sm-3 form-group">
											<select name="overnight_hospital_visits" id="overnight_hospital_visits" class="form-control" disabled>
												<option value="NA" {{ $touchpoint->overnight_hospital_visits == 'NA' ? 'checked' : null }}>N/A</option>
												@for($i = 0; $i <= 14;$i++)
													<option value="{{ $i }}" {{ $touchpoint->overnight_hospital_visits == $i ? 'selected' : null }}>{{ $i }} {{ $i == 0 || $i > 1 ? 'visits' : 'visit' }}</option>
												@endfor
											</select>
										</div>
										<div class="col-sm-9 form-group">
											<textarea name="overnight_hospital_reasons" id="overnight_hospital_reasons" cols="30" rows="5" placeholder="The reason for the visit(s)" readonly> {{ $touchpoint->overnight_hospital_reasons }}</textarea>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						@endif

						@if((int)$touchpoint->emergency_room_visit || $touchpoint->emergency_room_visit == 'NA')
				   		<div class="report-row">
					   		<div class="col-sm-5 form-group">
						   		<label for="#">Have you had an emergency room visit related to cardio?</label>
					   		</div>
					   		<div class="col-sm-7 form-group">
						   		<div class="btn-group" data-toggle="buttons">
							   		<label class="btn btn-default <?php if($touchpoint->emergency_room_visit == 'NA') { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="emergency_room_visit" id="emergency-room-visit" value="NA" />N/A</label>
							   		<label class="btn btn-default <?php if((int)$touchpoint->emergency_room_visit === 1) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="emergency_room_visit" id="emergency-room-visit" value="1" />yes</label>
							   		<label class="btn btn-default <?php if(!is_null($touchpoint->emergency_room_visit)) {
							   											if((int)$touchpoint->emergency_room_visit === 0) { echo 'active';} else { echo 'disabled';}
							   										} else {
							   											echo 'disabled';
							   										} ?>">
							   										<input type="radio" name="emergency_room_visit" id="emergency_room_visit" value="0" />no</label>
						   		</div>
					   		</div>
					   		<div class="clear"></div>
				   		</div>
						@endif

						@if((int)$touchpoint->cardio_hospitalization || $touchpoint->cardio_hospitalization == 'NA')
				   		<div class="report-row">
					   		<div class="col-sm-5 form-group">
						   		<label for="#">Cardio Hospitalization?</label>
					   		</div>
					   		<div class="col-sm-7 form-group">
						   		<div class="row">
							   		<div class="col-sm-6 form-group pull-left">
								   		<div class="btn-group" data-toggle="buttons">
									   		<label class="btn btn-default <?php if($touchpoint->cardio_hospitalization == 'NA') { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="cardio_hospitalization" id="cardio-hospice" value="NA" />N/A</label>
									   		<label class="btn btn-default <?php if((int)$touchpoint->cardio_hospitalization === 1) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="cardio_hospitalization" id="cardio-hospice" value="1" />yes</label>
						   					<label class="btn btn-default <?php if(!is_null($touchpoint->cardio_hospitalization)) {
							   											if((int)$touchpoint->cardio_hospitalization === 0) { echo 'active';} else { echo 'disabled';}
							   										} else {
							   											echo 'disabled';
							   										} ?>">
							   										<input type="radio" name="cardio_hospitalization" id="cardio_hospitalization" value="0" />no</label>
								   		</div>
								   		<br /><br />
							   		</div>
							   		<div class="col-sm-6 form-group pull-left">

							   			<select name="cardio_hospitalization_total" id="cardio-hospice-amount" class="form-control" disabled>

								   			<?php for ($i=0; $i <= 100; $i++) : if( $touchpoint->cardio_hospitalization_total == $i ) : ?>
								   			
								   				<option value="<?= $i ?>" selected><?= $i ?></option>

								   			<?php else : ?>

								   				<option value="<?= $i ?>"><?= $i ?></option>

								   			<?php endif; endfor; ?>						   		
									   		<!--NOTES TO MIGUEL: PLEASE ADD MORE NUMBERS...probably till 100 -->
								   		</select>
							   			
							   		</div>
						   		</div>
					   		</div>
					   		<div class="clear"></div>
				   		</div>
				   		@endif


						@if($touchpoint->cardio_related_procedure || (int)$touchpoint->cardio_related_procedure)
							<div class="report-row">
								<div class="col-sm-5 form-group">
									<label for="#">Have you had a cardio-related Procedure?</label>
								</div>
								<div class="col-sm-7 form-group">
									<div class="btn-group" data-toggle="buttons">
										<label class="btn btn-default <?php if($touchpoint->cardio_related_procedure == 'NA') { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="cardio_related_procedure" id="cardio-procedure" value="NA" />N/A</label>
										<label class="btn btn-default <?php if((int)$touchpoint->cardio_related_procedure === 1) { echo 'active';} else { echo 'disabled';} ?>"><input type="radio" name="cardio_related_procedure" id="cardio-procedure" value="1" />yes</label>
										<label class="btn btn-default <?php if(!is_null($touchpoint->cardio_related_procedure)) {
											if((int)$touchpoint->cardio_related_procedure === 0) { echo 'active';} else { echo 'disabled';}
										} else {
											echo 'disabled';
										} ?>">
											<input type="radio" name="cardio_related_procedure" id="cardio_related_procedure" value="0" />no</label>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						@else
							<div class="report-row">
								<div class="col-sm-5 form-group">
									<label for="#">{{ $touchpoint->in_the_last_date_range }}, have you had a cardiac related event, such as a bypass or stent, or a heart attack?</label>
								</div>
								<div class="col-sm-7 form-group">
									<div class="row">
										<div class="col-sm-12">
											<label><input disabled type="checkbox" name="cardiac_coronary" value="1" {{ $touchpoint->cardiac_coronary == 1 ? 'checked' : null }}> {{ ucwords('coronary artery bypass') }}</label><br>
											<label><input disabled type="checkbox" name="cardiac_pci" value="1" {{ $touchpoint->cardiac_pci == 1 ? 'checked' : null }}> {{ ucwords('PCI (stent)') }}</label><br>
											<label><input disabled type="checkbox" name="cardiac_heart_attack" value="1" {{ $touchpoint->cardiac_heart_attack == 1 ? 'checked' : null }}> {{ ucwords('myocardial infarction') }} (heart attack)</label>
										</div>
										<div class="col-sm-12">
											<textarea name="cardiac_notes" id="cardiac_notes" class="form-control" cols="30" rows="10" placeholder="Numbers and Notes" readonly>{{ $touchpoint->cardiac_notes }}</textarea>
										</div>
									</div>
								</div>
							</div>
						@endif

						<div class="report-row">
							<div class="form-group col-sm-5">
								{{ Form::label('life_improved', 'Have you noticed any improvements in any other areas of your life since completing treatment?') }}
							</div>
							<div class="col-sm-7 form-group">
								{{ Form::textarea('life_improved', $patient->life_improved, ['class' => 'form-control']) }}
							</div>
						</div>

					   		<div class="form-group col-sm-9">
								{{ Form::label('notes', 'Notes') }}
								{{ Form::textarea('notes', null, array('placeholder' =>'enter your notes here', 'class' => 'form-control', 'rows' => '10', 'disabled' => '')) }}
							</div>
							<div class="form-group col-sm-3">
								{{ Form::label('interviewer_initial', 'Interviewer Initial') }}
								{{ Form::text('interviewer_initial', $touchpoint->interviewer_initial, ['class' => 'form-control', 'disabled' => true, 'readonly' => true]) }}
							</div>
				   		
			   		</div>
			   		
			   		
		   		</div>
		   		
	   		</div>
	   		
   		{{ Form::close() }}
	</div>

	<!-- ///END DEV NOTE : PATIENT INFORMATION FORM -->

@stop

@section('script')

<script>
	    $(document).ready(function(){
	    	var filterGroup = $('div.btn-group');
	    	
	    	$(location).click(function(){
		    	$(filterGroup).text($(this).text());
	    	});
	    });
    </script>
@stop