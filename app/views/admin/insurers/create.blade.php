@extends('admin.layouts.master')

@section('title')
Insurer Create 
@stop

@section('head')
	
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/insurers" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			{{ Form::open(array('route' => 'admin.insurers.store', 'class' => 'form-admin')) }}

				@if(Session::get('class'))
					<div class="alert {{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						@if($errors->any())
							<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
							</ul>
						@else
							{{ Session::get('success') }}
						@endif
					</div>
				@endif

				<div class="form-group">
					{{ Form::label('name', 'Name *') }}
					{{ Form::text('name', '', array('placeholder' =>'Name', 'class' => 'form-control')) }}
				</div>

				<?php
					$status = Config::get('constants.STATUS');
				?>
				<div class="form-group">								
					{{ Form::label('active', 'Status') }}
					{{ Form::select('active', $status, 0, array('placeholder' =>'Categories', 'class' => 'form-control')) }}
				</div>

				<div class="form-group pull-right">			
					{{ link_to('admin/insurers', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
				</div>

			{{ Form::close() }}
		</div>
		
	 </section> <!-- /.container -->
@stop

@section('script')
	
@stop