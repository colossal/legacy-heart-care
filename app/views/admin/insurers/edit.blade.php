@extends('admin.layouts.master')

@section('title')
Insurer Edit
@stop

@section('head')
	
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/insurers" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			{{ Form::model($insurer, array('route' => array('admin.insurers.update', $insurer->id), 'method' => 'PUT')) }}	

				@if(Session::get('class'))
					<div class="alert {{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						@if($errors->any())
							<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
							</ul>
						@else
							{{ Session::get('success') }}
						@endif
					</div>
				@endif

				<div class="form-group">
					{{ Form::label('name', 'Title *') }}
					{{ Form::text('name', null, array('placeholder' =>'Title', 'class' => 'form-control')) }}
				</div>
				<?php
					$status = Config::get('constants.STATUS');
				?>
				<div class="form-group">								
					{{ Form::label('active', 'Status') }}
					{{ Form::select('active', $status, null, array('placeholder' =>'Categories', 'class' => 'form-control')) }}
				</div>

				<div class="form-group pull-right">			
					{{ link_to('admin/insurers', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
				</div>

			{{ Form::close() }}
		</div>
	 </section> <!-- /.container -->
@stop

@section('script')

@stop