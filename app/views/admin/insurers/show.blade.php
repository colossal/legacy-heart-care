@extends('admin.layouts.master')

@section('title')
Delete Insurer
@stop

@section('head')

@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/insurers" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			<div class="alert alert-warning">
				<p>Are you sure you want to delete your <strong>{{$insurer->name}}</strong> insurer?</p>
			</div>
			{{ Form::model($insurer, array('route' => array('admin.insurers.destroy', $insurer->id), 'method' => 'DELETE')) }}										
				<div class="form-group">
					{{ Form::label('name', 'Title *') }}
					{{ Form::text('name', null, array('placeholder' =>'Title', 'class' => 'form-control', 'disabled' => '')) }}
				</div>
				
				<div class="form-group pull-right">			
					{{ link_to('admin/insurers', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				</div>

			{{ Form::close() }}
			
		</div>
	 </section> <!-- /.container -->
@stop

@section('script')

@stop