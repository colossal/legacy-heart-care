@include('admin.layouts.partials.header')

  <!-- DEV NOTE : MAIN HEADER -->
    <header id="main">
      <!-- DEV NOTE : TOP BAR NAVIGATION -->  
      <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('/') }}">TouchPoint</a>
          </div>
          
          <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav">
            </ul>
            
          </div>
          
      </div>
      <div class="banner">
        <div class="container">
          <h1 class="title"><!-- DEV NOTE : TITLE OF PAGE --> @yield('title') </h1>
          <div class="profile">
          </div>
        </div>
      </div>
    </nav>
      <!-- ///END DEV NOTE : TOP BAR NAVIGATION -->
      
      
    </header>
    <!-- ///END DEV NOTE : MAIN HEADER -->

   	<section id="main">
	   	<div class="container">
	   		<div class="row global-date">
				<div class="col-sm-12">
					<h3 class="date"><small>Current Date</small> <!-- DEV NOTE : ENTER CURRENT DATE HERE --> {{ $now = Carbon\Carbon::now()->toFormattedDateString(); }}</h3>
				</div>
			</div>

	   		@yield('content')	  
		   	
	   	</div> <!-- /.container -->
   	</section> <!-- /#main -->
<footer id="main">
	<nav class="navbar navbar-inverse navbar-static-bottom">
		<div class="container">
		   	<p>Copyright &copy; {{ date('Y') }} Legacy Heart Care, LLC. All Rights Reserved. &nbsp;&nbsp;&nbsp;&nbsp; v2.0</p>
		</div>
	</nav>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ URL::asset('library/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('library/js/jquery.plugin.js') }}"></script>
<script src="{{ URL::asset('library/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('library/js/jquery.maskedinput.min.js') }}"></script>

	@yield('script')
	
@include('admin.layouts.partials.subfooter')