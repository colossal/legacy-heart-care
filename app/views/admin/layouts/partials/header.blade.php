<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TouchPoint | @yield('title', 'Welcome')</title>

    <!-- Bootstrap -->
    <link href="{{ strpos(URL::current(), 'localhost') ? asset('library/css/bootstrap.css') : secure_asset('library/css/bootstrap.css') }}" rel="stylesheet">
    
    <link href="{{ strpos(URL::current(), 'localhost') ? asset('library/css/datepicker.css') : secure_asset('library/css/datepicker.css') }}" rel="stylesheet">
    <link href="{{ strpos(URL::current(), 'localhost') ? asset('library/css/password.css') : secure_asset('library/css/password.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="shortcut icon" type="image/ico" href="favicon.ico"/>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('head')
  </head>
  <body>