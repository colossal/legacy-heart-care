<footer id="main">
	<nav class="navbar navbar-inverse navbar-static-bottom">
		<div class="container">
		   	<p>Copyright &copy; <?= date('Y') ?> Legacy Heart Care, LLC. All Rights Reserved. &nbsp;&nbsp;&nbsp;&nbsp; v2.0</p>
		   	<ul class="footer-nav">
		   		<!--
		   		DEV NOTE : HELP HAS BEEN HIDDEN UNTIL FURTHER USE
	   			<li>Help</li>
	   			-->
		   		@if(Auth::check())
				@if(Auth::user()->access_level == 0 || Auth::user()->access_level == 9)
				<li><a href="{{ URL::to('admin') }}">Settings</a></li>
				@endif
				<li><a href="{{ URL::to('logout') }}">Logout</a></li>
		   		@endif 	
		   	</ul>
		</div>
	</nav>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/bootstrap.min.js') : secure_asset('library/js/bootstrap.min.js') }}"></script>
<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/jquery.plugin.js') : secure_asset('library/js/jquery.plugin.js') }}"></script>
<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/bootstrap-datepicker.js') : secure_asset('library/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/jquery.maskedinput.min.js') : secure_asset('library/js/jquery.maskedinput.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#treatment_completion').datepicker()
		$('.datepickerfield').datepicker()
		$('input[type="phone"]').mask('999.999.9999');
		$('input#dob').mask('99/99/9999');
		
		$('div.patient-state label').click(function(){
			$('div.patient-state label').removeClass('active');
			$(this).addClass('active');
		});
		
	});
</script>