  <!-- DEV NOTE : MAIN HEADER -->
    <header id="main">
      <!-- DEV NOTE : TOP BAR NAVIGATION -->  
      <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('dashboard') }}">TouchPoint</a>
          </div>
          
          <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="{{ set_active('dashboard') }} {{ set_active('clinic-locations') }}"><a href="{{ URL::to('dashboard') }}">Clinic Locations</a></li>
              <li class="{{ set_active('reports*') }}"><a href="{{ URL::to('reports') }}">Reports</a></li>
              <li class="{{ set_active('patient-directory*') }}"><a href="{{ URL::to('patient-directory') }}">Patient Directory</a></li>
            </ul>
            
            <!-- DEV NOTE : PATIENT SEARCH FORM -->
            {{ Form::open(array('url' => 'search', 'class' => 'navbar-form navbar-right', 'method' => 'GET')) }}
              <div class="form-group">
                {{ Form::text('q', (isset($_GET['q']) ? $_GET['q'] : ''), array('class' => 'form-control', 'placeholder' => 'Search Name or ID', 'autocomplete' => 'off')) }}
              </div>
              {{ Form::submit('Search', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
            <!-- ///END DEV NOTE : PATIENT SEARCH FORM -->
          </div>
          
      </div>
      <div class="banner">
        <div class="container">
          <h1 class="title"><!-- DEV NOTE : TITLE OF PAGE --> @yield('title') </h1>
          <div class="profile">
            <h2>Welcome, <!-- DEV NOTE : USER LOGIN NAME -->{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h2>
            <a href="{{ URL::to('logout') }}" class="logout">logout</a>
          </div>
        </div>
      </div>
    </nav>
      <!-- ///END DEV NOTE : TOP BAR NAVIGATION -->
      
      
    </header>
    <!-- ///END DEV NOTE : MAIN HEADER -->