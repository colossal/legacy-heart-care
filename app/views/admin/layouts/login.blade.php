
@include('admin.layouts.partials.header')
   	
   	<header id="main" class="home">
	   	<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
		  <div class="container">
			  <div class="navbar-header">
			      <a class="navbar-brand" href="/demo/"></a>
		      </div>
		      
		      <div class="collapse navbar-collapse">
			      <ul class="nav navbar-nav">
				      
			      </ul>
		      </div>
		      
		  </div>
		  <div class="banner">
		   	<div class="container">
			   	@yield('content')


		   	</div>
	   	</div>
		</nav>
	   	
   	</header>
   	<div class="clear"></div>
	<section id="main" class="home">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<img class="lhc-logo" src="library/img/logo-lhc.png" alt="" />
				</div>
				<div class="col-sm-11">
					<h1 class="tagline">
						Our commitment to care for the health of our patients continues beyond treatment. TouchPoint <nobr>is that</nobr> connection.
					</h1>
				</div>
			</div>
		</div>
	</section>

@include('admin.layouts.partials.footer')

	@yield('scripts')
	
@include('admin.layouts.partials.subfooter')