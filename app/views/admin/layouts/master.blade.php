@include('admin.layouts.partials.header')

@include('admin.layouts.partials.navigation')

   	<section id="main">
	   	<div class="container">
	   		<div class="row global-date">
				<div class="col-sm-12">
					<h3 class="date"><small>Current Date</small> <!-- DEV NOTE : ENTER CURRENT DATE HERE --> {{ $now = Carbon\Carbon::now()->toFormattedDateString(); }}</h3>
				</div>
			</div>

	   		@yield('content')	  
		   	
	   	</div> <!-- /.container -->
   	</section> <!-- /#main -->
<div class="modal fade" id="inactiveUser" tabindex="-1" role="dialog" aria-labelledby="inactiveUser" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Due to inactivity you are about to be logged out</h4>
			</div>
			<div class="modal-body">
				You will be logged out in <strong class="counter">59</strong> seconds.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close-modal" data-dismiss="modal">Stay Logged In</button>
				<a href="{{ URL::to('logout') }}" class="btn btn-danger modal-logout">Log Out</a>
			</div>
		</div>
	</div>
</div>
@include('admin.layouts.partials.footer')

	@yield('script')
<script>
	//Detect Inactivity
	$(document).ready(function () {
		var idleState = false;
		var idleTimer = null;
		var logoutURL = "{{ URL::to('logout') }}";
		var t1 = '';
		var t2 = '';
		idleTimer = setTimeout(function () {
			t1 = new Date();
			idleState = true; }, 1740000);
		$('#inactiveUser button.close-modal').on('click', function(){
			clearTimeout(idleTimer);
			idleState = false;
			idleTimer = setTimeout(function () {
				t1 = new Date();
				idleState = true;
			}, 5000);
		});
		$('*').bind('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick', function () {
			if(idleState == false) {
				clearTimeout(idleTimer);
				idleTimer = setTimeout(function () {
					t1 = new Date();
					idleState = true; }, 1740000);
			}
		});

		$(window).on('scroll', function(){
			if(idleState == false) {
				clearTimeout(idleTimer);
				idleTimer = setTimeout(function () {
					t1 = new Date();
					idleState = true; }, 1740000);
			}
		});
		setInterval(function(){
			if (idleState == true) {
				$('#inactiveUser').modal('show');
				t2 = new Date();
				var diff = 60 - Math.round((t2.getTime() - t1.getTime()) / 1000);
					if(diff >= 0) {
						$('#inactiveUser strong.counter').text(diff);
					} else {
						location.replace(logoutURL);
					}
			}
		}, 1000);
		$("body").trigger("mousemove");
	});
</script>
	
@include('admin.layouts.partials.subfooter')