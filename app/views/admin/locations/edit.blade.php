@extends('admin.layouts.master')

@section('title')
Location Edit
@stop

@section('head')
	
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/locations" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			{{ Form::model($location, array('route' => array('admin.locations.update', $location->id), 'method' => 'PUT', 'files' => true)) }}	

				@if(Session::get('class'))
					<div class="alert {{ Session::get('class') }}">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						@if($errors->any())
							<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
							</ul>
						@else
							{{ Session::get('success') }}
						@endif
					</div>
				@endif

				<div class="form-group">
					{{ Form::label('name', 'Title *') }}
					{{ Form::text('name', null, array('placeholder' =>'Title', 'class' => 'form-control')) }}
				</div>

				<div class="form-group">
					<div class="media">
						<div class="pull-left">
							@if(!empty($location->photo))
							<img width="220" src="/images/locations/{{$location->photo}}" alt="">
							@else
							<img width="220" src="http://placehold.it/220" alt="">
							@endif
						</div>
						<div class="media-body">
							<br />
							<label for="photo">Photo</label>
							<input type="file" name="photo">
							<p class="help-block">390px x 180px image size.</p>
						</div>
					</div>
				</div>

				<div class="form-group">								
					{{ Form::label('active', 'Status') }}
					{{ Form::select('active', Config::get('constants.STATUS'), null, array('placeholder' =>'Categories', 'class' => 'form-control')) }}
				</div>

				<div class="form-group">
					{{ Form::label('description', 'Description') }}
					{{ Form::textarea('description', null, array('placeholder' =>'Description', 'class' => 'form-control')) }}
				</div>

				<div class="form-group pull-right">			
					{{ link_to('admin/locations', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
				</div>

			{{ Form::close() }}
		</div>
	 </section> <!-- /.container -->
@stop

@section('script')

@stop