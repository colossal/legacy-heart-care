@extends('admin.layouts.master')

@section('title')
Locations
@stop

@section('head')

@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back to Admin</a>
			<a href="/admin/locations/create" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i> Create Location Record</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			@if( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('success') }}
				</div>
			@endif

			@if(!empty($locations[0]))

			<table class="table table-striped table-hover">
				<thead>
				<tr>
					<th width="48%">Title</th>
					<th>Status</th>
					<th>Actions</th>
				</tr>
				</thead>
				<?php
					$status = Config::get('constants.STATUS');
				?>
				
				@foreach($locations as $location)
					<tr>
						<td class="td_valign"><a href="/admin/locations/{{ $location->id }}/edit">{{ $location->name }}</a></td>
						<td class="td_valign">{{ $status[$location->active] }}</td>
						<td class="text-center">
							<a href="/admin/locations/{{ $location->id }}/edit" class="btn btn-info btn-sm col-sm-3 col-sm-push-3"><i class="glyphicon glyphicon-pencil"></i></a>
							<a href="/admin/locations/{{ $location->id }}" class="btn btn-danger btn-sm col-sm-3 col-sm-push-4"><i class="glyphicon glyphicon-trash"></i></a>	
						</td>
					</tr>
				@endforeach

			</table>
			@else
					
				<div class="alert alert-info">No locations were found.</div>	
					
			@endif
			
		</div> <!-- /.col-sm-12 -->	
	 </section> <!-- /.container -->
@stop

@section('script')

@stop