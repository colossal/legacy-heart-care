@extends('admin.layouts.master')

@section('title')
Delete Location
@stop

@section('head')
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"  rel='stylesheet' type='text/css'>
	<link href="/css/summernote.css" rel='stylesheet' type='text/css' />
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/locations" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			<div class="alert alert-warning">
				<p>Are you sure you want to delete your <strong>{{$location->name}}</strong> location?</p>
			</div>
			{{ Form::model($location, array('route' => array('admin.locations.destroy', $location->id), 'method' => 'DELETE')) }}										
				<div class="form-group">
					{{ Form::label('name', 'Title *') }}
					{{ Form::text('name', null, array('placeholder' =>'Title', 'class' => 'form-control', 'disabled' => '')) }}
				</div>
				
				<div class="form-group pull-right">			
					{{ link_to('admin/locations', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				</div>

			{{ Form::close() }}
			
		</div>
	 </section> <!-- /.container -->
@stop

@section('script')
<script src="/js/vendor/summernote.min.js"></script>
@stop