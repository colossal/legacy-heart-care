@extends('admin.layouts.master')

@section('title')
User Delete
@stop

@section('head')
	
@stop

@section('content')
	
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="/admin/users" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>

	<section class="row">
		<div class="col-sm-12">
			<br />
			<div class="alert alert-warning">
				<p>Are you sure you want to delete <strong>{{$user->fullName()}}</strong>?</p>
			</div>
			{{ Form::model($user, array('route' => array('admin.users.destroy', $user->id), 'method' => 'DELETE')) }}										
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
						</ul>
					</div>
				@endif

				<div class="col-sm-6"> 
					<div class="form-group">
						{{ Form::label('first_name', 'First Name *') }}
						{{ Form::text('first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control')) }}
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						{{ Form::label('last_name', 'Last Name *') }}
						{{ Form::text('last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control')) }}
					</div>
				</div>

				<div class="col-sm-6"> 
					<div class="form-group">
						{{ Form::label('email', 'Email *') }}
						{{ Form::text('email', null, array('placeholder' =>'Email', 'class' => 'form-control')) }}
					</div>
				</div>

				<div class="form-group pull-right">			
					{{ link_to('admin/users', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Delete', array('class' => 'btn btn-warning')) }}
				</div>

			{{ Form::close() }}
					
		</div>
		
	 </section> <!-- /.container -->
@stop

@section('script')

@stop