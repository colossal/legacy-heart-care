@extends('admin.layouts.master')

@section('title')
Admins
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="{{ URL::to('admin') }}" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back to Admin</a>
			<a href="{{ URL::to('admin/users/create') }}" class="btn btn-default"><i class="glyphicon glyphicon-plus-sign"></i> Create User Record</a>
		</div>
	</div>
	<section class="row">

		<div class="col-sm-12">
			<br />
			@if( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('success') }}
				</div>
			@endif

			@if(!empty($users[0]))

			<div class="table-responsive">
				<table class="table table-striped table-hover">
				<thead>
				<tr>
					<th width="45%">Name</th>
					<th>Status</th>
					@if(Auth::user()->access_level == 9 && Auth::user()->active)
					<th>API Key</th>
					@endif
					<th>Access Level</th>
					<th>Locked Out</th>
					<th>Actions</th>
				</tr>
				</thead>
				<?php
					$status = Config::get('constants.STATUS');
				?>
				
				@foreach($users as $item)
					<tr>
						<td class="td_valign"><a href="/admin/users/{{ $item->id }}/edit">{{ $item->first_name }} {{ $item->last_name }}</a></td>
						<td class="td_valign">{{ $status[$item->active] }}</td>
						@if(Auth::user()->access_level == 9 && Auth::user()->active)
						<td style="vertical-align: middle;padding:0;position: relative;">
							@if((Auth::user()->id == $item->id || (Auth::user()->access_level == 9 && Auth::user()->active)) && APIKey::where('user_id' ,$item->id)->where('active', 1)->first())
								<code class="text-primary" style="display:inline-block;padding:10px;background:#FFF;">
											{{ APIKey::where('user_id' ,$item->id)->where('active', 1)->first()->key }}
										</code>
								@endif
						</td>
						@endif
						<td class="td_valign">{{ $item->access_level_label }}</td>
						<td class="td_valign">
							@if($item->locked == 1)
								@if($item->restore == 1)
								<a data-toggle="modal" data-target="#unlockUser{{ $item->id }}" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-lock"></i> Pending Password Reset</a>
								@else
								<a data-toggle="modal" data-target="#unlockUser{{ $item->id }}" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-lock"></i> Locked Out</a>
								@endif
							@endif
							<!-- Modal -->
							<div class="modal fade" id="unlockUser{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="unlockUser{{ $item->id }}">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title" id="myModalLabel">{{ $item->first_name.' '.$item->last_name }} Is Locked Out</h4>
							      </div>
							      <div class="modal-body">
										<p>
								        	This user will, on the next attempted login, be redirected to a password reset page. You can also share the following link via email to the user so that they can directly visit this page.
								        </p>
										<br>
										<span class="text-muted">Password Reset Page</span><br>
										<strong>{{ URL::to('password/reset') }}</strong>
							      </div>
							      <div class="modal-footer">
							      	@if($item->restore == 1)
							      	<div class="alert alert-info">
							      		The user is locked until they reset their password <button class="btn btn-default" data-dismiss="modal">Close</button>
							      	</div>							      	
							      	@else
							      	{{ Form::open(['route' => ['admin.users.unlock', $item->id]]) }}
							        
							        <button type="submit" class="btn btn-primary">Unlock User</button>
							        {{ Form::close() }}
							      	@endif
							      	
							      </div>
							    </div>
							  </div>
							</div>
						</td>
						<td class="text-center">
							<nobr>
								<a href="{{ URL::to('admin/users/'.$item->id.'/edit') }}" style="display: inline-block" class="btn btn-info btn-sm"><i class="glyphicon glyphicon-pencil"></i></a>
							<a href="{{ URL::to('admin/users/'.$item->id) }}" style="display: inline-block" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></a>
							</nobr>
						</td>
					</tr>
				@endforeach
													
				
			</table>
			</div>

			@else
				<div class="alert alert-info">No users were found.</div>
			@endif			
					
		</div> <!-- /.col-sm-12 -->
		
		
	 </section> <!-- /.row -->
@stop

@section('script')
<script>
	$('.delete').click( function (event) {
		var item = $(this);
		//bootbox.confirm("Are you sure delete?", function(result) {
		//	if(result){
		item.closest('tr').animate(
			{ opacity: 0 }, {
				duration: 400,
				easing: 'easeOutExpo',
				complete:  function() {
					item.closest('tr').remove();
				} 
			});
		//	}
		//});
				
		event.preventDefault();
	});
</script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="/js/vendor/jquery.ui.touch-punch.min.js"></script>
<script>
	$(function () {
		$(".list-group").sortable({
		    tolerance: 'pointer',
		    revert: 'invalid',
		    placeholder: 'placeholder-item',
		    forceHelperSize: true,

		    update: function(event, ui) {
		        order = $('.list-group').sortable("toArray");
		        $.ajax({
		        	type: "POST",
		            url: URL.base + '/admin/manage/staff/sort',
		            data: {order:order},
		            success: function(data)
					{
						console.log(data);
					}                     
		        });
		    }
		});
	});
</script>
<script src="/js/vendor/holder.js"></script>
@stop
