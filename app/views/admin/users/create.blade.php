@extends('admin.layouts.master')

@section('title')
User Create
@stop

@section('head')
	<link rel="stylesheet" href="{{ URL::asset('library/css/password.css') }}">
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="{{ URL::to('admin/users') }}" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		
		<div class="col-sm-12">	
			<br />
			{{ Form::open(array('url' => 'admin/users', 'class' => 'form-admin', 'files' => true)) }}

				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
						</ul>
					</div>
				@endif

				<div class="row">
					<div class="col-sm-12">
						<div class="form-group password-rules hidden">
							<div class="well">
								<strong>Your password must</strong><br>
								be a minimum of 10 characters <br>
								contain at least 1 uppercase character <em>(A-Z)</em><br>
								contain at least 1 numeral <em>(0-9)</em><br>
								container a special character <em>(ex: @$%)</em>

							</div>
						</div>
					</div>
					<div class="col-sm-6"> 
						<div class="form-group">
							{{ Form::label('first_name', 'First Name *') }}
							{{ Form::text('first_name', null, array('placeholder' =>'First Name', 'class' => 'form-control')) }}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('last_name', 'Last Name *') }}
							{{ Form::text('last_name', null, array('placeholder' =>'Last Name', 'class' => 'form-control')) }}
						</div>
					</div>

					<div class="col-sm-6"> 
						<div class="form-group">
							{{ Form::label('email', 'Email *') }}
							{{ Form::text('email', null, array('placeholder' =>'Email', 'class' => 'form-control')) }}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('password', 'Password') }}
							<input type="password" id="password" name="password" class="form-control" />
							
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('access_level', 'Access Level') }}
							<select name="access_level" class="form-control">
								<option value="0">Administrator</option>
								<option value="1">User</option>
								<option value="9">Super Administrator</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('active', 'status') }}
							<select name="active" class="form-control">
								<option value="0">Inactive</option>
								<option value="1">Active</option>
							</select>
						</div>
					</div>
					
				</div>

				

				<div class="form-group pull-right">			
					{{ link_to('admin/users', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Create', array('class' => 'btn btn-primary')) }}
				</div>

			{{ Form::close() }}
					
		</div>
		
	 </section> <!-- /.container -->
@stop

@section('script')
	<script src="/js/vendor/summernote.min.js"></script>
	<script src="/js/vendor/holder.js"></script>
	<script src="{{ URL::asset('library/js/password.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('#password').password({
				minimumLength: 10,
				showText: true, // shows the text tips
				animate: true, // whether or not to animate the progress bar on input blur/focus
				animateSpeed: 'fast', // the above animation speed
			});
			$(window).on('keyup', function(){
				var passwordLength = $('#password').val().length;
				if($('#password').val().length > 0) {
					$('.password-rules').removeClass('hidden');
					$('#password').on('password.score', function(e, score){
						if(score >= 68) {
							$('input[type="submit"]').removeClass('disabled');
						} else {
							$('input[type="submit"]').addClass('disabled');
						}
					});
				} else {
					$('.password-rules').addClass('hidden');
					$('input[type="submit"]').removeClass('disabled');
				}
			});
		});
	</script>
@stop