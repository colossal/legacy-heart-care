@extends('admin.layouts.master')

@section('title')
User Edit
@stop

@section('head')
	<link rel="stylesheet" href="{{ secure_asset('library/css/password.css') }}">
@stop

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<br />
			<a href="{{ URL::to('admin/users') }}" class="btn btn-default"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
		</div>
	</div>
	<section class="row">
		<div class="col-sm-12">
			<br />
			@if( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('success') }}
				</div>
			@endif
			{{ Form::model($user, array('route' => array('admin.users.update', $user->id), 'method' => 'PUT', 'files' => true)) }}								
				@if($errors->any())
					<div class="alert alert-danger">
						<ul>
							{{ implode('', $errors->all('<li class="error">:message</li>')) }}
						</ul>
					</div>
				@endif

				<div class="row">
					@if((Auth::user()->id == $user->id || (Auth::user()->access_level == 9 && Auth::user()->active)) && APIKey::where('user_id' ,$user->id)->where('active', 1)->first())
					<div class="col-md-12">
						<div class="alert alert-info" style="overflow:hidden">
							<strong>API KEY Linked to this user:</strong><br> 
							<code class="text-left text-info" style="background:#FFF;padding:10px;box-sizing: border-box;display: inline-block;word-wrap: break-word;width:100%;overflow-x:scroll">
								{{ APIKey::where('user_id' ,$user->id)->where('active', 1)->first()->key }}
							</code>
							<br>
							<br>
							<strong>API URL with key:</strong><br> 
							<code class="text-left text-info" style="background:#FFF;padding:10px;box-sizing: border-box;display: inline-block;word-wrap: break-word;width:100%;overflow-x:scroll">
								{{ URL::to('reports/api?apikey='.APIKey::where('user_id' ,$user->id)->where('active', 1)->first()->key) }}
							</code>
						</div>
					</div>
					@endif
					<div class="col-sm-12">
						<div class="form-group password-rules hidden">
							<div class="well">
								<strong>Your password must</strong><br>
								be a minimum of 10 characters <br>
								contain at least 1 uppercase character <em>(A-Z)</em><br>
								contain at least 1 numeral <em>(0-9)</em><br>
								container a special character <em>(ex: @$%)</em>

							</div>
						</div>
					</div>
					<div class="col-sm-6"> 
						<div class="form-group">
							{{ Form::label('first_name', 'First Name *') }}
							{{ Form::text('first_name', $user->first_name, array('placeholder' =>'First Name', 'class' => 'form-control')) }}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('last_name', 'Last Name *') }}
							{{ Form::text('last_name', $user->last_name, array('placeholder' =>'Last Name', 'class' => 'form-control')) }}
						</div>
					</div>

					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('email', 'Email *') }}
							{{ Form::text('email', $user->email, array('placeholder' =>'Email', 'class' => 'form-control')) }}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('password', 'Password') }}
							<input type="password" id="password" name="password" class="form-control" value="" />
							
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('access_level', 'Access Level') }}
							<select name="access_level" class="form-control">
								<option value="0" {{ $user->access_level == 0 ? 'selected' : null }}>Administrator</option>
								<option value="1" {{ $user->access_level == 1 ? 'selected' : null }}>User</option>
								<option value="9" {{ $user->access_level == 9 ? 'selected' : null }}>Super Administrator</option>
							</select>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							{{ Form::label('active', 'status') }}
							<select name="active" class="form-control">
								<option value="0" {{ $user->active == 0 ? 'selected' : null }}>Inactive</option>
								<option value="1" {{ $user->active == 1 ? 'selected' : null }}>Active</option>
							</select>
						</div>
					</div>
					
				</div>

				<div class="form-group pull-right">			
					{{ link_to('admin/users', 'Cancel',  array('class' => 'btn btn-danger')) }}
					{{ Form::submit('Update', array('class' => 'btn btn-success')) }}
				</div>

			{{ Form::close() }}
					
		</div>
		
	 </section> <!-- /.row -->
@stop

@section('script')

	<script src="{{ secure_asset('library/js/password.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('#password').password({
				minimumLength: 10,
				showText: true, // shows the text tips
				animate: true, // whether or not to animate the progress bar on input blur/focus
				animateSpeed: 'fast', // the above animation speed
			});
			$(window).on('keyup', function(){
				var passwordLength = $('#password').val().length;
				if($('#password').val().length > 0) {
					$('.password-rules').removeClass('hidden');
					$('#password').on('password.score', function(e, score){
						if(score >= 68) {
							$('input[type="submit"]').removeClass('disabled');
						} else {
							$('input[type="submit"]').addClass('disabled');
						}
					});
				} else {
					$('.password-rules').addClass('hidden');
					$('input[type="submit"]').removeClass('disabled');
				}
			});
		});
	</script>
@stop