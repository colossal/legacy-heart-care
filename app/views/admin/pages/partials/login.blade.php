<div class="row">
						@if(Session::get('login_attempts'))
						<strong>You have <span class="text-danger">{{ Session::get('login_attempts') }}</span> login attempt{{ Session::get('login_attempts') == 1 ? '' : 's' }} left</strong>
						<hr>
						@endif
			    		@if($errors->any())
							<div class="alert alert-danger">
								<ul>
									{{ implode('', $errors->all('<li class="error">:message</li>')) }}
								</ul>
							</div>
						@endif

						@if ($message = Session::get('error'))
							<div class="alert alert-danger">
								<ul>
									<li>{{ $message }}</li>
								</ul>
							</div>
						@endif

				    	<div class="col-sm-2">
					    	<label for="#">Email</label>
				    	</div>
				    	<div class="col-sm-10">
					    	<input name="email" type="email" placeholder="email" />
				    	</div>
				    	<div class="col-sm-2">
					    	<label for="#">Password</label>
				    	</div>
				    	<div class="col-sm-10">
					    	<input name="password" type="password" placeholder="password" />
				    	</div>
				    	<div class="col-sm-10 col-sm-offset-2">
					    	<input name="remember_me" type="checkbox" /> <small>Remember me</small>
				    	</div>
				    	<div class="clear"></div>
				    	<div class="col-sm-10 col-sm-offset-2">
					    	<input type="submit" class="btn btn-default" value="Sign in" /><br />
					    	<!-- <a href="/demo/dashboard" class="btn btn-default">Sign in</a> -->
					    	<small><a href="mailto:touchpoint@legacyheartcare.com?subject=TouchPoint%20password%20request">Forgot Password?</a></small>
				    	</div>

			    	</div>
		    	</div>