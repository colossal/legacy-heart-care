<div class="row">
						
		<div class="col-md-12">
			<div class="jumbotron text-center" style="background:transparent">
				<h3 class="text-danger"><i class="glyphicon glyphicon-lock"></i><br> Locked out</h3>
				<h4>You have failed to successully log in more than 5 times. To request a password reset</h4>
				<a class="btn btn-info" href="mailto:touchpoint@legacyheartcare.com?subject=User%20locked%20out">click here</a>
				<h4>to email the admin team.</h4>
			</div>
		</div>
    	</div>