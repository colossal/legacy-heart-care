@extends('admin.layouts.master')

@section('title')
Admin
@stop

@section('head')

@stop

@section('content')
	<div class="row" id="admin-panels">
		<div class="col-sm-1"></div>
		<div class="col-sm-3 panel admin-locations">
			<h3>Locations</h3>
			<a class="btn btn-default" href="{{ URL::to('admin/locations') }}">view locations</a>
		</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-3 panel admin-insurers">
			<h3>Insurers</h3>
			<a class="btn btn-default" href="{{ URL::to('admin/insurers') }}">view insurers</a>
		</div>
		<div class="col-sm-1"></div>
		<div class="col-sm-3 panel admin-users">
			<h3>Admins</h3>
			<a class="btn btn-default" href="{{ URL::to('admin/users') }}">view admins</a>
		</div>
	</div>
@stop

@section('script')
	
@stop