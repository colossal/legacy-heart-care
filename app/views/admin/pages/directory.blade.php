@extends('admin.layouts.master')

@section('title')
Directory
@stop

@section('head')

@stop

@section('content')

	<div class="row">
	  	<!--DEV NOTE : LOCATION FILTER
	  	- Selecting the location with filter patients to the specific location chosen-->
   	  	<form action="" class="filter pull-right col-sm-6">
			<br /><br />
			<div class="row">
   			<div class="col-sm-6">
	   			<label class="pull-left" for="">Filter by Treatment Location:</label> 
   			</div>
   			<div class="col-sm-6">
	   			<select name="" id="" class="form-control pull-right">
		   			<option value="">Choose a Location</option>
		   			<option value="">Austin</option>
		   			<option value="" selected>Dallas</option>
		   			<option value="">Ft.Worth</option>
		   			<option value="">Kansas City</option>
	   			</select>
   			</div>
			</div>
			<br />
		</form>
			<!-- ///END DEV NOTE : LOCATION FILTER -->
	</div>

	<table class="table table-striped queue">
	   	<thead>
		   	<tr>
			   	<th colspan="2">Name</th>
			   	<th>Phone</th>
			   	<th>Cardiologist</th>
			   	<th>TouchPoint</th>
			   	<th>Actions</th>
		   	</tr>
	   	</thead>
	   	<tbody>
	   		<!-- DEV NOTE : PATIENT RECORD ROW -->
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Jason Hurst</td><!-- DEV NOTE : PATIENT NAME -->
			   	<td>214.324.4450</td><!-- DEV NOTE : SHOWS PATIENT PHONE NUMBER -->
			   	<td>Dr. Leo Spaceman</td> <!-- DEV NOTE : SHOWS PATIENT CARDIOLOGIST -->
			   	<td>60 Day</td><!-- DEV NOTE : SHOWS CURRENT TOUCHPOINT RECORD STATUS -->
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a> <!-- DEV NOTE : TAKES VIEWER TO PATIENT INFORMATION (GLOBAL, NOT SPECIFIC TO TOUCHPOINT RECORD) -->
		   		</td>
		   	</tr>
		   	<!-- ///END DEV NOTE : PATIENT RECORD ROW -->
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Christina Smith</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Jim Johnson</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Carrie Johnson</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Richard Douglas</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Kim Meyers</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Jim Johnson</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Nancy Garcia</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Jim Beam</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Tom Jones</td>
			   	<td>214.324.4450</td>
			   	<td>Dr. Leo Spaceman</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Rick W. Anderson</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Lynn Whitehouse</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Alex Sinclair</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Sue Williams</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Tom Wellington</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Aaron Newhouse</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="/demo/record" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Beth Ortanga</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Lara Jones</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Barry Johnston</td>
			   	<td>214.324.4450</td>
			   	<td>Dr. Leo Spaceman</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
		   	<tr>
		   		<td><img src="/images/patients/bg-generic-ppl.jpg" alt="" /></td>
			   	<td>Sue Smith</td>
			   	<td>214.324.4450</td>
			   	<td>Dr.  Richard Douglas</td>
			   	<td>60 Day</td>
		   		<td>
			   		<a href="#" class="btn btn-primary view">View</a>
		   		</td>
		   	</tr>
	   	</tbody>
   	</table>

@stop

@section('script')
	
@stop