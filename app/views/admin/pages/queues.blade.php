@extends('admin.layouts.master')

@section('title')
Clinic Locations
@stop

@section('head')

@stop

@section('content')
	<h1>{{ $location->name }}</h1>
	<p>{{ $location->description }}</p>
	<br /><br />
	
	<?php $touchpoints = Config::get('constants.TOUCHPOINT'); ?>
	
	@if($patients_pastdue->count())

	<h1>PAST DUE</h1>

	<table class="table table-striped queue past-due">
	   	<thead>
		   	<tr>
			   	<th colspan="2">Name</th>
			   	<th>Phone</th>
			   	<th>Round</th>
			   	<th>TouchPoint</th>
			   	<th>Contact By</th>
			   	<th>Actions</th>
		   	</tr>
	   	</thead>
	   	<tbody>
	   		<!-- DEV NOTE : PATIENT RECORD ROW -->

	   		

	   		@foreach($patients_pastdue as $pastdue)
	   			<tr>
			   		<td>
			   		    @if(!empty($pastdue->photo))	
							<img src="/images/patients/{{$pastdue->photo}}" width="40" alt="">
						@else
							<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
						@endif
			   		</td>
				   	<td>{{ $pastdue->fullName() }}</td> <!-- DEV NOTE : PATIENT NAME -->
				   	<td>{{ $pastdue->phone }}</td> <!-- DEV NOTE : PATIENT PHONE NUMBER -->
				   	<td>{{ $pastdue->rounds_label }}</strong></td>
				   	<td>{{ ($pastdue->touchpoint_progress == 1) && (date('Y-m-d H:i:s', strtotime($pastdue->touchpoints()->orderby('created_at', 'desc')->first()->created_at)) < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00'))) ? '60 Days' : $touchpoints[$pastdue->touchpoint_progress] }}</td> <!-- DEV NOTE : PATIENT'S CURRENT TOUCHPOINT STATUS -->
				   	<td><span>{{ $pastdue->touchpoint_end_date->format('F j, Y') }}</span></td> <!-- DEV NOTE : DATE DETERMINED BY TOUCHPOINT STATUS -->
			   		<td>
				   		<a href="{{ URL::to('patient-directory/touchpoint/'.$pastdue->id.'/create') }}" class="btn btn-primary">Input TouchPoint</a> <!-- DEV NOTE : TAKES USER TO CURRENT TOUCHPOINT RECORD FOR THE PATIENT -->
			   		</td>
			   	</tr>
	   		@endforeach

		   	<!-- ///END DEV NOTE : PATIENT RECORD ROW -->
		   
	   	</tbody>
	</table>

	@endif

		@if( $patients_coming->count() )

		<h1>UPCOMING</h1>

		<table class="table table-striped queue">
		   	<thead>
			   	<tr>
				   	<th colspan="2">Name</th>
				   	<th>Phone</th>
				   	<th>Round</th>
				   	<th>TouchPoint</th>
				   	<th>Contact By</th>
				   	<th>Actions</th>
			   	</tr>
		   	</thead>
		   	<tbody>
		   		@foreach($patients_coming as $coming)
			   	<tr>
			   		<td>
				   		@if(!empty($coming->photo))	
							<img src="/images/patients/{{$coming->photo}}" width="40" alt="">
						@else
							<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
						@endif
			   		</td>
				   	<td>{{ $coming->fullName() }}</td>
				   	<td>{{ $coming->phone }}</td>
				   	<td>{{ $coming->rounds_label }}</td>
				   	<td>{{ $touchpoints[$coming->touchpoint_progress] }}</td>
				   	<td><span>{{ $coming->touchpoint_end_date->format('F j, Y') }}</span></td>
			   		<td>
				   		<a href="{{ URL::to('patient-directory/touchpoint/'.$coming->id.'/create') }}" class="btn btn-primary">Input TouchPoint</a>
			   		</td>
			   	</tr>
			   	@endforeach
		   	</tbody>
		</table>

		@else
					
		<div class="alert alert-info">No upcoming TouchPoints for this location.</div>	
				
		@endif
	

@stop

@section('script')
	
@stop