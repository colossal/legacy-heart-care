@extends('admin.layouts.master')

@section('title')
Clinic Locations
@stop

@section('head')

@stop

@section('content')
	<div class="location-list row">

		<div class="col-sm-10 col-sm-offset-1">
			<?php $i = 0; ?>
	   	  	@if ($locations->count())
                @foreach(array_chunk( $locations->all(), 2 ) as $location)
                    <div class="row">
                        @foreach($location as $item)

                          <div class="col-sm-6 col-md-6 location">
						   	  <div class="location-container">
						   	  	  <div class="gradient">
							   	  	 
									<img src="{{ URL::asset('library/img/loc-gradient.png') }}" alt="" />
									<h1>{{ $item->name }}</h1>
						   	  	  </div>
							   	  <img src="{{ URL::asset('images/locations/'.$item->photo) }}" alt="" />
							   	  <div class="clear"></div>
							   	  
							   	  @if($pastdue[$i])
									<p class="audit overdue">{{ $pastdue[$i] }} TouchPoints Overdue</p>
								  @else
									<p class="audit overdue none">&nbsp;</p>  
								  @endif							   	  
							   	  
							   	  <div class="clear"></div>
							   	  <hr />
							   	  <div class="review">
							   	  {{ Form::open(array('route' => 'admin.locations.set')) }}
							   	  	{{ Form::hidden('location_id', $item->id) }}							   	  
							   	  	{{ Form::submit('View', array('class' => 'btn btn-primary pull-right')) }}
							   	  {{ Form::close() }}
							   	  <p class="eligible">{{ $coming[$i] }} TouchPoints</p>
							   	  </div>
							   	  <div class="clear"></div>
							   	  <!--
							   	  DEV NOTE : THE FOLLOW COMMENT HIDES THE PROGRESS BAR
							   	  <hr />
							   	  <div class="progress">
								   	  <div class="progress-bar" role="progressbar" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100" style="width: 18%;"></div>
								   	  <span>18%</span>
							   	  </div>
							   	  -->
						   	  </div>
					   	  </div>
					   	<?php $i++; ?>
                        @endforeach
                    </div>
                @endforeach
              @else
                <p>No locations were found.</p>
            @endif
	
		   	  
		</div>

	</div>
@stop

@section('script')
	
@stop