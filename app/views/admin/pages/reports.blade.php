@extends('admin.layouts.master')

@section('title')
Reports
@stop

@section('head')





<style>
	table.reports a.btn-warning {
		border-color: #ed9c28;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 5px;
		background: #ffdbab;
		color: #333;
	}
	table.reports a.btn-danger {
		border-color: #aa2121;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 5px;
		background: #ffcccc;
		color: #333;
	}
	table.reports a.btn-legacy {
		border-color: #999999;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 5px;
		background: #cccccc;
		color: #333;
	}
	table.reports a.btn-default {
		border-color: #364BBE;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 5px;
		background: #d9dfff;
		color: #333;
	}
</style>
@stop

@section('content')
	<div class="row">
	
	    <div class="col-md-6">
			<br />
			
			<div class="clearfix"></div>
					@if(!empty($patients[0]))
						<a data-toggle="collapse" data-target="#extraFilters" class="btn btn-default" style="margin-top:-5px;"><i class="glyphicon glyphicon-circle-arrow-down"></i> Download TouchPoint Data</a>
					@endif
				
				
				
		
			
		</div>		
		<div class="col-xs-12 col-sm-12 col-md-2">
			<br />
			<label class="pull-right" for="">Filter View:</label> 
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-xs-8">
					{{ Form::open(array('route' => 'reports.set', 'class' => 'filter')) }}
					<div class="form-group">

						<select name="location_id" id="location_id" class="form-control col-sm-6">
							<option value="">View All Locations</option>
							@foreach(Location::all() as $location)
							<option value="{{ $location->id }}" {{ Session::get('location.id') && Session::get('location.id') == $location->id ? 'selected' : null  }}>{{ $location->name }}</option>
							@endforeach
						</select>
					</div>
					{{ Form::close() }}
				</div>				

				<div class="col-xs-4">
					{{ Form::open(array('route' => 'reports.state', 'class' => 'filter')) }}
					<div class="form-group">
						{{ Form::select('patient_state', Config::get('constants.PATIENT_STATE'), $patient_state, array('class' => 'form-control col-sm-6', 'id' => 'patient_state') ) }}
					</div>

					{{ Form::close() }}
				</div>
			</div> <!-- /.row -->
		</div>
	</div> <!-- /.row -->
	<div class="row extra-filters collapse" id="extraFilters">
		<div class="col-md-12">
			<br>
			<div class="well">
				{{ Form::open(['url' => URL::to('reports/download')]) }}
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<small>Between Two Dates</small>
							</div>
							<div class="col-md-6 form-group">
								<label for="">After</label>
								<input type="text" name="after" class="form-control datepickerfield">
							</div>
							<div class="col-md-6 form-group">
								<label for="">Before</label>
								<input type="text" name="before" class="form-control datepickerfield">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<small>Touchpoint Filters</small>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Touchpoint State</label>
								<br>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 30 Day <input type="checkbox" value="1" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 90 Day <input type="checkbox" value="2" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 6 Months <input type="checkbox" value="3" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 1 Year <input type="checkbox" value="4" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 2 Years <input type="checkbox" value="5" name="touchpoint[]"></label>
								<br>
								<small>PLEASE NOTE: Checking all the boxes is the same as leaving them all empty, as it will be assumed that you want all available Touchpoint States.</small>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Round</label>
								<select name="round" id="round" class="form-control">
									<option value="">Latest Round</option>
									@for($i = 1; $i <= Patient::getLargestAmountOfRounds($patients); $i++)
									<option value="{{ $i }}">Round {{ $i }}</option>
									@endfor
								</select>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Locations</label>
								<select name="location_id" id="location_id" class="form-control">
									<option value="">View All Locations</option>
									@foreach(Location::all() as $location)
									<option value="{{ $location->id }}" {{ Session::get('location.id') && Session::get('location.id') == $location->id ? 'selected' : null  }}>{{ $location->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Patient State</label>
								{{ Form::select('patient_state', Config::get('constants.FILTER_PATIENT_STATE'), $patient_state, array('class' => 'form-control', 'id' => 'patient_state') ) }}
							</div>
							<div class="col-md-12 form-group">
								<button type="submit" class="btn btn-success">Download</button>
							</div>
						</div>
					</div>
				</div>
				{{ Form::close() }}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<hr>
		</div>
		<div class="col-md-1">
			<a data-toggle="collapse" data-target="#legend" class="btn btn-default btn-xs">Legend</a>
		</div>
		<div id="legend" class="col-md-11 collapse">
			<table class="table table-bordered table-condensed reports">
				<tbody>
					<tr>
						<td class="td_valign"><a class="btn btn-danger">30 Day</a></td>
						<td class="td_valign"><span class="reports-style">We were not able to contact patient after 2 attempts.</span></td>
					</tr>
					<tr>
						<td class="td_valign"><a class="btn btn-warning">30 Day</a></td>
						<td class="td_valign"><span class="reports-style">Touchpoint is ready to be created</span></td>
					</tr>
					<tr>
						<td class="td_valign"><a class="btn btn-legacy">30 Day</a></td>
						<td class="td_valign"><span class="reports-style">
							This Touchpoint was skipped
						</span></td>
					</tr>
					<tr>
						<td class="td_valign"><a class="btn btn-default">30 Day</a></td>
						<td class="td_valign"><span class="reports-style">A Touchpoint exists for this patient</span></td>
					</tr>
					<tr>
						<td class="td_valign"><a class="btn btn-default inactive">30 Day</a></td>
						<td class="td_valign">This is a future or pending touchpoint stage</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br />
	</div>

	@if(!empty($patients[0]))

		<table class="table table-striped reports table-responsive">
		   	<thead>
			   	<tr>
				   	<th colspan="2">Name</th>
				   	<th>Phone</th>
				   	<th>Status</th>
				   	<th>Round</th>
				   	<th>TouchPoint</th>
				   	<th width="10"></th>
				   	<th>Actions</th>
			   	</tr>
		   	</thead>
		   	<tbody>
	   		@foreach($patients as $patient)
			   	<tr>
			   		<td>
			   		@if(!empty($patient->photo))
						<img src="/images/patients/{{$patient->photo}}" width="40" alt="">
					@else
						<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
					@endif
			   		</td>
				   	<td class="td_valign"><span class="reports-style">{{ $patient->fullName() }}</span></td>
				   	<td class="td_valign"><span class="reports-style">{{ $patient->phone }}</span></td>
				   	<td class="td_valign">
						@if($patient->patient_state == 1)
							<span style="color:#00804d">Active</span>
						@elseif ($patient->patient_state == 2)
							<span style="color:#d1be20">Archived</span>
						@elseif ($patient->patient_state == 3)
							<span style="color:#878787">Expired</span>
						@endif
					</td>
					<td class="td_valign">
						{{ $patient->rounds_label }}
					</td>
				   	<td class="td_valign">
				   		{{ $patient->touchpointProgress() }} 
				   		@if($patient->number_of_rounds > 1)
				   		<div class="collapse" id="rounds{{ $patient->id }}">
				   			<hr>
				   			<?php $count = $patient->number_of_rounds - 1; ?>
				   			<ul style="margin:0;padding:0;list-style-type:none;">
				   				@foreach(Patient::where('patient_id', $patient->patient_id)->whereNotIn('id', [$patient->id])->orderby('created_at', 'desc')->get() as $p)
								<li><small style="padding-right:10px;"><strong>Round {{ $count-- }}</strong>
								<br> <em class="text-muted">{{ date('m/d/Y', strtotime($p->created_at)) }}</em>
								</small> {{ $p->touchpointProgress('small', 'past') }}</li>
				   				@endforeach
				   			</ul>
				   		</div>
				   		@endif
			   		</td>
			   		<td class="td_valign">
						@if($patient->number_of_rounds > 1)
			   			<a class="toggle-rounds" style="cursor:pointer;" data-toggle="collapse" data-target="#rounds{{ $patient->id }}"><i class="far fa-clock"></i></a>
			   			@endif

			   		</td>
			   		<td>
				   		<a href="{{ URL::to('patient-directory/'.$patient->id.'/edit') }}" class="btn btn-primary view">Patient Info</a>
			   		</td>
			   	</tr>
		   	@endforeach
		   	</tbody>
	   	</table>

		@if(!empty($patients[0]))
			<div class="text-right">
			{{ $patients->appends(Request::except('page', '_token'))->links() }}
			</div>
		@endif
		
   	@else
					
		<div class="alert alert-info">No reports were found.</div>	
			
	@endif
	
@stop

@section('script')
	<script>
		$(function() {
			$('#location_id').change(function() {
				this.form.submit();
			});
			$('#patient_state').change(function() {
				this.form.submit();
			});
			// $('a.toggle-rounds').on('click', function(){
			// 	var target = $(this).data('target');
			// 	$(target).toggleClass('in');
			// });
		});
	</script>	
@stop