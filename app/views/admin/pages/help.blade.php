@extends('admin.layouts.master')

@section('title')
Help
@stop

@section('head')

@stop

@section('content')
<!--<h1>Help</h1>-->
<br /><br />
<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna sed urna ultricies ac tempor.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus luctus urna sed urna ultricies ac tempor dui sagittis. In condimentum facilisis porta. Sed nec diam.</p>	
<hr />
<div class="row">
	<div class="col-lg-8">
		
		<div class="panel-group" id="accordion">
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
		          <i class="glyphicon glyphicon-play-circle"></i> Navigating the Site
		        </a>
		      </h4>
		    </div>
		    <div id="collapseOne" class="panel-collapse collapse in">
		      <div class="panel-body">
		        <h4>Sections of the Site</h4>
					<ol>
						<li>
							<strong><a href="/dashboard">Dashboard</a></strong>: The dashboard gives you a quick view, by clinic location, of the amount of TouchPoint Records to review, along with any TouchPoint Records that are overdue and need to be addressed immediately. 
						</li>
						<li>
							<strong><a href="/reports">Reports</a></strong>: Filter patients by their clinic location and then you can view a patient's TouchPoint Records as well their general Patient information here.
						</li>
						<li><strong><a href="/patient-directory">Patient Directory</a></strong>: Filter patients by their clinic location and view their general patient information, and create new patients as well.</li>
					</ol>
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
		          <i class="glyphicon glyphicon-user"></i> Patient Administration
		        </a>
		      </h4>
		    </div>
		    <div id="collapseTwo" class="panel-collapse collapse">
		      <div class="panel-body">
		        <h4>Adding A Patient</h4>
					<ol >
						<li>To add a patient visit the <strong><a href="/patient-directory">Patient Directory</a></strong> section and click the <span class="btn btn-xs btn-default"><i class="glyphicon glyphicon-plus-sign"></i> Add Patient Record</span> button</li>
						<li>You will be sent to a form, where you can fill out the necessary fields. <button data-toggle="modal" data-target="#patientForm" class="btn btn-default btn-xs">See Reference</button></li>
						<li>Once you have filled out the information, click the <span class="btn btn-xs btn-primary">Create</span> button but to add the patient or <span class="btn btn-xs btn-danger">Cancel</span> if you do not wish to add this patient.</li>
					</ol>
						<br />
					<h4>Editing An Existing Patient</h4>
					<ol>
						<li>A patient's information is editable via either the <strong><a href="/reports">Reports</a></strong> or <strong><a href="/patient-directory">Patient Directory</a></strong> sections, where you click the <span class="btn btn-xs btn-primary">View</span> button on the desired patient row.</li>
						<li><label class="text-info">Note :</label> You will not be able to edit the <strong>Treatment Completion Date</strong> once the patient records has been submitted.</li>
					</ol>
					
					<!-- Modal -->
					<div class="modal fade" id="patientForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					        <h4 class="modal-title" id="myModalLabel">Patient Form</h4>
					      </div>
					      <div class="modal-body">
					        <img style="max-width:intrinsic;width:100%; margin:0 auto;display:block;" src="/library/img/patient-form.jpg" alt="" />
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					  </div>
					</div>
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
		          <i class="glyphicon glyphicon-flag"></i> TouchPoint Administration
		        </a>
		      </h4>
		    </div>
		    <div id="collapseThree" class="panel-collapse collapse">
		      <div class="panel-body">
		        <h4>Adding A Touchpoint Record</h4>
					<ol>
						<li>After choosing your location from the <strong><a href="/dashboard">Dashboard</a></strong>, click on the <span class="btn btn-xs btn-primary">TouchPoint</span> button by the patient you wish to add a TouchPoint Record. <button data-toggle="modal" data-target="#touchPointRecord" class="btn btn-default btn-xs">See Reference</button></li>
						<li>Once you have filled out the information, click the <span class="btn btn-xs btn-primary">Create</span> button but to add the TouchPoint Record or <span class="btn btn-xs btn-danger">Cancel</span> if you do not wish to add this TouchPoint Record</li></li>
					</ol>
					<br />
				<h4>Editing an Existing TouchPoint Record</h4>
					<ol>
						<li>Once a TouchPoint Record is submitted, you will not be able to edit the information in that Record. However, you can review the TouchPoint records submitted for each specific patient in the <strong><a href="/reports">Reports</a></strong> page.</li>
					</ol>
					<!-- Modal -->
					<div class="modal fade" id="touchPointRecord" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					        <h4 class="modal-title" id="myModalLabel">Patient Form</h4>
					      </div>
					      <div class="modal-body">
					        <img style="max-width:intrinsic;width:100%; margin:0 auto;display:block;" src="/library/img/touchpoint-record.png" alt="" />
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					  </div>
					</div>
		      </div>
		    </div>
		  </div>
		</div>
		
	</div>
</div>
@stop

@section('script')
	
@stop