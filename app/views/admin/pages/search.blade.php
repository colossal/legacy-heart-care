@extends('admin.layouts.master')

@section('title')
Search Results
@stop

@section('head')
<style>
	table.reports a.btn-warning {
		border-color: #ed9c28;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 5px;
		background: #fff;
		color: #333;
	}
	table.reports a.btn-danger {
		border-color: #aa2121;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 5px;
		background: #fff;
		color: #333;
	}
	table.reports a.btn-legacy {
		border-color: #999999;
		margin-bottom: 5px;
		margin-top: 5px;
		margin-right: 5px;
		background: #fff;
		color: #333;
	}
</style>
@stop

@section('content')
	<div class="row">
	  	<!--DEV NOTE : LOCATION FILTER
	  	- Selecting the location with filter patients to the specific location chosen-->
	  	<div class="col-sm-12">
	  		<br /><br />
	  	</div>
			<!-- ///END DEV NOTE : LOCATION FILTER -->
	</div>

	<section class="row">
		<div class="col-sm-12">
			
			@if( Session::has('success') )
				<div class="alert alert-success alert-dismissable">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('success') }}
				</div>
			@endif
			
			<h1>TouchPoints</h1>
		
			<table class="table table-striped queue">
			   	<thead>
				   	<tr>
					   	<th colspan="2">Name</th>
					   	<th>Phone</th>
					   	<th>Round</th>
					   	<th>TouchPoint</th>
					   	<th>Contact By</th>
					   	<th>Actions</th>
				   	</tr>
			   	</thead>
			   	
			   	<?php $touchpoints = Config::get('constants.TOUCHPOINT'); ?>
			   	
			   	<tbody>
			   		<!-- DEV NOTE : PATIENT RECORD ROW -->
				
						@foreach($patients as $patient)
							@if($patient->patient_state == 1)
							<tr>
						   		<td>
						   		    @if(!empty($patient->photo))	
										<img src="/images/patients/{{$patient->photo}}" width="40" alt="">
									@else
										<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
									@endif
						   		</td>
							   	<td>{{ $patient->fullName() }}
									{{ $patient->repeat > 0 ? '<br/><small class="text-muted">Patient has recurred '.$patient->repeat.($patient->repeat == 1 ? ' time' : ' times').'</small>' : null }}
								</td> <!-- DEV NOTE : PATIENT NAME -->
							   	<td>{{ $patient->phone }}</td> <!-- DEV NOTE : PATIENT PHONE NUMBER -->
							   	<td>{{ $patient->rounds_label }}</td>
							   	<td>{{ isset($touchpoints[$patient->touchpoint_progress]) ? $touchpoints[$patient->touchpoint_progress] : 'N/A' }}</td> <!-- DEV NOTE : PATIENT'S CURRENT TOUCHPOINT STATUS -->
							   	<td class="td_valign">{{ $patient->touchpoint_end_date->format('F j, Y') }}</td> <!-- DEV NOTE : DATE DETERMINED BY TOUCHPOINT STATUS -->
						   		<td>
							   		<a href="{{ URL::to('patient-directory/touchpoint/'.$patient->id.'/create') }}" class="btn btn-primary">Input TouchPoint</a> <!-- DEV NOTE : TAKES USER TO CURRENT TOUCHPOINT RECORD FOR THE PATIENT -->
						   		</td>
						   	</tr>
						   	@elseif ($patient->patient_state == 2 || $patient->patient_state == 3)
							<tr>
						   		<td>
						   		    @if(!empty($patient->photo))	
										<img src="/images/patients/{{$patient->photo}}" width="40" alt="">
									@else
										<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
									@endif
						   		</td>
							   	<td>{{ $patient->fullName() }}
									{{ $patient->repeat > 0 ? '<br/><small class="text-muted">Patient has recurred '.$patient->repeat.($patient->repeat == 1 ? ' time' : ' times').'</small>' : null }}
								</td> <!-- DEV NOTE : PATIENT NAME -->
							   	<td>{{ $patient->phone }}</td> <!-- DEV NOTE : PATIENT PHONE NUMBER -->
							   	<td>Round <strong>{{ $patient->number_of_rounds }}</strong></td>
							   	<td>N/A</td>
							   	<td><span>N/A</span></td>
						   		<td>
							   		<a class="btn btn-primary" style="background-color:#CCCCCC;border-color:#CCCCCC;">TouchPoint Ineligible</a>
							   	</td>
						   	</tr>
						   	@endif
			   		@endforeach		
				   	<!-- ///END DEV NOTE : PATIENT RECORD ROW -->
				   
			   	</tbody>
			</table>
			
			
			@if(!empty($patients[0]))

			<table class="table table-striped reports table-responsive">
				<thead>
			   	<tr>
				   	<th colspan="2">Name</th>
				   	<th>Phone</th>
				   	<th>Round</th>
				   	<th>Status</th>
				   	<th>TouchPoint</th>
				   	<th>Actions</th>
			   	</tr>
				</thead>
				
				<?php $touchpoints = Config::get('constants.TOUCHPOINT'); ?>
				
				
		   		<h1>Patient History</h1>
			   		

				@foreach($patients as $patient)
					<tr>
				   		<td>
				   		@if(!empty($patient->photo))
							<img src="/images/patients/{{$patient->photo}}" width="40" alt="">
						@else
							<img src="/images/patients/bg-generic-ppl.jpg" width="40" alt="">
						@endif
				   		</td>
				   		<td class="td_valign"><span class="reports-style">{{ $patient->fullName() }}
								{{ $patient->repeat > 0 ? '<br/><small class="text-muted">Patient has recurred '.$patient->repeat.($patient->repeat == 1 ? ' time' : ' times').'</small>' : null }}
							</span></td>
				   		<td class="td_valign"><span class="reports-style">{{ $patient->phone }}</span></td>
				   		<td class="td_valign">{{ $patient->rounds_label }}</td>
					   	<td class="td_valign">
						@if($patient->patient_state == 1)
							<span style="color:#00804d">Active</span>
						@elseif ($patient->patient_state == 2)
							<span style="color:#d1be20">Archived</span>
						@elseif ($patient->patient_state == 3)
							<span style="color:#878787">Expired</span>
						@endif
					</td>
					   	<td class="td_valign">
					   		{{ $patient->touchpointProgress() }}
					   		@if($patient->number_of_rounds > 1)
				   		<div class="collapse" id="rounds{{ $patient->id }}">
				   			<hr>
				   			<?php $count = $patient->number_of_rounds - 1; ?>
				   			<ul style="margin:0;padding:0;list-style-type:none;">
				   				@foreach(Patient::where('patient_id', $patient->patient_id)->whereNotIn('id', [$patient->id])->orderby('created_at', 'desc')->get() as $p)
								<li><small style="padding-right:10px;"><strong>Round {{ $count-- }}</strong>
								<br> <em class="text-muted">{{ date('m/d/Y', strtotime($p->created_at)) }}</em>
								</small> {{ $p->touchpointProgress('small', 'past') }}</li>
				   				@endforeach
				   			</ul>
				   		</div>
				   		@endif
				   		</td>
				   		<td class="td_valign">
						@if($patient->number_of_rounds > 1)
			   			<a class="toggle-rounds" style="cursor:pointer;" data-toggle="collapse" data-target="#rounds{{ $patient->id }}"><i class="far fa-clock"></i></a>
			   			@endif

			   		</td>
				   		<td>
					   		<a href="{{ URL::to('patient-directory/'.$patient->id.'/edit') }}" class="btn btn-primary view">Patient Info</a>
				   		</td>
				   	</tr>
				@endforeach
			</table>
			
			@if(!empty($patients[0]))
				<div class="text-right">
				{{ $patients->appends(Request::except('page', '_token'))->links() }}
				</div>
			@endif
			
			@else
					
				<div class="alert alert-info">No patients were found.</div>	
					
			@endif
			
			
		</div> <!-- /.col-sm-12 -->	
	 </section> <!-- /.container -->
@stop

@section('script')

@stop