@extends('admin.layouts.login')

@section('title')
Login
@stop

@section('head')

@stop

@section('content')

	<div class="row">
    	<div class="col-sm-5">
	    	<h1>
			   	<small>welcome to</small><br />TouchPoint
		   	</h1>
    	</div>

    	
    	<!-- LOGIN FORM -->

	    <!-- <form action="" class="login col-sm-7"> -->
	    {{ Form::open(array('class' => 'form-signin login col-sm-7', 'url' => URL::to('login'))) }}
	    	<div class="row">
		    	<p class="col-sm-8 col-sm-offset-2">
			    	login to get started
		    	</p>
		    	<div class="col-sm-12">
	    		@if(Session::get('locked_out') && Session::get('locked_out') == 1)
	    			@include('admin.pages.partials.locked-out')
	    		@else
	    			@include('admin.pages.partials.login')
	    		@endif
	    	</div>
	    <!-- </form> -->
	    {{ Form::close() }}
	    <!-- ///END LOGIN FORM -->
	    <div class="clear"></div>
    </div>
@stop

@section('scripts')
	<script>
		$(function() {
			$('input[name=email]').focus();
		});
	</script>
@stop