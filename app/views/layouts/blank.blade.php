@include('admin.layouts.partials.header')

{{-- @include('admin.layouts.partials.navigation') --}}

   	<section id="main">
	   	<div class="container">

	   		@yield('content')	  
		   	
	   	</div> <!-- /.container -->
   	</section> <!-- /#main -->
<div class="modal fade" id="inactiveUser" tabindex="-1" role="dialog" aria-labelledby="inactiveUser" data-backdrop="static">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel">Due to inactivity you are about to be logged out</h4>
			</div>
			<div class="modal-body">
				You will be logged out in <strong class="counter">59</strong> seconds.
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default close-modal" data-dismiss="modal">Stay Logged In</button>
				<a href="{{ URL::to('logout') }}" class="btn btn-danger modal-logout">Log Out</a>
			</div>
		</div>
	</div>
</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/bootstrap.min.js') : secure_asset('library/js/bootstrap.min.js') }}"></script>
	<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/jquery.plugin.js') : secure_asset('library/js/jquery.plugin.js') }}"></script>
	<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/bootstrap-datepicker.js') : secure_asset('library/js/bootstrap-datepicker.js') }}"></script>
	<script src="{{ strpos(URL::current(), 'localhost') ? asset('library/js/jquery.maskedinput.min.js') : secure_asset('library/js/jquery.maskedinput.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#treatment_completion').datepicker()
			$('.datepickerfield').datepicker()
			$('input[type="phone"]').mask('999.999.9999');
			$('input#dob').mask('99/99/9999');
			
			$('div.patient-state label').click(function(){
				$('div.patient-state label').removeClass('active');
				$(this).addClass('active');
			});
			
		});
	</script>

	@yield('script')

	</body>
</html>