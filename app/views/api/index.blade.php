@extends('layouts.blank')

@section('content')
<div class="row extra-filters" id="extraFilters">
	<div class="col-md-12">
		<h1 class="page-header">Generate Touchpoints API URL</h1>
	</div>
		<div class="col-md-12">
			<br>
			<div class="well">
				{{ Form::open(['class' => 'generate-url', 'url' => URL::to('reports/api/create-link')]) }}
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<small>Between Two Dates</small>
							</div>
							<div class="col-md-6 form-group">
								<label for="">After</label>
								<input type="text" name="after" class="form-control datepickerfield">
							</div>
							<div class="col-md-6 form-group">
								<label for="">Before</label>
								<input type="text" name="before" class="form-control datepickerfield">
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12">
								<small>Touchpoint Filters</small>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Touchpoint State</label>
								<br>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 30 Day <input type="checkbox" value="0" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 90 Day <input type="checkbox" value="1" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 6 Months <input type="checkbox" value="2" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 1 Year <input type="checkbox" value="3" name="touchpoint[]"></label>
								<label class="btn btn-default btn-xs" style="margin-bottom:5px;"> 2 Years <input type="checkbox" value="4" name="touchpoint[]"></label>
								<br>
								<small>PLEASE NOTE: Checking all the boxes is the same as leaving them all empty, as it will be assumed that you want all available Touchpoint States.</small>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Round</label>
								<select name="round" id="round" class="form-control">
									<option value="">Latest Round</option>
									@for($i = 1; $i <= 100; $i++)
									<option value="{{ $i }}">Round {{ $i }}</option>
									@endfor
								</select>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Locations</label>
								<select name="location_id" id="location_id" class="form-control">
									<option value="">View All Locations</option>
									@foreach(Location::all() as $location)
									<option value="{{ $location->id }}">{{ $location->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-md-3 form-group">
								<label for="">Patient State</label>
								{{ Form::select('patient_state', Config::get('constants.FILTER_PATIENT_STATE'), null, array('class' => 'form-control', 'id' => 'patient_state') ) }}
							</div>
							<div class="col-md-12 form-group">
								<input type="hidden" name="apikey" value="{{ $_GET['apikey'] }}">
								<button type="submit" class="btn btn-primary">Generate URL</button>
							</div>
						</div>
					</div>
				</div>
				{{-- {{ Form::close() }} --}}
			</div>
			<hr>
			<div class="panel panel-default">
				<div class="panel-heading">Generated URL</div>
				<div class="panel-body url-generated">
					<input type="hidden" name="generated_url">
					<div class="message"></div>
					<span class="url" style="font-family:courier;font-weight: bold;color:green;word-wrap:break-word;"></span>
					<div class="actions">
						
					</div>

				</div>
			</div>
		</div>
	</div>
@stop

@section('script')
	<script>
		$('form.generate-url').on('submit', function(e){
			e.preventDefault();
			var form = $(this);
			var url = form.attr('action')
			var fields = form.serialize();
			$.ajax({
				url:url,
				method:'POST',
				data: fields, 
				beforeSend: function(){
					$('.url-generated .message').html('');
				},
				success:function(response){
					$('input[name="generated_url"]').val(response.url);
					$('.url-generated span.url').text(response.url);
					$('.url-generated .actions').html('<br/><a target="_blank" class="btn btn-default" href="'+response.url+'">See Results</a> <span class="btn btn-primary copy-btn" data-type="attribute" data-attr-name="data-clipboard-text" data-model="couponCode" data-clipboard-text="'+response.url+'">Copy URL</span>');
					if(response.message.length > 0) {
						$('.url-generated .message').html(response.message);
					}
				},
				error:function(response){
					console.log(response);
				}
			});
		});
		$('.url-generated').on("click", '.copy-btn', function(){
	        value = $(this).data('clipboard-text'); //Upto this I am getting value
	        var that = $(this);
	 		var text = $(this).text();
	 		$(this).text('copied!');
	 		$(this).addClass('btn-success');
	        var $temp = $("<input>");
	          $("body").append($temp);
	          $temp.val(value).select();
	          document.execCommand("copy");
	          $temp.remove();
	          setTimeout(function(){
	          	 that.text(text);
	          	 that.removeClass('btn-success');
	          }, 2000);
	    });
	</script>
@stop