@extends('layouts.blank')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-header">LHC Touchpoint Reports API</h1>
			<p class="lead">Use the following form to create an api key. You will need this key to generate data from the reports API.</p>
		</div>
		<div class="col-md-12">
			@if(APIKey::where('user_id', Auth::user()->id)->where('active', 1)->first() && !Session::get('success'))
			<div class="alert alert-danger">
				<h4>Hey {{ Auth::user()->fullName() }}</h4>
				<p>You have already generated an API Key. If you choose to generate a new key, <strong>your old URLs will no longer be considered active and will not generate reports</strong>.
				<br><br>
				If you do not wish to generate a new API key and instead want to see your current API Key, click below <br>
				<a href="{{ URL::to('admin/users/'.Auth::user()->id.'/edit') }}" class="btn btn-default">View Current API Key</a>
				</p>
			</div>
			@endif
			<div class="well">
				{{ Form::open() }}
				<button type="submit" class="btn btn-primary btn-lg">Generate API Key</button>
				{{ Form::close() }}
			</div>
			@if(Session::get('success'))
			<div class="panel panel-default">
				<div class="panel-heading">
					Here is your API Key and URL
				</div>
				<div class="panel-body">
					<strong>Your API Key</strong><br>
					<code>
						{{ str_replace(URL::to('reports/api?apikey='), '', Session::get('success')) }}
					</code>
					<br>
					<strong>Your API URL</strong><br>
					<code>
						{{ Session::get('success') }}
					</code>
				</div>
			</div>
			@endif
		</div>
	</div>
@stop