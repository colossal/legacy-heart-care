<?php namespace admin;

use Clockwork\Request\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use BaseController;
use Input;
use View;
use Redirect;
use Validator;
use Str;
use User;
use Location;
use Insurer;
use Patient;
use Image;
use Session;
use Carbon\Carbon;
use Config;
use Excel;
use Touchpoint;
use Illuminate\Support\Facades\DB;
class PatientDirectoryController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $location_id = $this->getLocation();
        $patient_state = Session::get('patient_state.id', function () {
            return array('patient_state' => '1');
        });

        $locations = Location::where('active', '>', 0)->orderBy('name', 'ASC')->lists('name', 'id');


        if ($location_id) {
            $patient_ids = Patient::whereNotNull('first_name')->whereNotNull('patient_id')->orderby('created_at', 'desc')->where('location_id', '=', $location_id)->where('patient_state', '=', $patient_state['patient_state'])->groupby('patient_id')->lists('id');
            $patients = Patient::wherein('id', $patient_ids)->orderBy('last_name', 'ASC')
                            ->paginate(50);
        } else {
            $patient_ids = Patient::whereNotNull('first_name')->whereNotNull('patient_id')->orderby('created_at', 'desc')->where('patient_state', '=', $patient_state['patient_state'])->groupby('patient_id')->lists('id');
            $patients = Patient::wherein('id', $patient_ids)->orderBy('last_name', 'ASC')
                            ->paginate(50);
        }

        return View::make('admin.patient-directory.index', compact('patients', 'locations', 'patient_state'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $locations = Location::where('active', '>', 0)->orderBy('name', 'ASC')->lists('name', 'id');

        $insurers = Insurer::where('active', '>', 0)->orderBy('name', 'ASC')->lists('name', 'id');

        $secondary_insurers = array('' => 'Please Select') + $insurers;

        $location_id = $this->getLocation();

        return View::make('admin.patient-directory.create', compact('locations', 'insurers', 'secondary_insurers', 'location_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {

        //$touchpoint_date = Carbon::create( 2014, 5, 29, 0, 0, 0 );
        //echo $touchpoint_date->addDays(30);

        $input  = Input::all();

        $rules = array(
            'patient_id'				=>  'required',
            'first_name'				=>	'required',
            'last_name'					=>	'required',
//			'cardiologist_first_name'	=>	'required',
//			'cardiologist_last_name'	=>	'required',
            'location_id'				=>	'required',
            'treatment_completion'		=>	'required|date_format:m/d/Y',
//			'insurer_id'				=>	'required',
//			'address'					=>	'required',
//			'city'						=>	'required',
//			'state'						=>	'required',
//			'zip'						=>	'required|numeric|min:5',
//			'date_of_birth'				=>	'required|date_format:m/d/Y',
            'phone'						=>	'required',
//			'email'						=>	'email',
            );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {

            //$y = date('Y', strtotime(str_replace('/', '-', $input['treatment_completion'])));
            //$m = date('m', strtotime(str_replace('/', '-', $input['treatment_completion'])));
            //$d = date('d', strtotime(str_replace('/', '-', $input['treatment_completion'])));

            //$touchpoint_start_date = Carbon::create( $y, $d, $m, 0, 0, 0 );
            //$touchpoint_end_date = Carbon::create( $y, $d, $m, 0, 0, 0 );

            //$touchpoint_start_date->addDays(25);
            //$touchpoint_end_date->addDays(30);
            
            $patient = new Patient();
            $patient->non_english = $input['non_english'];
            // $patient->repeat = $input['repeat'];
            $patient->patient_id = $input['patient_id'];
            $patient->first_name = $input['first_name'];
            $patient->last_name = $input['last_name'];
            $patient->cardiologist_first_name = $input['cardiologist_first_name'];
            $patient->cardiologist_last_name = $input['cardiologist_last_name'];
            $patient->location_id = $input['location_id'];
            $patient->treatment_completion = date("Y-m-d", strtotime($input['treatment_completion']));
            $patient->insurer_id = $input['insurer_id'];
            $patient->secondary_insurer_id = isset($input['secondary_insurer_id']) ? $input['secondary_insurer_id'] : 0;
            $patient->address = $input['address'];
            $patient->city = $input['city'];
            $patient->state = $input['state'];
            $patient->zip = $input['zip'];
            $patient->date_of_birth = date("Y-m-d", strtotime($input['date_of_birth']));
            $patient->phone = $input['phone'];
            $patient->alternate_phone = $input['alternate_phone'];
            $patient->email = $input['email'];
            $patient->notes = $input['notes'];

            $touchpoint_start_date = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);
            $touchpoint_start_date->addDays(25);
            $patient->touchpoint_start_date = $touchpoint_start_date;
            
            $touchpoint_end_date = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);
            $touchpoint_end_date->addDays(30);
            $patient->touchpoint_end_date = $touchpoint_end_date;

            if (Input::has('patient_state')) {
                $patient->patient_state = $input['patient_state'];
            } else {
            	$input['patient_state'] = 1;
            	$patient->patient_state = 1;
            }
            $patient->expiration_note = $input['patient_state'] == 3 ? $input['expiration_note'] : null;

            //check if user has an updated image
            if (Input::hasFile('photo')) {
                // check if previous photo exists and delete it.
                $patient->deletePhoto($patient->photo);

                // generate a random file name
                $filename = Str::random(10) . time();
                // assinged file input to a variable
                $image = $input['photo'];
                // get original image file extension
                $extension = $image->getClientOriginalExtension();
                // open image file
                $photo = Image::make($image->getRealPath());
                // final file name
                $filename = $filename . '.' . $extension;
                // save file with medium quality
                $photo->save(public_path() . '/images/patients/'. $filename, 60);
                // store file name in database
                $patient->photo = $filename;
            }

            $patient->save();

            return Redirect::to('patient-directory')
            ->with('success', $patient->first_name . " " . $patient->last_name . ', has been added successfully.')
            ->with('class', 'alert-success');
        } else {
            return Redirect::to('patient-directory/create')->withInput()->withErrors($v)->with('class', 'alert-danger');
        }
    }

    public function getPatientById()
    {
    	$input = Input::all();

    	if(Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()) {
    		return [
    			'status' => true,
    			'id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->patient_id,
    			'name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->name,
    			'first_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->first_name,
    			'last_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->last_name,
    			'location_id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->location_id,
    			'cardiologist_first_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->cardiologist_first_name,
    			'cardiologist_last_name' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->cardiologist_last_name,
    			'insurer_id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->insurer_id,
    			'secondary_insurer_id' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->secondary_insurer_id,
    			'address' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->address,
    			'city' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->city,
    			'state' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->state,
    			'zip' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->zip,
    			'phone' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->phone,
    			'alternate_phone' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->alternate_phone,
    			'email' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->email,
    			'photo' => Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->photo,
    			'treatment_completion' => date('m/d/Y', strtotime(Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->treatment_completion)),
    			'date_of_birth' => date('m/d/Y', strtotime(Patient::where('patient_id', $input['search'])->orderby('created_at', 'desc')->first()->date_of_birth)),
    		];
    	}
		return ['status' => false];	
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $patient = Patient::findOrFail($id);

        
        return View::make('patient-directory.show', compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $patient = Patient::findOrFail($id);

        $locations = Location::where('active', '>', 0)->orderBy('name', 'ASC')->lists('name', 'id');

        $insurers = Insurer::where('active', '>', 0)->orderBy('name', 'ASC')->lists('name', 'id');

        $secondary_insurers = array('' => 'Please Select') + $insurers;

        return View::make('admin.patient-directory.edit', compact('patient', 'locations', 'insurers', 'secondary_insurers'));
    }

    public function deleteRound($id)
    {
        $patient = Patient::find($id);
        $patient->delete();
        $patientId = $patient->patient_id;
        return Redirect::to('patient-directory/'.Patient::where('patient_id', $patientId)->orderby('created_at', 'desc')->first()->id.'/edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input  = Input::all();

        $rules = array(
            'patient_id'				=>  'required',
            'first_name'				=>	'required',
            'last_name'					=>	'required',
//			'cardiologist_first_name'	=>	'required',
//			'cardiologist_last_name'	=>	'required',
            'location_id'				=>	'required',
            //'treatment_completion'		=>	'required',
//			'insurer_id'				=>	'required',
//			'address'					=>	'required',
//			'city'						=>	'required',
//			'state'						=>	'required',
//			'zip'						=>	'required',
//			'date_of_birth'				=>	'required|date_format:m/d/Y',
            'phone'						=>	'required',
//			'email'						=>	'email',
            );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {
            $patient = Patient::find($id);
            $patient->non_english = $input['non_english'];
            if(isset($input['repeat'])) {
            $patient->repeat = $input['repeat'];
            }
            $patient->first_name = $input['first_name'];
            $patient->last_name = $input['last_name'];
            $patient->cardiologist_first_name = $input['cardiologist_first_name'];
            $patient->cardiologist_last_name = $input['cardiologist_last_name'];
            $patient->location_id = $input['location_id'];
            $patient->patient_id = $input['patient_id'];
            //$patient->treatment_completion = $input['treatment_completion'];
            $patient->insurer_id = $input['insurer_id'];
            $patient->secondary_insurer_id = $input['secondary_insurer_id'];
            $patient->address = $input['address'];
            $patient->city = $input['city'];
            $patient->state = $input['state'];
            $patient->zip = $input['zip'];
            $patient->date_of_birth = date("Y-m-d", strtotime($input['date_of_birth']));
            $patient->phone = $input['phone'];
            $patient->alternate_phone = $input['alternate_phone'];
            $patient->email = $input['email'];
            $patient->notes = $input['notes'];

            if (Input::has('patient_state')) {
                $patient->patient_state = $input['patient_state'];
            }
            $patient->expiration_note = $input['patient_state'] == 3 ? $input['expiration_note'] : null;

            //check if user has an updated image
            if (Input::hasFile('photo')) {
                // check if previous photo exists and delete it.
                $patient->deletePhoto($patient->photo);

                // generate a random file name
                $filename = Str::random(10) . time();
                // assinged file input to a variable
                $image = $input['photo'];
                // get original image file extension
                $extension = $image->getClientOriginalExtension();
                // open image file
                $photo = Image::make($image->getRealPath());
                // final file name
                $filename = $filename . '.' . $extension;
                // save file with medium quality
                $photo->save(public_path() . '/images/patients/'. $filename, 60);
                // store file name in database
                $patient->photo = $filename;
            }

            $patient->save();

            return Redirect::to('patient-directory/'.$id.'/edit')->withInput()
            ->with('success', $patient->first_name . ' ' . $patient->last_name . ' has been updated successfully.')
            ->with('class', 'alert-success');
        } else {
            return Redirect::to('patient-directory/'.$id.'/edit')->withInput()->withErrors($v)->with('class', 'alert-danger');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $patient = Patient::find($id);
        $name = $patient->first_name.' '.$patient->last_name;
        $patient->deletePhoto($patient->photo);

        $patient->delete();

        return Redirect::to('patient-directory')->with('success', $name . ' has been deleted successfully.')->with('class', 'alert-success');
    }




    /**
     * Set current from patient directory location.
     *
     * @return Response
     */
    public function setLocation()
    {
        $id = Input::get('location_id');

        Session::put('location.id', $id);

        return Redirect::to('patient-directory');
    }

    /**
     * Set patient current state from patient directory.
     *
     * @return Response
     */
    public function setState()
    {
        $id = Input::only('patient_state');

        Session::put('patient_state.id', $id);

        return Redirect::back();
    }

    

    /**
     * View by location past due and upcoming.
     *
     * @param
     * @return Response
     */
    public function clinicLocations()
    {
        $location_id = $this->getLocation();
        $location = Location::find($location_id);
        $now = Carbon::today();

        $patients_pastdue = Patient::where('touchpoint_end_date', '<', $now->toDateTimeString())
                    ->where('location_id', $location_id)
                    ->where('patient_state', '=', 1)
                    ->where('touchpoint_progress', '<', 5)
                    ->orderBy('touchpoint_end_date', 'ASC')
                    ->distinct()
                    ->get();

        $patients_coming = Patient::where('touchpoint_start_date', '<=', $now->toDateTimeString())
                    ->where('touchpoint_end_date', '>=', $now->toDateTimeString())
                    ->where('location_id', $location_id)
                    ->where('patient_state', '=', 1)
                    ->where('touchpoint_progress', '<', 5)
                    ->orderBy('touchpoint_end_date', 'ASC')
                    ->distinct()
                    ->get();
    
        return View::make('admin.pages.queues', compact('location', 'patients_pastdue', 'patients_coming'));
    }
    

    /**
     * get current location id.
     *
     * @param
     * @return Response
     */
    public function getLocation()
    {
        if (Session::get('location.id')) {
            $location_id = Session::get('location.id', function () {
                try {
                    $location = Location::firstOrFail();
                    Session::set('location.id', $location->id);
                    return $location->id;
                } catch (ModelNotFoundException $e) {
                    return Redirect::to('admin/locations');
                }
            });

            return $location_id;
        }
        return null;
    }
    
    /**
     * Download report.
     *
     * @return Response
     */
    public function download()
    {
    	$data = [];
        $patient_state = Session::get('patient_state.id', function () {
            return array('patient_state' => '1');
        });

        $location_id = $this->getLocation();

        $location = $location_id ? Location::find($location_id) : null;

        $location_name = $location_id ? Str::slug($location['name']) : 'all';

        if($location_id) {
        	$patient_ids = Patient::whereNotNull('first_name')->whereNotNull('patient_id')->where('location_id', '=', $location_id)->where('patient_state', '=', $patient_state['patient_state'])->orderby('created_at', 'desc')->groupby('patient_id')->lists('id');
        	$patients = Patient::wherein('id', $patient_ids)->orderBy('last_name', 'ASC')->get();
        } else {
        	$patient_ids = Patient::whereNotNull('first_name')->whereNotNull('patient_id')->where('patient_state', '=', $patient_state['patient_state'])->orderby('created_at', 'desc')->groupby('patient_id')->lists('id');
        	$patients = Patient::wherein('id', $patient_ids)->orderBy('last_name', 'ASC')->get();
        }
        foreach($patients as $patient) {
        	array_push($data, [
        		'patient_ID' => $patient->patient_id,
        		'first_name' => $patient->first_name,
        		'last_name' => $patient->last_name,
        		'email' => $patient->email,
        		'location' => $patient->location->name,
        		'treatment_completion' => date('m/d/Y', strtotime($patient->treatment_completion)),
        		'cardiologist_first_name' => $patient->cardiologist_first_name,
        		'cardiologist_last_name' => $patient->cardiologist_last_name,
        		'address' => $patient->address,
        		'city' => $patient->city,
        		'state' => $patient->state,
        		'zip' => $patient->zip,
        		'patient_state' => $patient->patient_state,
        		'notes' => $patient->notes
        	]);
        }

        

        // $data = $patients->toArray();
        
        Excel::create('patient-reports-'.$location_name, function ($excel) use ($data) {
            
            // Set the title
            $excel->setTitle('TouchPoint Patient Report');

            // Chain the setters
            $excel->setCreator('TouchPoint')->setCompany('TouchPoint');

            // Call them separately
            $excel->setDescription('Generated report for patients.');

            $excel->sheet('Sheetname', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xlsx');
    }
}
