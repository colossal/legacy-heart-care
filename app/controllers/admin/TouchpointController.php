<?php namespace admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use BaseController;
use Input;
use View;
use Redirect;
use Validator;
use Str;
use User;
use Session;
use Touchpoint;
use Patient;
use Carbon\Carbon;

class TouchpointController extends BaseController
{

    /**
     * Display a listing of the resource.
     * GET /touchpoint
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * GET /touchpoint/create
     *
     * @return Response
     */
    public function create($id)
    {
        try {
            $patient = Patient::findOrFail($id);
            
            return View::make('admin.touchpoint.create', compact('patient'));
        } catch (ModelNotFoundException $e) {
            return '';
        }
    }

    /**
     * Store a newly created resource in storage.
     * POST /touchpoint
     *
     * @return Response
     */
    public function store()
    {
        $input  = Input::all();
        //		return $input;
        if (Input::has('skiptouchpoint')) {
            return $this->skiptouchpoint($input, $input['patient_id']);
        } else {
            $patient_id = $input['patient_id'];

            $rules = array(
                //'heart_failure'				=>	'required',
                //'ranexa'						=>	'required',
                'angina'						=>	'required',
                'sympton_severity'				=>	'required',
                'nitro'							=>	'required',
                //'walking_distance'			=>	'required',
                //'emergency_room_visit'		=>	'required',
                //'cardio_hospitalization'		=>	'required',
                'emergency_number_visits' 		=> 'required',
                //'emergency_visit_reason' 		=> 'required',
                'overnight_hospital_visits' 	=> 'required',
                //'overnight_hospital_reasons' 	=> 'required',
                //'cardio_related_procedure'	=> 'required',
                'interviewer_initial'				=> 'required'
                );

            $v = Validator::make($input, $rules);

            if ($v->passes()) {
                $patient = Patient::find($patient_id);
                $patient->touchpoint_snooze = 0;
                $touchpoint_progress = $patient->touchpoint_progress;

                $date_end = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);

                $date_start = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);

                // 30, 60, 365, 730 // 30 days is taken care off on PatientDirectoryController@store
                // 5, 7, 14, 21

                switch ($touchpoint_progress) {
                    case '0':
                    
                        $date_end->addDays(90);

                        $date_start->addDays(83);

                        break;
                    case '1':
                    
                        $date_end->addDays(182);

                        $date_start->addDays(172);

                        break;
                    case '2':
                    
                        $date_end->addDays(365);
                        
                        $date_start->addDays(351);

                        break;
                    case '3':
                    
                        $date_end->addDays(730);
                        
                        $date_start->addDays(709);

                        break;
                    case '4':
                    
                        $date_end->addDays(730);
                        
                        $date_start->addDays(709);

                        break;
                    default:
                        
                        break;
                }

                $patient->touchpoint_progress = $touchpoint_progress + 1;
                
                if ($patient->touchpoint_progress > 4) {
                    $patient->patient_state = '2'; // auto-archive on last TouchPoint
                }
                
                $patient->touchpoint_end_date = $date_end;
                $patient->touchpoint_start_date = $date_start;

                $touchpoint = new Touchpoint();

                $touchpoint->touchpoint_progress = $touchpoint_progress;

                if (Input::has('heart_failure')) {
                    $touchpoint->heart_failure = $input['heart_failure'];
                }

                if (Input::has('ranexa')) {
                    $touchpoint->ranexa = $input['ranexa'];
                }

                $touchpoint->angina = $input['angina'];

                if (Input::has('sympton_severity')) {
                    $touchpoint->sympton_severity = $input['sympton_severity'];
                } else {
                    $touchpoint->sympton_severity = $input['sympton_severity'];
                }
                
                $touchpoint->nitro = $input['nitro'];
                if (Input::has('walking_distance')) {
                    $touchpoint->walking_distance = $input['walking_distance'];
                }

                if (Input::has('emergency_room_visit')) {
                    $touchpoint->emergency_room_visit = $input['emergency_room_visit'];
                }

                if (Input::has('cardio_hospitalization')) {
                    $touchpoint->cardio_hospitalization = $input['cardio_hospitalization'];
                }
                if (Input::has('cardio_hospitalization_total')) {
                    $touchpoint->cardio_hospitalization_total = $input['cardio_hospitalization_total'];
                }
                if (Input::has('cardio_related_procedure')) {
                    $touchpoint->cardio_related_procedure = $input['cardio_related_procedure'];
                }

                $touchpoint->emergency_number_visits = isset($input['emergency_number_visits']) ? $input['emergency_number_visits'] : null;
                $touchpoint->emergency_visit_reason = isset($input['emergency_visit_reason']) ? $input['emergency_visit_reason'] : null;

                $touchpoint->overnight_hospital_visits = isset($input['overnight_hospital_visits']) ? $input['overnight_hospital_visits'] : null;
                $touchpoint->overnight_hospital_reasons = isset($input['overnight_hospital_reasons']) ? $input['overnight_hospital_reasons'] : null;

                $touchpoint->cardiac_coronary = isset($input['cardiac_coronary']) ? $input['cardiac_coronary'] : null;
                $touchpoint->cardiac_pci = isset($input['cardiac_pci']) ? $input['cardiac_pci'] : null;
                $touchpoint->cardiac_heart_attack = isset($input['cardiac_heart_attack']) ? $input['cardiac_heart_attack'] : null;
                $touchpoint->cardiac_notes = isset($input['cardiac_notes']) ? $input['cardiac_notes'] : null;

                $touchpoint->life_improved = $input['life_improved'];
                $touchpoint->notes = $input['notes'];
                $touchpoint->interviewer_initial = $input['interviewer_initial'];

                $touchpoint->touchpoint_complete = 1;
                $patient->touchpoint_snooze = 0;
                $patient->touchpoints()->save($touchpoint);

                $patient->save();


                return Redirect::to('reports')
                ->with('success', 'New touchpoint has been added successfully.')
                ->with('class', 'alert-success');
            } else {
                return Redirect::back()->withInput()->withErrors($v)->with('class', 'alert-danger');
            }
        }
    }

    /**
     * Display the specified resource.
     * GET /touchpoint/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * GET /touchpoint/{id}/edit
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id, $progress_id)
    {
        try {
            $patient = Patient::findOrFail($id);

            $touchpoint = Touchpoint::findOrFail($progress_id);

            return View::make('admin.touchpoint.edit', compact('patient', 'touchpoint'));
        } catch (ModelNotFoundException $e) {
            return '';
        }
    }

    /**
     * Update the specified resource in storage.
     * PUT /touchpoint/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, $progress_id)
    {
        $input  = Input::all();

        //		return $input;
        $rules = array(
            // 'heart_failure'				=>	'required',
            // 'ranexa'						=>	'required',
            'angina'						=>	'required',
            'sympton_severity'				=>	'required',
            'nitro'							=>	'required',
            // 'walking_distance'			=>	'required',
            //'emergency_room_visit'		=>	'required',
            //'cardio_hospitalization'		=>	'required',
            //'cardio_related_procedure'	=>	'required',
            'interviewer_initial'				=> 'required'
            );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {
            $touchpoint = Touchpoint::find($progress_id);

            if (Input::has('heart_failure')) {
                $touchpoint->heart_failure = $input['heart_failure'];
            }

            if (Input::has('ranexa')) {
                $touchpoint->ranexa = $input['ranexa'];
            }

            $touchpoint->angina = $input['angina'];

            if (Input::has('sympton_severity')) {
                $touchpoint->sympton_severity = $input['sympton_severity'];
            } else {
                $touchpoint->sympton_severity = $input['sympton_severity'];
            }
            
            $touchpoint->nitro = $input['nitro'];
            if (Input::has('walking_distance')) {
                $touchpoint->walking_distance = $input['walking_distance'];
            }

            if (Input::has('emergency_room_visit')) {
                $touchpoint->emergency_room_visit = $input['emergency_room_visit'];
            }

            if (Input::has('cardio_hospitalization')) {
                $touchpoint->cardio_hospitalization = $input['cardio_hospitalization'];
            }
            if (Input::has('cardio_hospitalization_total')) {
                $touchpoint->cardio_hospitalization_total = $input['cardio_hospitalization_total'];
            }
            if (Input::has('cardio_related_procedure')) {
                $touchpoint->cardio_related_procedure = $input['cardio_related_procedure'];
            }

            $touchpoint->emergency_number_visits = isset($input['emergency_number_visits']) ? $input['emergency_number_visits'] : null;
            $touchpoint->emergency_visit_reason = isset($input['emergency_visit_reason']) ? $input['emergency_visit_reason'] : null;

            $touchpoint->overnight_hospital_visits = isset($input['overnight_hospital_visits']) ? $input['overnight_hospital_visits'] : null;
            $touchpoint->overnight_hospital_reasons = isset($input['overnight_hospital_reasons']) ? $input['overnight_hospital_reasons'] : null;

            $touchpoint->cardiac_coronary = isset($input['cardiac_coronary']) ? $input['cardiac_coronary'] : null;
            $touchpoint->cardiac_pci = isset($input['cardiac_pci']) ? $input['cardiac_pci'] : null;
            $touchpoint->cardiac_heart_attack = isset($input['cardiac_heart_attack']) ? $input['cardiac_heart_attack'] : null;
            $touchpoint->cardiac_notes = isset($input['cardiac_notes']) ? $input['cardiac_notes'] : null;
            $touchpoint->life_improved = $input['life_improved'];
            $touchpoint->notes = $input['notes'];
            $touchpoint->interviewer_initial = $input['interviewer_initial'];

            $touchpoint->touchpoint_complete = 1;

            $touchpoint->save();
            $patient = Patient::find($input['patient_id']);
            $patient->touchpoint_snooze = 0;
            $patient->save();

            return Redirect::to('reports')
            ->with('success', 'Touchpoint has been updated successfully.')
            ->with('class', 'alert-success');
        } else {
            return Redirect::back()->withInput()->withErrors($v)->with('class', 'alert-danger');
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /touchpoint/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show a touchpoint for the specified patient.
     *
     * @param  int  $id
     * @param  int  $progress_id
     * @return Response
     */
    public function touchpoint($id, $progress_id)
    {
        try {
            $patient = Patient::findOrFail($id);

            $touchpoint = Touchpoint::findOrFail($progress_id);

            return View::make('admin.touchpoint.show', compact('patient', 'touchpoint'));
        } catch (ModelNotFoundException $e) {
            return '';
        }
        

        //return Redirect::to('patient-directory')->with('success', 'Your ' . $patient->name . ' location has been deleted successfully.')->with('class', 'alert-success');
    }

    /**
     * Unable to contact patiant.
     *
     * @param  int  $id
     * @return Response
     */
    public function unable()
    {
        $input  = Input::all();

        $patient_id = $input['patient_id'];

        $patient = Patient::find($patient_id);

        $snooze = $patient->touchpoint_snooze;
        $snooze++;
        $patient->touchpoint_snooze = $snooze;


        if ($snooze >= 3) {
            $touchpoint_progress = $patient->touchpoint_progress;

            $date_end = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);

            $date_start = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);

            // 30, 60, 365, 730 // 30 days is taken care off on PatientDirectoryController@store
            // 5, 7, 14, 21

            switch ($touchpoint_progress) {
                case '0':
                
                    $date_end->addDays(90);

                    $date_start->addDays(83);

                    break;
                case '1':
                
                    $date_end->addDays(182);

                    $date_start->addDays(172);

                    break;
                case '2':
                
                    $date_end->addDays(365);
                    
                    $date_start->addDays(351);

                    break;
                case '3':
                
                    $date_end->addDays(730);
                    
                    $date_start->addDays(709);

                    break;
                case '4':
                    
                        $date_end->addDays(730);
                        
                        $date_start->addDays(709);

                        break;
                default:
                    
                    break;
            }

            $patient->touchpoint_progress = $touchpoint_progress + 1;
             
            if ($patient->touchpoint_progress > 4 || $snooze >= 3) {
                $patient->patient_state = '2'; // auto-archive on last TouchPoint
            }
                
                
            $patient->touchpoint_end_date = $date_end;
            $patient->touchpoint_start_date = $date_start;

            $touchpoint = new Touchpoint();

            $touchpoint->touchpoint_progress = $touchpoint_progress;

            $touchpoint->touchpoint_complete = 0;

            $patient->touchpoints()->save($touchpoint);

            $patient->touchpoint_snooze = 0;
            $patient->save();

            return Redirect::to('clinic-locations');
        }
        

        $today = Carbon::today();

        $start_date = '';
        $end_date = '';

        $touchpoint_start_date = Carbon::createFromFormat('Y-m-d H:i:s', $patient->touchpoint_start_date);
        $touchpoint_end_date = Carbon::createFromFormat('Y-m-d H:i:s', $patient->touchpoint_end_date);

        $greater = $today->gt($touchpoint_end_date);

        if ($greater) {
            $start_date = $today->addDays(3)->toDateTimeString();
            ;

            $end_date = $today->addDays(3)->toDateTimeString();
        } else {
            $start_date = $touchpoint_end_date->toDateTimeString();
            ;
            $end_date  = $touchpoint_end_date->addDays(3)->toDateTimeString();
            ;
        }

        $patient->touchpoint_start_date = $start_date;
        $patient->touchpoint_end_date = $end_date;

        $patient->save();

        return Redirect::to('clinic-locations');
    }


    /**
     * Skip this touch point but can be edit later.
     *
     * @return Response
     */
    public function skiptouchpoint($data, $id)
    {
        $input  = $data;

        $patient_id = $id;

        $patient = Patient::find($patient_id);
        $patient->touchpoint_snooze = 0;
        $touchpoint_progress = $patient->touchpoint_progress;

        $date_end = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);

        $date_start = Carbon::createFromFormat('Y-m-d H:i:s', $patient->treatment_completion);

        // 30, 60, 365, 730 // 30 days is taken care off on PatientDirectoryController@store
        // 5, 7, 14, 21

        switch ($touchpoint_progress) {
            case '0':
            
                $date_end->addDays(90);

                $date_start->addDays(83);

                break;
            case '1':
            
                $date_end->addDays(182);

                $date_start->addDays(172);

                break;
            case '2':
            
                $date_end->addDays(365);
                
                $date_start->addDays(351);

                break;
            case '3':
            
                $date_end->addDays(730);
                
                $date_start->addDays(709);

                break;
            case '4':
                    
                    $date_end->addDays(730);
                    
                    $date_start->addDays(709);

                    break;
            default:
                
                break;
        }

        $patient->touchpoint_progress = $touchpoint_progress + 1;
        
        if ($patient->touchpoint_progress > 4) {
            $patient->patient_state = '2'; // auto-archive on last TouchPoint
        }
                
                
        $patient->touchpoint_end_date = $date_end;
        $patient->touchpoint_start_date = $date_start;

        $touchpoint = new Touchpoint();

        $touchpoint->touchpoint_progress = $touchpoint_progress;
        
        $touchpoint->notes = $input['notes'];

        $touchpoint->touchpoint_complete = 0;

        $touchpoint->touchpoint_skip = $touchpoint_progress + 1;

        $patient->touchpoints()->save($touchpoint);

        $patient->save();

        return Redirect::to('clinic-locations')
        ->with('success', 'Blank touchpoint has been added successfully.')
        ->with('class', 'alert-success');
    }
}
