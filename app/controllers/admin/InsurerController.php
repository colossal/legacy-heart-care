<?php namespace admin;

use BaseController, Input, View, Redirect, Validator, Str, Insurer;

class InsurerController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$insurers = Insurer::all();

		return View::make('admin.insurers.index', compact('insurers'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.insurers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input  = Input::all();

		$rules = array(
			'name'	=>	'required|unique:insurers',
			);

		$v = Validator::make($input, $rules);

		if($v->passes())
		{

			$location = new Insurer();
			$location->name = $input['name'];
			$location->active = $input['active'];
			$location->save();

			return Redirect::to('admin/insurers')
			->with('success', $location->name . ', a new insurer has been created successfully.')
			->with('class', 'alert-success');
		
		} else {

			return Redirect::to('admin/insurers/create')->withInput()->withErrors($v)->with('class', 'alert-danger');
		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$insurer = Insurer::findOrFail($id);
		
        return View::make('admin.insurers.show', compact('insurer'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$insurer = Insurer::findOrFail($id);

        return View::make('admin.insurers.edit', compact('insurer'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input  = Input::all();

		$rules = array(
			'name'	=>	'required|unique:insurers,name,' . $id,
			);

		$v = Validator::make($input, $rules);

		if($v->passes())
		{

			$location = Insurer::find($id);
			$location->name = $input['name'];
			$location->active = $input['active'];
			$location->save();

			return Redirect::to('admin/insurers')->withInput()
			->with('success', $location->name . ' insurer has been updated successfully.')
			->with('class', 'alert-success');
		
		} else {

			return Redirect::to('admin/insurers/'.$id.'/edit')->withInput()->withErrors($v)->with('class', 'alert-danger');
		
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$location = Insurer::find($id);

		$location->delete();

		return Redirect::to('admin/insurers')->with('success', $location->name . ' insurer has been deleted successfully.')->with('class', 'alert-success');
	}

}