<?php namespace admin;

use BaseController, Input, View, Redirect, Validator, Str, Location, Image, Session;

class LocationController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$locations = Location::all();

		return View::make('admin.locations.index', compact('locations'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.locations.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input  = Input::all();

		$rules = array(
			'name'	=>	'required|unique:locations',
			);

		$v = Validator::make($input, $rules);

		if($v->passes())
		{

			$location = new Location();
			$location->name = $input['name'];
			$location->active = $input['active'];

			//check if user has an updated image	
			if (Input::hasFile('photo'))
			{
				// check if previous photo exists and delete it.
				$location->deletePhoto($location->photo);

				// generate a random file name
				$filename = Str::random(10) . time();
				// assinged file input to a variable
				$image = $input['photo'];
				// get original image file extension
				$extension = $image->getClientOriginalExtension();
				// open image file
				$photo = Image::make($image->getRealPath());
				// final file name
				$filename = $filename . '.' . $extension;
				// save file with medium quality
				$photo->save(public_path() . '/images/locations/'. $filename, 60);
				// store file name in database	
				$location->photo = $filename;
			}

			$location->save();

			return Redirect::to('admin/locations')
			->with('success', $location->name . ', a new location has been created successfully.')
			->with('class', 'alert-success');
		
		} else {

			return Redirect::to('admin/locations/create')->withInput()->withErrors($v)->with('class', 'alert-danger');
		
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$location = Location::findOrFail($id);
		
        return View::make('admin.locations.show', compact('location'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$location = Location::findOrFail($id);

        return View::make('admin.locations.edit', compact('location'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input  = Input::all();

		$rules = array(
			'name'	=>	'required|unique:locations,name,' . $id,
			);

		$v = Validator::make($input, $rules);

		if($v->passes())
		{

			$location = Location::find($id);
			$location->name = $input['name'];
			$location->active = $input['active'];

			//check if user has an updated image	
			if (Input::hasFile('photo'))
			{
				// check if previous photo exists and delete it.
				$location->deletePhoto($location->photo);

				// generate a random file name
				$filename = Str::random(10) . time();
				// assinged file input to a variable
				$image = $input['photo'];
				// get original image file extension
				$extension = $image->getClientOriginalExtension();
				// open image file
				$photo = Image::make($image->getRealPath());
				// final file name
				$filename = $filename . '.' . $extension;
				// save file with medium quality
				$photo->save(public_path() . '/images/locations/'. $filename, 60);
				// store file name in database	
				$location->photo = $filename;
			}
			
			$location->save();

			return Redirect::to('admin/locations')->withInput()
			->with('success', 'Your ' . $location->name . ' location has been updated successfully.')
			->with('class', 'alert-success');
		
		} else {

			return Redirect::to('admin/locations/'.$id.'/edit')->withInput()->withErrors($v)->with('class', 'alert-danger');
		
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$location = Location::find($id);

		$location->deletePhoto($location->photo);

		$location->delete();

		return Redirect::to('admin/locations')->with('success', 'Your ' . $location->name . ' location has been deleted successfully.')->with('class', 'alert-success');
	}

	/**
	 * Set current location.
	 *
	 * @return Response
	 */
	public function setLocation()
	{
		$id = Input::get('location_id');

		Session::put('location.id', $id);

		return Redirect::to('clinic-locations');
	}

}