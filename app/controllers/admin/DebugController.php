<?php namespace admin;

use BaseController, Input, View, Redirect, Validator, Str, User;

class DebugController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return View::make('admin.debug.index');
	}


}