<?php namespace admin;

use Request, Patient, View;

class SearchController extends \BaseController {

	/**
	 * Display a search listing of the resource.
	 * GET /search
	 *
	 * @return Response
	 */
	public function index()
	{
		$query = Request::get('q');

		$patients = array();
		if($query)
		{
			$search1 = Patient::where('first_name', 'LIKE', "%$query%")->lists('id');
			$search2 = Patient::where('last_name', 'LIKE', "%$query%")->lists('id');
			$search3 = Patient::where('patient_id', 'LIKE', "%$query%")->lists('id');
			$allsearch = [];
			$allsearch = array_merge($allsearch,$search1);
			$allsearch = array_merge($allsearch,$search2);
			$allsearch = array_merge($allsearch,$search3);
			foreach($allsearch as $search) {
				$currentP = Patient::find($search);
				$latest = Patient::where('patient_id', $currentP->patient_id)->orderby('created_at', 'desc')->first()->id;
				if($currentP->number_of_rounds > 1) {
					if($search != $latest) {
						$allsearch = $this->remove_element($allsearch, $search);
					}
				}
			}
			if(count($allsearch)) {
				$patients = Patient::wherein('id', $allsearch)->orderby('created_at', 'desc')->paginate(20);
			}

		}
		
		return View::make('admin.pages.search', compact('patients'));

	}

	public function remove_element($array,$value) {
	  return array_diff($array, (is_array($value) ? $value : array($value)));
	}

}