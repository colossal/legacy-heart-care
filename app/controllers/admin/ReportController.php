<?php namespace admin;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use BaseController, Input, View, Redirect, Validator, Str, User, Session, Location, Patient;
use Excel;
class ReportController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$location_id = $this->getLocation();

		$patient_state = Session::get('patient_state.id', function() { return array('patient_state' => '1'); });

		$locations = Location::where('active', '>', 0)->orderBy('name', 'ASC')->lists('name','id');
		
		if($location_id) {
			$patient_ids = Patient::whereNotNull('first_name')->whereNotNull('patient_id')->orderby('created_at', 'desc')->where('location_id', '=', $location_id)->where('patient_state', '=', $patient_state['patient_state'])->groupby('patient_id')->lists('id');
			$patients = Patient::wherein('id', $patient_ids)->orderBy('last_name', 'ASC')
							->paginate(50);
		} else {
			$patient_ids = Patient::whereNotNull('first_name')->whereNotNull('patient_id')->orderby('created_at', 'desc')->where('patient_state', '=', $patient_state['patient_state'])->groupby('patient_id')->lists('id');
			$patients = Patient::wherein('id', $patient_ids)->orderBy('last_name', 'ASC')
							->paginate(50);
		}
		

		return View::make('admin.pages.reports', compact('patients', 'locations', 'patient_state'));
		
	}

	/**
	 * get current location id.
	 *
	 * @param  
	 * @return Response
	 */
	public function getLocation()
	{
		if(Session::get('location.id')) {
			$location_id = Session::get('location.id', function()
		{ 
			try
			{
			    $location = Location::firstOrFail();
			    Session::set('location.id', $location->id);
				return $location->id;
			}
			catch(ModelNotFoundException $e)
			{
			    return Redirect::to('admin/locations');
			}

		});

			return $location_id;
		}
		return null;

	}

	/**
	 * Set current from reports.
	 *
	 * @return Response
	 */
	public function setLocation()
	{
		$id = Input::get('location_id');
		if($id) {
			Session::put('location.id', $id);
		}
		else {
			Session::forget('location.id');
		}

		return Redirect::back();
	}

	/**
	 * Set patient current state from reports.
	 *
	 * @return Response
	 */
	public function setState()
	{
		$id = Input::only('patient_state');

		Session::put('patient_state.id', $id);

		return Redirect::back();
	}

	/**
	 * Download report.
	 *
	 * @return Response
	 */
	public function download()
	{	
		$input = Input::all();
		/*
		"_token": "wcKL6bx2yTICJ764qRhPzUBHlSavAoc0cE2n8UHC",
		"after": "09/01/2018",
		"before": "10/31/2018",
		"touchpoint": "",
		"round": "",
		"patient_state": "1"
		*/
		// return $input;
		set_time_limit(5000);
	 	$location_id = $input['location_id'] ?: null;

		$location = $location_id ? Location::find($location_id) : null;


		$location_name = $location_id ? Str::slug($location['name']) : 'all';		

		$after = strlen($input['after']) > 0 ? date('Y-m-d 00:00:00', strtotime($input['after'])) : null;
		$before = strlen($input['before']) > 0 ? date('Y-m-d 00:00:00', strtotime($input['before'])) : null;
		$patient_state = $input['patient_state'] > 0 ? $input['patient_state'] : null;
		$touchpoint_progress = 	isset($input['touchpoint']) ? $input['touchpoint'] : [0, 1, 2, 3, 4];
		$round = $input['round'] ?: null;
		//$patients = Patient::with('touchpoints')->where('patient_state', $patient_state)->wherein('touchpoint_progress', $input['touchpoint']);
		if(isset($input['touchpoint']) && count($input['touchpoint']) < 5) {
			$patients = Patient::with('touchpoints')->wherein('touchpoint_progress', $touchpoint_progress);
		} else {
			$patients = Patient::with('touchpoints');
		}
		if($patient_state) {
			$patients = $patients->where('patient_state', $patient_state);
		}
		if($location_id) {
			$patients = $patients->where('location_id', $location->id);
		}
		// return $patients;
		

		if($location_id) {
		Excel::create('patient-touchpoint-report-'.$location_name, function($excel) use($location_name, $patient_state, $location_id, $patients, $input, $touchpoint_progress, $after, $before, $round) {

			$patients->orderBy('last_name', 'ASC')->chunk(600, function($patients) use($location_name, $excel, $input, $touchpoint_progress, $after, $before, $round){
		    // Set the title
		    $excel->setTitle('TouchPoint Patient Report');

		    // Chain the setters
		    $excel->setCreator('TouchPoint')->setCompany('TouchPoint');

		    // Call them separately
		    $excel->setDescription('Generated report for patients.');

		    $excel->sheet('Sheetname', function($sheet) use($patients, $input, $touchpoint_progress, $after, $before, $round) {

		    	$sheet->cells('A1:O1', function($cells) {
		    		// Set background color
		    		$cells->setBackground('#DDDDDD');
				    // Set with font color
					$cells->setFontColor('#000000');

				});

		    	// TouchPoint labels
		    	$progress = array('0' => '30 Days', '1' => '90 Days', '2' => '6 months', '3' => '1 year', '4' => '2 years');
		    	$i = 2;
				
		    	$data[] = [
		    		'Patient ID' => '', 
		    		'First Name' => '', 
		    		'Last Name' => '', 
		    		'Patient State' => '', 
		    		'Insurer' => '', 
		    		'Round' => '', 
		    		'TouchPoint' => '', 
		    		'Heart Failure' => '', 
		    		'Ranexa' => '', 
		    		'Angina' => '', 
		    		'Sympton Severity' => '', 
		    		'Nitro' => '', 
		    		'Walking Distance' => '', 
		    		'Emergency Room Visit' => '', 
		    		'Emergency Room Number Visits' => '', 
		    		'Emergency Room Visit Reason' => '', 
		    		'Overnight Hospital Visits' => '', 
		    		'Overnight Hospital Reasons' => '', 
		    		'Cardio Hospitalization' => '', 
		    		'Cardio Hospitalization Total' => '', 
		    		'Cardio Related Procedure' => '', 
		    		'Cardiac Coronary' => '', 
		    		'Cardiac PCI' => '', 
		    		'Cardiac Heart Attack' => '', 
		    		'Cardiac Notes' => '', 
		    		'Life Improvements' => '',
		    		'Notes' => '', 
		    		'Touchpoint Skipped' => '', 
		    		'Interviewer Initials' => '',
		    		'Unable to Contact?' => '',
		    		'Created At' => ''
		    	];


				foreach ($patients as $patient) {


					$rounds = Patient::where('patient_id', $patient->patient_id)->orderby('created_at', 'asc')->lists('id');
					$show = true;

					if($round) {
						if((array_search($patient->id, $rounds) + 1) != $round) {
							$show = false;
						}
					}

					if($show) {
						for($t = 0; $t <= 4; $t++) {
						if(isset($patient->touchpoints[$t])) {
							$tp1 = $patient->touchpoints[$t];
							$show = true;
							if(strlen($input['after']) > 0 && strlen($input['before']) < 1) {
								$show = date('Y-m-d', strtotime($tp1->created_at)) >= date('Y-m-d', strtotime($input['after'])) ? true : false;
							} elseif(strlen($input['after']) < 1 && strlen($input['before']) > 0) {
								$show = date('Y-m-d', strtotime($tp1->created_at)) <= date('Y-m-d', strtotime($input['before'])) ? true : false;
							} elseif(strlen($input['after']) > 0 && strlen($input['before']) > 0) {
								$show = (date('Y-m-d', strtotime($tp1->created_at)) >= date('Y-m-d', strtotime($input['after']))) && (date('Y-m-d', strtotime($tp1->created_at)) <= date('Y-m-d', strtotime($input['before']))) ? true : false;
							}
							if($tp1->touchpoint_complete && $show) {
								if(in_array($tp1->touchpoint_progress, $touchpoint_progress)) {
									$data[] = [
										$patient->patient_id, 
										$patient->first_name, 
										$patient->last_name, 
										$patient->state,
										$patient->insurer['name'], 
										('Round '.(array_search($patient->id, $rounds) + 1)), 
										$progress[$tp1->touchpoint_progress], 
										$tp1->heart_failure, 
										$tp1->ranexa, 
										$tp1->angina, 
										$tp1->sympton_severity, 
										$tp1->nitro, 
										$tp1->walking_distance, 
										$tp1->emergency_room_visit, 
										$tp1->emergency_number_visits, 
										$tp1->emergency_visit_reason, 
										$tp1->overnight_hospital_visits, 
										$tp1->overnight_hospital_reasons, 
										$tp1->cardio_hospitalization, 
										$tp1->cardio_hospitalization_total, 
										$tp1->cardio_related_procedure, 
										$tp1->cardiac_coronary, 
										$tp1->cardiac_pci, 
										$tp1->cardiac_heart_attack, 
										$tp1->cardiac_notes, 
										$tp1->life_improved, 
										$tp1->notes, 
										$tp1->touchpoint_skip, 
										$tp1->interviewer_initial,
										$patient->touchpoint_snooze,
										$tp1->created_at->format('m/j/Y')
									];
								}
							}
						}
					}	
					}				
				}
		    		
		        $sheet->fromArray($data);			

		    });


		
		});
			})->download('xlsx');
	} else {
		Excel::create('patient-touchpoint-report-'.$location_name, function($excel) use($location_name, $patient_state, $patients, $input, $touchpoint_progress, $after, $before, $round) {
		
			$patients->orderBy('last_name', 'ASC')->chunk(600, function($patients) use($location_name, $excel, $input, $touchpoint_progress, $after, $before, $round){
		    // Set the title
		    $excel->setTitle('TouchPoint Patient Report');

		    // Chain the setters
		    $excel->setCreator('TouchPoint')->setCompany('TouchPoint');

		    // Call them separately
		    $excel->setDescription('Generated report for patients.');

		    $excel->sheet('Sheetname', function($sheet) use($patients, $input, $touchpoint_progress, $after, $before, $round) {

		    	$sheet->cells('A1:O1', function($cells) {
		    		// Set background color
		    		$cells->setBackground('#DDDDDD');
				    // Set with font color
					$cells->setFontColor('#000000');

				});

		    	// TouchPoint labels
		    	$progress = array('0' => '30 Days', '1' => '90 Days', '2' => '6 months', '3' => '1 year', '4' => '2 years');
		    	$i = 2;
				
		    	$data[] = [
		    		'Patient ID' => '', 
		    		'First Name' => '', 
		    		'Last Name' => '', 
		    		'Patient State' => '', 
		    		'Insurer' => '', 
		    		'Round' => '', 
		    		'TouchPoint' => '', 
		    		'Heart Failure' => '', 
		    		'Ranexa' => '', 
		    		'Angina' => '', 
		    		'Sympton Severity' => '', 
		    		'Nitro' => '', 
		    		'Walking Distance' => '', 
		    		'Emergency Room Visit' => '', 
		    		'Emergency Room Number Visits' => '', 
		    		'Emergency Room Visit Reason' => '', 
		    		'Overnight Hospital Visits' => '', 
		    		'Overnight Hospital Reasons' => '', 
		    		'Cardio Hospitalization' => '', 
		    		'Cardio Hospitalization Total' => '', 
		    		'Cardio Related Procedure' => '', 
		    		'Cardiac Coronary' => '', 
		    		'Cardiac PCI' => '', 
		    		'Cardiac Heart Attack' => '', 
		    		'Cardiac Notes' => '', 
		    		'Life Improvements' => '',
		    		'Notes' => '', 
		    		'Touchpoint Skipped' => '', 
		    		'Interviewer Initials' => '',
		    		'Unable to Contact?' => '',
		    		'Created At' => ''
		    	];


				foreach ($patients as $patient) {


					$rounds = Patient::where('patient_id', $patient->patient_id)->orderby('created_at', 'asc')->lists('id');
					$show = true;

					if($round) {
						if((array_search($patient->id, $rounds) + 1) != $round) {
							$show = false;
						}
					}

					if($show) {
						for($t = 0; $t <= 4; $t++) {
						if(isset($patient->touchpoints[$t])) {
							$tp1 = $patient->touchpoints[$t];
							$show = true;
							if(strlen($input['after']) > 0 && strlen($input['before']) < 1) {
								$show = date('Y-m-d', strtotime($tp1->created_at)) >= date('Y-m-d', strtotime($input['after'])) ? true : false;
							} elseif(strlen($input['after']) < 1 && strlen($input['before']) > 0) {
								$show = date('Y-m-d', strtotime($tp1->created_at)) <= date('Y-m-d', strtotime($input['before'])) ? true : false;
							} elseif(strlen($input['after']) > 0 && strlen($input['before']) > 0) {
								$show = (date('Y-m-d', strtotime($tp1->created_at)) >= date('Y-m-d', strtotime($input['after']))) && (date('Y-m-d', strtotime($tp1->created_at)) <= date('Y-m-d', strtotime($input['before']))) ? true : false;
							}
							if($tp1->touchpoint_complete && $show) {
								if(in_array($tp1->touchpoint_progress, $touchpoint_progress)) {
									$data[] = [
										$patient->patient_id, 
										$patient->first_name, 
										$patient->last_name, 
										$patient->state,
										$patient->insurer['name'], 
										('Round '.(array_search($patient->id, $rounds) + 1)), 
										$progress[$tp1->touchpoint_progress], 
										$tp1->heart_failure, 
										$tp1->ranexa, 
										$tp1->angina, 
										$tp1->sympton_severity, 
										$tp1->nitro, 
										$tp1->walking_distance, 
										$tp1->emergency_room_visit, 
										$tp1->emergency_number_visits, 
										$tp1->emergency_visit_reason, 
										$tp1->overnight_hospital_visits, 
										$tp1->overnight_hospital_reasons, 
										$tp1->cardio_hospitalization, 
										$tp1->cardio_hospitalization_total, 
										$tp1->cardio_related_procedure, 
										$tp1->cardiac_coronary, 
										$tp1->cardiac_pci, 
										$tp1->cardiac_heart_attack, 
										$tp1->cardiac_notes, 
										$tp1->life_improved, 
										$tp1->notes, 
										$tp1->touchpoint_skip, 
										$tp1->interviewer_initial,
										$patient->touchpoint_snooze,
										$tp1->created_at->format('m/j/Y')
									];
								}
							}

						}
					}	
					}				
				}
		    		
		        $sheet->fromArray($data);			

		    });


		
		});
			})->download('xlsx');
		
	}

	}


}