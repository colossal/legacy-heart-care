<?php namespace admin;

use BaseController, Input, View, Redirect, Validator, Str, User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PasswordController extends BaseController
{
    public function refresh()
    {
        return View::make('admin.password.refresh');
    }

    public function refreshPassword()
    {

        $input = Input::all();
        $rules = array(
            'password'		=>	'required|min:10|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
        );

        $v = Validator::make($input, $rules);
        if($v->fails())
        {
            return Redirect::back()->withErrors($v);
        } else {
            $user = Auth::user();
            $user->password = Hash::make($input['password']);
            $user->reset_password = date('Y-m-d 00:00:00', strtotime('+1 year'));
            $user->save();
            return Redirect::to('dashboard');
        }
    }

    public function showReset()
    {
        return View::make('admin.password.reset');
    }

    public function postReset()
    {
        $input = Input::all();
        $rules = [
            'first_name' => 'required|exists:users',
            'last_name' => 'required|exists:users',
            'email' => 'required|email|exists:users',
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        ];
        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            return Redirect::back()->withErrors($validator);
        } else {
            $user = User::where('email', $input['email'])->first();
            $user->password = Hash::make($input['password']);
            $user->restore = null;
            $user->locked = 0;
            $user->login_attempts = 0;
            $user->save();
            Auth::attempt(['email' => $input['email'],'password' => $input['password']]);
            return Redirect::to('dashboard');
        }
        return $input;
    }
}