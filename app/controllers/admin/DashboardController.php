<?php namespace admin;

use BaseController, Input, View, Redirect, Validator, Str, Location, Patient;
use Carbon\Carbon;

class DashboardController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$locations = Location::where('active','>',0)->orderBy('name', 'ASC')->get();

		$now = Carbon::today();

		$pastdue = array();
		$coming = array();

		foreach ($locations as $location) {

			$pastdue[] = Patient::where('touchpoint_end_date', '<', $now->toDateTimeString())
						->where('location_id', $location->id)
						->where('patient_state', '=', 1)
						->where('touchpoint_progress', '<', 5)
						->distinct()
						->count();

			$coming[] = Patient::where('touchpoint_start_date', '<=', $now->toDateTimeString())
					->where('touchpoint_end_date', '>=', $now->toDateTimeString())
					->where('location_id', $location->id)
					->where('patient_state', '=', 1)
					->where('touchpoint_progress', '<', 5)
					->distinct()
					->count();
		}

		return View::make('admin.pages.dashboard', compact('locations', 'pastdue', 'coming'));
		
	}


}