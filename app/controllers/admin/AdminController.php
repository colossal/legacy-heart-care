<?php namespace admin;

use BaseController, Input, View, Redirect, Validator, Str, User;

class AdminController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		//$user = User::findOrFail($id);
		$user = array();

		return View::make('admin.pages.admin', compact('user'));
		
	}


}