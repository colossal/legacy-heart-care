<?php namespace admin;

use BaseController;
use ModelNotFoundException;
use Auth;
use Input;
use View;
use Redirect;
use Validator;
use Str;
use User;
use Hash;
use Illuminate\Support\Facades\Session;

class UserController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::user()->access_level >= 2) {
            $users = User::orderBy('id', 'DESC')->get();

            return View::make('admin.users.index', compact('users'));
        } else {
            return Redirect::to('/dashboard');
        }

        

        //$show = Input::get('a');
        //if($show == 'sort'){
            //return View::make('admin.users.sort', compact('users'));
        //} else {
               //return View::make('admin.users.index', compact('users'));
        //}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        if (Auth::user()->access_level >= 2) {
            return View::make('admin.users.create');
        } else {
            return Redirect::to('/dashboard');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input  = Input::all();

        $rules = array(
            'first_name'	=>	'required',
            'last_name'	=>	'required',
            'email'	=>	'required|unique:users|email',
            'password' => strlen($input['password']) > 0 ? 'required|min:10|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9]).*$/' : ''
            );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {
            $user = new User();
            $user->first_name = $input['first_name'];
            $user->last_name = $input['last_name'];
            $user->email = $input['email'];
            $user->active = $input['active'];
            $user->access_level = $input['access_level'];
            $user->password = Hash::make($input['password']);
            $user->reset_password = date('Y-m-d 00:00:00', strtotime('+1 year'));
            $user->save();

            return Redirect::to('admin/users')->withInput()->with('success', 'New User has been created successfully.');
        } else {
            return Redirect::to('admin/users/create')->withInput()->withErrors($v);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return View::make('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return View::make('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
        
        $input  = Input::all();

        $rules = array(
            'first_name'	=>	'required',
            'last_name'	=>	'required',
            'email'	=>	'required|email',
            'password' => strlen($input['password']) > 0 ? 'min:10|regex:/^.*(?=.*[a-zA-Z])(?=.*[0-9]).*$/' : ''
            );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {
            $user = User::find($id);
            $user->first_name = $input['first_name'];
            $user->last_name = $input['last_name'];
            $user->email = $input['email'];
            $user->active = $input['active'];
            $user->access_level = $input['access_level'];
            $password = $input['password'];
            if (!empty($password)) {
                $user->password = Hash::make($password);
                $user->reset_password = date('Y-m-d 00:00:00', strtotime('+1 year'));
            }
                
            $user->save();

            return Redirect::to('admin/users/'.$id.'/edit')->withInput()->with('success', 'User info has been updated successfully.');
        } else {
            return Redirect::to('admin/users/'.$id.'/edit')->withInput()->withErrors($v);
        }
    }


    /**
     * Update users password.
     *
     * @return Response
     */
    public function updatepassword()
    {
        //
        $input  = Input::all();

        $id = $input['id'];

        $rules = array(
            'password'			=>	'required|min:10',
            'password_confirm'		=>	'required|same:password',
            );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {
            $user = User::find($id);
            $user->password = $input['password'];
            $user->reset_password = date('Y-m-d 00:00:00', strtotime('+1 year'));
            $user->save();

            return Redirect::to('admin/users/'.$id.'/edit')->withInput()->with('success', 'User password has been updated successfully.');
        } else {
            return Redirect::to('admin/users/'.$id.'/edit')->withInput()->withErrors($v);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //

        $user = User::find($id);

        $user->delete();

        return Redirect::to('admin/users')->with('success', 'User has been deleted.');
    }



    /**
     * Display a loing view.
     *
     * @return Response
     */
    public function showLogin()
    {
        return View::make('admin.pages.login');
    }

    /**
     * Log user from cms.
     *
     * @return Response
     */
    public function showLogout()
    {
        Auth::logout();

        return Redirect::to('/');
    }
    
    /**
     * Login user to cms.
     *
     * @return Response
     */
    public function postLogin()
    {
        $input  = Input::all();

        $rules = array(
        'email'			=>	'required|email',
        'password'		=>	'required',
    );
        $v = Validator::make($input, $rules);

        if ($v->fails()) {
            return Redirect::to('/')->withErrors($v);
        } else {

            // create our user data for the authentication
            $userdata = array(
                'email' => $input['email'],
                'password' => $input['password']
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {
                $user = User::where('email', $input['email'])->first();
                //if user has been cleared to get a password reset, then next attempt at logging in will redirect the user to the password reset functions
                if($user->restore == 1) {
                	Auth::logout();
                	Session::flash('restore', 'A Touchpoint Admin Team Member has allowed you to reset your password');
                	return Redirect::to('password/reset');
                }

                // if user is locked out, prevent authentication and show locked out content
                if($user->locked == 1) {
                	Auth::logout();
                	Session::flash('locked_out', 1);
                	return Redirect::to('/')->with('error', 'You are locked out because you failed to log in correctly 5 times');
                }
                
                // make sure user is set to normal state
                $user->login_attempts = 0;
                $user->locked = 0;
                $user->restore = null;
                $user->save();
                
                return Redirect::intended('dashboard');
            } else {
                // validation not successful, send back to form
                if (User::where('email', $input['email'])->first()) {
                    $user = User::where('email', $input['email'])->first();
                    $user->login_attempts = $user->login_attempts + 1;
                    $user->save();
                    if($user->restore == 1) {
                	Session::flash('restore', 'A Touchpoint Admin Team Member has allowed you to reset your password');
                	return Redirect::to('password/reset');
                	}
                	if($user->locked == 1) {
                	Session::flash('locked_out', 1);
	                }
                    if ($user->login_attempts < 5) {
                        Session::flash('login_attempts', 5 - $user->login_attempts);
                    } else {
                    	$user->locked = 1;
                    	$user->restore = null;
                    	$user->save();
                        Session::flash('locked_out', 1);
                    }
                }
                return Redirect::to('/')->with('error', 'Check your username or password');
                ;
            }
        }
    }

    public function unlockUser($id)
    {
    	$input = Input::all();
    	$user = User::find($id);
    	$user->restore = 1;
    	$user->save();
    	return Redirect::back()->with('success', 'User will able to reset their password.');
    }
}
