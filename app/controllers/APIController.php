<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;

class APIController extends BaseController {

	public function index()
	{
		if(isset($_GET['apikey']) && APIKey::where('key', $_GET['apikey'])->where('active', 1)->first()) {
			return View::make('api.index', compact('apikey'));
		}   
		return 'Incorrect or missing API Key';
	}

	public function createLink()
	{
		$input = Input::all();
		if(isset($input['apikey']) && APIKey::where('key', $input['apikey'])->where('active', 1)->first()) {	
		$url = URL::to('reports/api/generate?apikey='.$input['apikey']);
		$paramCount = 0;
		if(isset($input['after']) && strlen($input['after']) > 0) {
			$url .= '&after='.date('Y-m-d', strtotime($input['after']));
			$paramCount++;
		}
		if(isset($input['before']) && strlen($input['before']) > 0) {
			$url .= '&before='.date('Y-m-d', strtotime($input['before']));
		}
		if(isset($input['touchpoint']) && count($input['touchpoint']) > 0) {
			$touchpoints = '';
			for($i = 0; $i < count($input['touchpoint']); $i++) {
				$touchpoints .= $i < count($input['touchpoint']) - 1 ? $input['touchpoint'][$i].'+' : $input['touchpoint'][$i];
			}
			$url .= '&touchpoint='.$touchpoints;
			$paramCount++;
		}
		if(isset($input['round']) && strlen($input['round']) > 0) {
			$url .= '&round='.$input['round'];
			$paramCount++;
		}
		if(isset($input['location_id']) && strlen($input['location_id']) > 0) {
			$url .= '&location_id='.$input['location_id'];
			$paramCount++;
		}
		if(isset($input['patient_state']) && strlen($input['patient_state']) > 0) {
			$url .= '&patient_state='.$input['patient_state'];
			$paramCount++;
		}
		return [
			'url' => $url,
			'message' => $paramCount <= 1 ? '<div class="alert alert-danger"><strong>Please Note!</strong><br/>The query you have created may be too broad and may results in the system timing out before it can show results. We recommend clarifying your query with some more filters. </div>' : null
		];
		}   
		return 'Incorrect or missing API Key';

	}

	public function generate()
	{
		if(isset($_GET['apikey']) && APIKey::where('key', $_GET['apikey'])->where('active', 1)->first()) {
		$input = Input::all();
		set_time_limit(15000);
		$touchpoints = [0, 1, 2, 3, 4];
		if(isset($_GET['touchpoint']) && strlen($_GET['touchpoint'])) {
			$touchpoints = explode(" ", $_GET['touchpoint']);
		}
		
	 	$location_id = isset($_GET['location_id']) ? $_GET['location_id'] : null;

		$location = $location_id ? Location::find($location_id) : null;

		$location_name = $location_id ? Str::slug($location['name']) : 'all';		

		$after = isset($_GET['after']) && strlen($_GET['after']) > 0 ? date('Y-m-d 00:00:00', strtotime($_GET['after'])) : null;
		$before = isset($_GET['before']) && strlen($_GET['before']) > 0 ? date('Y-m-d 00:00:00', strtotime($_GET['before'])) : null;
		$patient_state = isset($_GET['patient_state']) && $_GET['patient_state'] > 0 ? $_GET['patient_state'] : null;
		$touchpoint_progress = 	$touchpoints ;
		$round = isset($_GET['round']) && strlen($_GET['round']) ? $_GET['round']: null;
		if($touchpoints && count($touchpoints) < 5) {
			$patients = Patient::with('touchpoints')->wherein('touchpoint_progress', $touchpoint_progress);
		} else {
			$patients = Patient::with('touchpoints');
		}
		if($patient_state) {
			$patients = $patients->where('patient_state', $patient_state);
		}
		if($location_id) {
			$patients = $patients->where('location_id', $location->id);
		}

		$progress = array('0' => '30 Days', '1' => '90 Days', '2' => '6 months', '3' => '1 year', '4' => '2 years');
		$touchpointQuery = array();
		foreach($touchpoints as $touchpoint) {
			$touchpointQuery[] = $progress[$touchpoint];
		}
		$patientStateQuery = array('0' => 'Ignore Patient State', '1' => 'Active', '2' => 'Archive', '3' => 'Expired', '4' => 'Incorrect');
		$data[] = [
			'query' => [
				'location' => $location_name,
				'after' => $after,
				'before' => $before,
				'touchpoints' => $touchpointQuery,
				'round' => $round,
				'patient_state' => $patient_state ? $patientStateQuery[$patient_state] : null,
				'touchpoint_count' => 0
			]
		];

		
		$i = 2;
		$tpCount = 0;
			foreach ($patients->get() as $patient) {
					$rounds = Patient::where('patient_id', $patient->patient_id)->orderby('created_at', 'asc')->lists('id');
					$show = true;
				if($round) {
					if((array_search($patient->id, $rounds) + 1) != $round) {
						$show = false;
					}
				}

				if($show) {
					for($t = 0; $t <= 4; $t++) {
					if(isset($patient->touchpoints[$t])) {
						$tp1 = $patient->touchpoints[$t];
						$show = true;
						if($after && !$before) {
							$show = date('Y-m-d', strtotime($tp1->created_at)) >= $after ? true : false;
						} elseif(!$after && $before) {
							$show = date('Y-m-d', strtotime($tp1->created_at)) <= $before ? true : false;
						} elseif($after && $before) {
							$show = (date('Y-m-d', strtotime($tp1->created_at)) >= $after) && (date('Y-m-d', strtotime($tp1->created_at)) <= $before) ? true : false;
						}
						if($tp1->touchpoint_complete && $show) {
							if(in_array($tp1->touchpoint_progress, $touchpoint_progress)) {
								$tpCount++;
								$data[] = [
									'Patient ID' => intval($patient->patient_id), 
									// 'First Name' => $patient->first_name, 
									// 'Last Name' => $patient->last_name, 
									// 'Patient State' => $patient->state,
									'Insurer' => $patient->insurer['name'], 
									'Round' => array_search($patient->id, $rounds) + 1, 
									'Touchpoint Progress' => $progress[$tp1->touchpoint_progress], 
									'Heart Failure' => $tp1->heart_failure, 
									'Ranexa' => $tp1->ranexa, 
									'Angina' => $tp1->angina, 
									'Symptom Severity' => $tp1->sympton_severity, 
									'Nitro' => $tp1->nitro, 
									'Walking Distance' => $tp1->walking_distance, 
									'Emergency Room Visit' => $tp1->emergency_room_visit, 
									'Emergency Number of Visits' => $tp1->emergency_number_visits, 
									'Emergency Visit Reason' => $tp1->emergency_visit_reason, 
									'Overnight Hospital Visits' => $tp1->overnight_hospital_visits, 
									'Overnight Hospital Reasons' => $tp1->overnight_hospital_reasons, 
									'Cardio Hospitalization' => $tp1->cardio_hospitalization, 
									ucwords(str_replace('_', ' ', 'cardio_hospitalization_total')) =>  $tp1->cardio_hospitalization_total, 
									ucwords(str_replace('_', ' ', 'cardio_related_procedure')) => $tp1->cardio_related_procedure, 
									ucwords(str_replace('_', ' ', 'cardiac_coronary')) => $tp1->cardiac_coronary, 
									ucwords(str_replace('_', ' ', 'cardiac_pci')) => $tp1->cardiac_pci, 
									ucwords(str_replace('_', ' ', 'cardiac_heart_attack')) => $tp1->cardiac_heart_attack, 
									ucwords(str_replace('_', ' ', 'cardiac_notes')) => $tp1->cardiac_notes, 
									ucwords(str_replace('_', ' ', 'life_improved')) => $tp1->life_improved, 
									// 'Notes' => $tp1->notes, 
									'Touchpoint Skip' => $tp1->touchpoint_skip, 
									'Interview Initials' => $tp1->interviewer_initial,
									'Touchpoint Snooze' => $patient->touchpoint_snooze,
									'Created At' => $tp1->created_at->format('m/j/Y')
								];
							}
						}
					}
					}	
				}				
			}
		$data[0]['query']['touchpoint_count'] = $tpCount;
		return $data;
		}   
		return 'Incorrect or missing API Key';
	}

	public function keypage()
	{
		return View::make('api.generate');
	}

	public function generateKey()
	{
		$input = Input::all();
		if(APIKey::where('user_id', Auth::user()->id)->where('active', 1)->first()) {
			$key = APIKey::where('user_id', Auth::user()->id)->where('active', 1)->first();
			$key->active = 0;
			$key->save();
		}
		$key = new APIKey();
		$key->key = Str::random(30);
		$key->active = 1;
		$key->user_id = Auth::user()->id;
		$key->save();
		return Redirect::back()->with('success', URL::to('reports/api?apikey='.$key->key));
	}

}
