<?php

class Location extends Eloquent {

	//protected $fillable = [];

	protected $table = 'locations';
	
	protected $columns = array('id','name','photo', 'active', 'description', 'created_at', 'updated_at'); // add all columns from you table

	public function scopeExclude($query,$value = array()) 
	{
	    return $query->select( array_diff( $this->columns,(array) $value) );
	}

	public function patients()
    {
        return $this->hasMany('Patient');
    }

	public function deletePhoto($filename) {

		if( File::exists( public_path() . '/images/locations/' . $filename) && !empty($filename) )
		{
			File::delete(public_path() . '/images/locations/' . $filename);

			return true;
		}

		return false;
	}

}