<?php

class Touchpoint extends \Eloquent {

	protected $fillable = [];

	protected $table = 'touchpoints';

	public function patients()
    {
        return $this->belongsToMany('Patient');
    }

    public function getInTheLastDateRangeAttribute()
    {
        $text = "In the last ";
        switch($this->touchpoint_progress) {
            case 0:
                return $text.'30 days';
                break;
            case 1:
                return $text.'2 months';
                break;
            case 2:
                return $text.'3 months';
                break;
            case 3:
                return $text.'6 months';
                break;
            case 4:
                return $text.'year';
                break;
            default:
                return null;
        }
    }

}