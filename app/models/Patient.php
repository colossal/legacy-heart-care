<?php

use Illuminate\Support\Facades\URL;
class Patient extends Eloquent {

	protected $fillable = [];

	protected $table = 'patients';
	protected $columns = array('id','patient_	id','first_name', 'last_name', 'date_of_birth', 'touchpoint_snooze', 'touchpoint_progress', 'touchpoint_start_date', 'touchpoint_end_date', 'location_id', 'treatment_completion', 'cardiologist_first_name', 'cardiologist_last_name', 'insurer_id', 'secondary_insurer_id', 'address', 'city', 'state', 'zip', 'phone', 'alternate_phone', 'email', 'notes', 'photo', 'patient_state', 'created_at', 'updated_at'); // add all columns from you table

	public function scopeExclude($query,$value = array()) 
	{
	    return $query->select( array_diff( $this->columns,(array) $value) );
	}

	public function getNameAttribute()
	{
		return $this->first_name.' '.$this->last_name;
	}

	public function getStateAttribute()
	{
		$states = array('1' => 'Active', '2' => 'Archive', '3' => 'Expired', '4' => 'Incorrect');
		return $states[$this->patient_state];
	}

	public function getDates()
	{
	    return array('created_at', 'updated_at', 'date_of_birth', 'touchpoint_start_date', 'touchpoint_end_date', 'treatment_completion');
	}

	public function touchpoints()
    {
        return $this->belongsToMany('Touchpoint')->withPivot('touchpoint_id');;
    }

    public function insurer()
    {
        return $this->belongsTo('Insurer');
    }

    public function location()
    {
        return $this->belongsTo('Location');
    }

    
	public function deletePhoto($filename) {

		if( File::exists( public_path() . '/images/locations/' . $filename) && !empty($filename) )
		{
			File::delete(public_path() . '/images/locations/' . $filename);

			return true;
		}

		return false;
	}

	public function fullName()
	{
		return $this->first_name . ' ' . $this->last_name;
	}


	public function fullDoctorName()
	{
		return $this->cardiologist_first_name . ' ' . $this->cardiologist_last_name;
	}

	public function progress()
	{
		$past = false;
		$rounds = $this->number_of_rounds;	
		$patient_records = Patient::where('patient_id', $this->patient_id)->orderby('created_at', 'desc')->lists('id');


		$html = '<div class="progress touchpoint-progress">';

		$progress = array('0' => '30 Day', '1' => '90 Day', '2' => '6 months', '3' => '1 year', '4' => '2 years');

		$urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		$segments = explode('/', $urlArray);
		$numSegments = count($segments);
		$currentSegment = $segments[$numSegments - 1];
		$id = $currentSegment;
		$create = true;
		if($rounds > 0 && ($this->id != $patient_records[0])) {
			$past = true;
			$create = false;
		}
		$latest_touchpoint = $this->touchpoints()->first() ? date('Y-m-d H:i:s', strtotime($this->touchpoints()->orderby('created_at', 'desc')->first()->created_at)) : date('Y-m-d H:i:s');
		for ( $i=0; $i < 5; $i++ ) {
			$progress_number = $progress[$i];
			if(($i == 1) && ($this->touchpoint_progress >= 1) &&
				(date('Y-m-d H:i:s', strtotime($this->created_at)) < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00'))) && ($latest_touchpoint < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00')))) {
				$progress_number = '60 Day';
			}
			if( $i < $this->touchpoint_progress ) {
				if($id == $this->touchpoints[$i]->id) {
					$html .= '<div class="progress-bar progress-bar-current" style="width: 20%">' . $progress_number . '</div>';
				} else {
					$html .= '<div class="progress-bar progress-bar-past" style="width: 20%"><a target="_blank" href="'.(URL::to('patient-directory/touchpoint/' . $this->id . '/' . $this->touchpoints[$i]->id)).'">' . $progress_number . '</a></div>';
				}

			} else {
				if($create && date('Y-m-d') >= date('Y-m-d', strtotime($this->touchpoint_end_date)) && $this->touchpoint_progress <= 4) {
					$html .= '<div class="progress-bar progress-bar-future" style="width: 20%"><a href="'.URL::to('/patient-directory/touchpoint/'. $this->id . '/create').'">' . $progress_number . '</a></div>';
					$create = false;
				} else {
					$html .= '<div class="progress-bar progress-bar-future" style="width: 20%">' . $progress_number . '</div>';
				}
			}
		}

		$html .= '</div><hr />';

		return $html;

	}

	public function progressCreate()
	{

		$html = '<div class="progress touchpoint-progress">';

		$progress = array('0' => '30 Day', '1' => '90 Day', '2' => '6 months', '3' => '1 year', '4' => '2 years');
		$create = true;
		$latest_touchpoint = $this->touchpoints()->first() ? date('Y-m-d H:i:s', strtotime($this->touchpoints()->orderby('created_at', 'desc')->first()->created_at)) : date('Y-m-d H:i:s');
		for ( $i=0; $i < 5; $i++ ) {
			$progress_number = $progress[$i];
			if(($i == 1) && ($this->touchpoint_progress >= 1) &&
				(date('Y-m-d H:i:s', strtotime($this->created_at)) < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00'))) && ($latest_touchpoint < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00')))) {
				$progress_number = '60 Day';
			}
			if($i == $this->touchpoint_progress && $this->touchpoint_snooze > 0 ) {
				$html .= '<div class="progress-bar progress-bar-future" style="width: 20%"><a href="'.URL::to('/patient-directory/touchpoint/'. $this->id . '/create').'">' . $progress_number . '</a></div>';
			}else if( $i < $this->touchpoint_progress ) {
				$html .= '<div class="progress-bar progress-bar-past" style="width: 20%"><a target="_blank" href="'.(URL::to('patient-directory/touchpoint/' . $this->id . '/' . $this->touchpoints[$i]->id)).'">' . $progress_number . '</a></div>';
			} else {
				if($create) {
					$html .= '<div class="progress-bar progress-bar-current" style="width: 20%">' . $progress_number . '</div>';
					$create = false;
				} else {
					$html .= '<div class="progress-bar progress-bar-future" style="width: 20%">' . $progress_number . '</div>';
				}
			}
		}

		$html .= '</div><hr />';

		return $html;
	}

	public function touchpointProgress($small = null, $past = null)
	{

		$html = '';
		//check for pre april 2018 push so that we can say they are "60 day"
		$tp_1 = '90 Day';
		$progress = array('0' => '30 Day', '1' => $tp_1, '2' => '6 months', '3' => '1 year', '4' => '2 years');
		$latest_touchpoint = $this->touchpoints()->first() ? date('Y-m-d H:i:s', strtotime($this->touchpoints()->orderby('created_at', 'desc')->first()->created_at)) : date('Y-m-d H:i:s');
		for ( $i=0; $i < 5; $i++ ) {
			$progress_number = $progress[$i];
			if(($i == 1) && ($this->touchpoint_progress >= 1) &&
				(date('Y-m-d H:i:s', strtotime($this->created_at)) < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00'))) && ($latest_touchpoint < date('Y-m-d H:i:s', strtotime('2018-04-16 00:00:00')))) {
				$progress_number = '60 Day';
			}
			if($i == $this->touchpoint_progress && $this->touchpoint_snooze > 0 ) {
					if($past) {
						$html .= '<a href="#" class="btn btn-default inactive '.($small ? 'btn-xs' : null).'">' . $progress_number . '</a>';
					} else {
					$html .= '<a href="'.(URL::to('patient-directory/touchpoint/'. $this->id . '/create')).'" class="btn btn-warning '.($small ? 'btn-xs' : null).'">' . $progress_number . '</a>';
					}
			}else if( $i < $this->touchpoint_progress ) {
				if($this->touchpoints[$i]->touchpoint_complete == 0) {
					if($this->touchpoints[$i]->automated == 1) {
						$html .= '<a href="'.(URL::to('patient-directory/touchpoint/' . $this->id . '/' . $this->touchpoints[$i]->id)).'" class="btn btn-legacy '.($small ? 'btn-xs' : null).'">' . $progress_number . '</a>';
					} else {
						$html .= '<a href="'.(URL::to('patient-directory/touchpoint/' . $this->id . '/' . $this->touchpoints[$i]->id)).'" class="btn btn-danger '.($small ? 'btn-xs' : null).'">' . $progress_number . '</a>';
					}
				} else {
						$html .= '<a href="'.(URL::to('patient-directory/touchpoint/' . $this->id . '/' . $this->touchpoints[$i]->id)).'" class="btn btn-default '.($small ? 'btn-xs' : null).'">' . $progress_number . '</a>';
				}
				
			} else {

				$html .= '<a href="#" class="btn btn-default inactive '.($small ? 'btn-xs' : null).'">' . $progress_number . '</a>';

			}
		}

		return $html;
	}

	public function getInTheLastDateRangeAttribute()
	{
		$text = "In the last ";
		switch($this->touchpoint_progress) {
			case 0:
				return $text.'30 days';
				break;
			case 1:
				return $text.'2 months';
				break;
			case 2:
				return $text.'3 months';
				break;
			case 3:
				return $text.'6 months';
				break;
			case 4:
				return $text.'year';
				break;
			default:
				return null;
		}
	}

	public function getNumberOfRoundsAttribute()
	{
		return Patient::where('patient_id', $this->patient_id)->count();
	}

	public function getRoundsLabelAttribute()
	{
		return '<span class="rounds-label" style="border:1px solid #d0d0d0; color:#888; font-size:10px;background:#f2f2f2;padding:5px 10px;border-radius:9999px;">Round <strong style="color:#7E3832;font-size:12px;padding-left:5px;">'.$this->number_of_rounds.'</strong></span>';
	}

	public function scopelatestPatient($query)
	{
		$omit = [];
		self::chunk(50, function($patients) use ($omit){
			foreach($patients as $patient) {
				if(self::where('patient_id', $patient->patient_id)->orderby('created_at', 'desc')->first()) {
					$latest = self::where('patient_id', $patient->patient_id)->orderby('created_at', 'desc')->first()->id;
					if($patient->number_of_rounds > 1 && ($patient->id != $latest)) {
						array_push($omit, $patient->id);
					}
				}
			}
		});
		return $query->whereNotIn('id', $omit);
	}

	public static function getLargestAmountOfRounds($query){
		$rounds = [];
		foreach($query as $patient) {
			array_push($rounds, $patient->number_of_rounds);
		}
		return max($rounds);
	}
}