<?php

class Insurer extends \Eloquent {

	protected $fillable = [];

	public function patients()
    {
        return $this->hasMany('Patient');
    }

}