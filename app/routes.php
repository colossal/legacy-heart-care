<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::pattern('id', '[0-9]+');
Route::pattern('touchpoint_id', '[0-9]+');

Route::get('update-patientID', 'SpecialController@transferID');

Route::get('/', 'admin\UserController@showLogin');
Route::post('/', 'admin\UserController@postLogin');
Route::post('login', 'admin\UserController@postLogin');
Route::get('logout', 'admin\UserController@showLogout');
Route::get('password/reset', 'admin\PasswordController@showReset');
Route::post('password/reset', 'admin\PasswordController@postReset');

// Authentication required
Route::group(array('before' => 'auth|auth.current-password'), function()
{
		//Debug
		Route::get('debug', 'admin\DebugController@index');
		//Dashboard
		Route::get('dashboard', 'admin\DashboardController@index');
		//Location Clinic
		Route::get('clinic-locations', 'admin\PatientDirectoryController@clinicLocations');
		//Reports
		Route::post('reports/set', array('as' => 'reports.set', 'uses' => 'admin\ReportController@setLocation'));
		Route::post('reports/state', array('as' => 'reports.state', 'uses' => 'admin\ReportController@setState'));
		Route::get('reports', 'admin\ReportController@index');
		Route::post('reports/download', 'admin\ReportController@download');
		Route::get('reports/debug', 'admin\ReportController@debug');
		// Touchpoint
		Route::get('patient-directory/touchpoint/{id}/create', array('as' => 'patient-directory.touchpoint.create', 'uses' => 'admin\TouchpointController@create'));
		Route::post('patient-directory/touchpoint', array('as' => 'patient-directory.touchpoint.store', 'uses' => 'admin\TouchpointController@store'));
		Route::get('patient-directory/touchpoint/{id}', array('as' => 'patient-directory.touchpoint.show ', 'uses' => 'admin\TouchpointController@show'));
		Route::get('patient-directory/touchpoint/{id}/{touchpoint_id}/edit', array('as' => 'patient-directory.touchpoint.edit', 'uses' => 'admin\TouchpointController@edit'));
		Route::put('patient-directory/touchpoint/{id}/{touchpoint_id}', array('as' => 'patient-directory.touchpoint.update', 'uses' => 'admin\TouchpointController@update'));
		Route::get('patient-directory/touchpoint/{id}/{touchpoint_id}', array('as' => 'patient-directory.touchpoint.touchpoint', 'uses' => 'admin\TouchpointController@touchpoint'));

		//Route::get('patient-directory/touchpoint/{id}/unable', array('as' => 'patient-directory.touchpoint.unable', 'uses' => 'admin\TouchpointController@unable'));
		Route::post('patient-directory/touchpoint/unable', array('as' => 'patient-directory.touchpoint.unable', 'uses' => 'admin\TouchpointController@unable'));


		// Directory
		Route::post('patient-directory/set', array('as' => 'patient-directory.set', 'uses' => 'admin\PatientDirectoryController@setLocation'));
		Route::post('patient-directory/state', array('as' => 'patient-directory.state', 'uses' => 'admin\PatientDirectoryController@setState'));
		Route::get('patient-directory/download', array('as' => 'patient-directory.download', 'uses' => 'admin\PatientDirectoryController@download'));
		Route::post('get-by-id', 'admin\PatientDirectoryController@getPatientById');
		Route::get('patient-directory/{id}/delete', 'admin\PatientDirectoryController@deleteRound');
		Route::resource('patient-directory', 'admin\PatientDirectoryController');
		// Search
		Route::get('search', 'admin\SearchController@index');

		// CMS Content Menu
		Route::group(array('prefix' => 'admin'), function()
		{
			//Admin
			Route::get('/', 'admin\AdminController@index');
			// Location
			Route::post('locations/set', array('as' => 'admin.locations.set', 'uses' => 'admin\LocationController@setLocation'));
			Route::resource('locations', 'admin\LocationController');
			// Insurance
			Route::resource('insurers', 'admin\InsurerController');
			// Users
			Route::post('users/updatepassword', array('as' => 'admin.users.updatepassword', 'uses' => 'UserController@updatepassword'));
			Route::post('users/{id}/unlock', ['as' => 'admin.users.unlock', 'uses' => 'admin\UserController@unlockUser']);
			Route::resource('users', 'admin\UserController');

		});
});

//Password
Route::post('password/refresh', 'admin\PasswordController@refreshPassword');
Route::get('password/refresh', 'admin\PasswordController@refresh');
Route::group(array('before' => 'auth|auth.current-password|auth.admin'), function()
{
Route::get('reports/api/key/generate', 'APIController@keypage');
Route::post('reports/api/key/generate', 'APIController@generateKey');
});
Route::post('reports/api/create-link', 'APIController@createLink');
Route::get('reports/api/generate', 'APIController@generate');
Route::get('reports/api', 'APIController@index');

//API Calls
Route::get('api/{table}/{apikey}', function($table = null, $apikey){
	if($apikey == '41a9l2334104KBO') {
		if($table == 'locations') {
			return Location::exclude(['photo'])->get();
		} elseif($table == 'insurers') {
			return Insurer::all();
		} elseif($table == 'patient-touchpoint') {
			return DB::table('patient_touchpoint')->get();
		} elseif($table == 'patients-proto') {
			return Patient::exclude(['first_name', 'last_name', 'address', 'city', 'state', 'zip', 'phone', 'alternate_phone', 'email', 'notes', 'photo', 'patient_state'])->get();
		} elseif($table == 'locations-proto') {
			return Location::exclude(['photo'])->get();
		} elseif($table == 'patients') {
			return Patient::exclude(['first_name', 'last_name', 'address', 'city', 'state', 'zip', 'phone', 'alternate_phone', 'email', 'notes', 'photo', 'patient_state'])->get();
		} elseif($table == 'touchpoints') {
			return Touchpoint::all();
		} 
		return '<p style="color:#fff;font-family:Helvetica, Arial, sans-serif;font-size:2em;font-weight:300;background:#fff; display:block;text-align:center; padding:15px;"><img style="margin-left:-10px;" src="http://touchpoint.legacyheartcare.com/library/img/logo-lhc.png" alt="" /><br /><br /> <span style="background:#D84348; padding:5px;">No Such Table Exists</span></p>';
	} 
	
	return '<p style="color:#fff;font-family:Helvetica, Arial, sans-serif;font-size:2em;font-weight:300;background:#fff; display:block;text-align:center; padding:15px;"><img style="margin-left:-10px;" src="http://touchpoint.legacyheartcare.com/library/img/logo-lhc.png" alt="" /><br /><br /> <span style="background:#D84348; padding:5px;">Incorrect API Key</span</p>';
	
});


// Help

Route::get('help', function(){
	return View::make('admin/pages/help');
});
