	<?php
return array (

	/*
	|--------------------------------------------------------------------------
	| Site global variables
	|--------------------------------------------------------------------------
	|
	| This are global varialbes that you can access from controller, models or views
	|
	*/

    'STATUS' => array('Inactive','Active'),

    'PATIENTS_PHOTOS_PATH' => public_path() . '/images/patients/',

    'LOCATION_PHOTOS_PATH' => public_path() . '/images/locations/',

    'EMAIL_SUBSCRIPTIONS' => array('Opt Out','Opt In'),

    'ACCESS_LEVEL' => array('User' => '1', 'Administrator' => '2'),

    'PATIENT_STATE' => array('1' => 'Active', '2' => 'Archive', '3' => 'Expired', '4' => 'Incorrect'),
    'FILTER_PATIENT_STATE' => array('0' => 'Ignore Patient State', '1' => 'Active', '2' => 'Archive', '3' => 'Expired', '4' => 'Incorrect'),

    'USA_STATES' => array(
    				'AL'=>"Alabama",  
					'AK'=>"Alaska",  
					'AZ'=>"Arizona",  
					'AR'=>"Arkansas",  
					'CA'=>"California",  
					'CO'=>"Colorado",  
					'CT'=>"Connecticut",  
					'DE'=>"Delaware",  
					'DC'=>"District Of Columbia",  
					'FL'=>"Florida",  
					'GA'=>"Georgia",  
					'HI'=>"Hawaii",  
					'ID'=>"Idaho",  
					'IL'=>"Illinois",  
					'IN'=>"Indiana",  
					'IA'=>"Iowa",  
					'KS'=>"Kansas",  
					'KY'=>"Kentucky",  
					'LA'=>"Louisiana",  
					'ME'=>"Maine",  
					'MD'=>"Maryland",  
					'MA'=>"Massachusetts",  
					'MI'=>"Michigan",  
					'MN'=>"Minnesota",  
					'MS'=>"Mississippi",  
					'MO'=>"Missouri",  
					'MT'=>"Montana",
					'NE'=>"Nebraska",
					'NV'=>"Nevada",
					'NH'=>"New Hampshire",
					'NJ'=>"New Jersey",
					'NM'=>"New Mexico",
					'NY'=>"New York",
					'NC'=>"North Carolina",
					'ND'=>"North Dakota",
					'OH'=>"Ohio",  
					'OK'=>"Oklahoma",  
					'OR'=>"Oregon",  
					'PA'=>"Pennsylvania",  
					'RI'=>"Rhode Island",  
					'SC'=>"South Carolina",  
					'SD'=>"South Dakota",
					'TN'=>"Tennessee",  
					'TX'=>"Texas",  
					'UT'=>"Utah",  
					'VT'=>"Vermont",  
					'VA'=>"Virginia",  
					'WA'=>"Washington",  
					'WV'=>"West Virginia",  
					'WI'=>"Wisconsin",  
					'WY'=>"Wyoming"),

	'TOUCHPOINT' => array('0' => '30 Days', '1' => '90 Days', '2' => '6 Months', '3' => '1 Year', '4' => '2 Years', '5' => '2 Years'),
	'ANGINA' => array('NA' => 'N/A', '0' => '0', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30'),
	'SYMPTON_SEVERITY' => array('0' => '0 Not Severe', '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10 Extremely Severe'),


);	